import { create } from 'zustand';

interface ArrivalSelectStore {
    isDisabled: boolean
    departCode: string
    arrivalCode: string
    onDepartSelect: (val: string) => void
    onArrivalSelect: (val: string) => void
}

const useArrivalSelect = create<ArrivalSelectStore>((set) => ({
    isDisabled: true,
    departCode: '',
    arrivalCode: '',
    onDepartSelect: (val) => set({ isDisabled: false, departCode: val }),
    onArrivalSelect: (val) => set({ isDisabled: false, arrivalCode: val })
}));


export default useArrivalSelect;