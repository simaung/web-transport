import { create } from 'zustand';

import { Outlet } from '@/types/outlet';

interface DetailOutletModalStore {
    isOpen: boolean
    data?: Outlet
    onOpen: (data: Outlet) => void
    onClose: () => void
}

const useDetailOutletModal = create<DetailOutletModalStore>((set) => ({
    isOpen: false,
    data: undefined,
    onOpen: (data: Outlet) => set({ isOpen: true, data }),
    onClose: () => set({ isOpen: false }),
}));


export default useDetailOutletModal;