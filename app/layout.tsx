import type { Metadata } from "next";
import { Inter, DM_Sans } from "next/font/google";
import "./globals.css";

// const inter = Inter({ subsets: ["latin"] });
const inter = DM_Sans({ weight: '400', subsets: ['latin'] });

export const metadata: Metadata = {
  title: "ARNES SHUTTLE",
  description: "Siap menemani semangatmu",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="scroll-smooth">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
