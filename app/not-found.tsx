const NotFound = () => {
    return (
        <div className="h-screen flex flex-col items-center justify-center">
            <h1 className="mb-4 text-6xl font-semibold text-primary">404</h1>
            <p className="mb-4 text-lg text-primary font-semibold">Waduuh! Sepertinya kamu tersesat.</p>
            <div className="animate-bounce">
                <svg className="mx-auto h-16 w-16 text-primary" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path>
                </svg>
            </div>
            <p className="mt-4 text-gray-600">Mari kita kembalikan kamu <a href="/" className="text-blue-500">Beranda</a>.</p>
        </div>
    )
}

export default NotFound