"use client"
import { useEffect } from "react";

/* eslint-disable @next/next/no-img-element */
export default function Download() {
  useEffect(() => {
    const getMobileOperatingSystem = () => {
      const userAgent = navigator.userAgent || navigator.vendor;
      if (/android/i.test(userAgent)) {
        window.location.href = "https://play.google.com/store/apps/details?id=id.metech.arnes_shuttle&hl=id";
      } else if (/iPad|iPhone|iPod/.test(userAgent)) {
        window.location.href = "https://apps.apple.com/id/app/arnes-shuttle-official/id6630376255?l=id";
      }
    };
    getMobileOperatingSystem();
  }, []);
  return (
    <>
      <div className="container">
        <div className="flex gap-3 lg:flex-row flex-col justify-center items-center mt-[5%]">
          <a href="https://play.google.com/store/apps/details?id=id.metech.arnes_shuttle&hl=id" target="_blank">
            <img src="/images/component/GooglePlay/Light/English.svg" alt="Google Play" className="w-auto h-32 hover:-translate-y-1 duration-200 cursor-pointer" />
          </a>
          <a href="https://apps.apple.com/id/app/arnes-shuttle-official/id6630376255?l=id" target="_blank">
            <img src="/images/component/AppStore/Light/English.svg" alt="App Store" className="w-auto h-32 hover:-translate-y-1 duration-200 cursor-pointer" />
          </a>
        </div>
        <div className="text-center mt-3">
          Pesan tiket jadi lebih mudah dan cepat dengan Aplikasi kami. Unduh Sekarang!
        </div>
      </div>
    </>
  )
}