"use client"

import config from "@/project.config";
import useSWR from "swr";
import ErrorFetch from "../components/errorFetch";
import SkeletonContent from "../components/skeleton/skeletonContent";
import MainBanner from "../components/feature/mainBanner";

const About = () => {
    const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/about-us`)

    if (error) return (
        <ErrorFetch />
    )

    if (isLoading) return (
        <SkeletonContent />
    )

    return (
        <>
            {/* <div className="flex bg-[url('/sampul.jpg')] md:min-h-[300px] min-h-[150px] bottom-6">
                <div className="m-auto text-white md:text-4xl text-2xl italic tracking-widest">
                    Tentang Kami
                </div>
            </div> */}
            <MainBanner />
            <div className="container">
                <div className="bg-white p-5 rounded-lg shadow-md">
                    {data && (
                        <div className="flex flex-col text-sm text-justify gap-4 default-heading" dangerouslySetInnerHTML={{ __html: data.about_us.value }}></div>
                    )}
                </div>
            </div>
        </>
    )
}

export default About