"use client"
import { Button } from "@/components/ui/button";
import cookie from "@/lib/cookies";
import { GET, POST } from "@/lib/http";
import config from "@/project.config";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { Input } from "@/components/ui/input";
import { FaAddressBook, FaAddressCard, FaEnvelope, FaHome, FaMailBulk, FaPhone, FaUser } from "react-icons/fa";
import RiwayatTransaksi from "../../components/feature/account/riwayatTransaksi";
import Link from "next/link";
import { Skeleton } from "@/components/ui/skeleton";
import { toast } from "@/components/ui/use-toast";
import GoogleEmail from "../../components/googleEmail";
import useSWR from "swr";
import SelectOption from "@/components/selectOption";

interface responseProfileSchema {
  "name": string,
  "email": string,
  "phone": string,
  "point": number,
  "type": string,
  "avatar": string,
  "province": string,
  "regencies": string,
  "district": string,
  "villages": string,
}

const profileSchema = z.object({
  name: z.string().min(1, 'Masukan Nama'),
  email: z.string()
    .min(1, { message: "Mohon masukan email anda" })
    .email("Silahkan perbaiki Email anda"),
  // password: z.string().min(1, 'Mohon masukan password'),
  "province": z.string(),
  "province_id": z.string(),
  "regencies": z.string(),
  "regencies_id": z.string(),
  "district": z.string(),
  "district_id": z.string(),
  "villages": z.string(),
  "villages_id": z.string(),
});


export default function Page() {
  const [isEmailOpen, setisEmailOpen] = useState(false);
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const [profile, setProfile] = useState<responseProfileSchema>();
  const [isEdit, setIsEdit] = useState(false)
  const form = useForm<z.infer<typeof profileSchema>>({
    resolver: zodResolver(profileSchema),
    defaultValues: {
      name: "",
      email: "",
      // password: profile?.phone || "",
      "province": "",
      "province_id": "",
      "regencies": "",
      "regencies_id": "",
      "district": "",
      "district_id": "",
      "villages": "",
      "villages_id": "",
    },
  })
  const { register, handleSubmit, setValue, watch, getValues } = form;

  // Fetch location data using useSWR
  const province = watch("province_id");
  const regencies = watch("regencies_id");
  const district = watch("district_id");

  // Fetch location data using useSWR
  const { data: provinsi } = useSWR(config.apiUrl + "/v1/location/provinces");
  const { data: kabupaten, isLoading: kabupatenIsLoading } = useSWR(
    province ? config.apiUrl + `/v1/location/regencies/${province}` : null
  );
  const { data: kecamatan, isLoading: kecamatanIsLoading } = useSWR(
    regencies ? config.apiUrl + `/v1/location/districts/${regencies}` : null
  );
  const { data: desa, isLoading: desaIsLoading } = useSWR(
    district ? config.apiUrl + `/v1/location/villages/${district}` : null
  );
  const transformedProvinces = provinsi ? provinsi.provinces.map((e: any) => ({
    label: e.name,
    value: String(e.id)
  })) : [];
  const transformedKabupaten = kabupaten ? kabupaten.regencies.map((e: any) => ({
    label: e.name,
    value: String(e.id)
  })) : [];
  const transformedKecamatan = kecamatan ? kecamatan.districts.map((e: any) => ({
    label: e.name,
    value: String(e.id)
  })) : [];
  const transformedDesa = desa ? desa.villages.map((e: any) => ({
    label: e.name,
    value: String(e.id)
  })) : [];

  async function getProfile() {
    if (!cookie.getToken()) {
      router.push('/auth/login')
      return
    }
    try {
      const { data } = await GET("/v1/account/profile", {})
      if (data.status === "SUCCESS") {
        if (data.data.email === null || data.data.phone === null) {
          Logout();
          return
        }
        setProfile(data.data)
        setValue('name', data.data.name)
        setValue('email', data.data.email)
        setValue('province', data.data.province)
        setValue('province_id', data.data.province_id)
        setValue('district', data.data.district)
        setValue('district_id', data.data.district_id)
        setValue('regencies', data.data.regencies)
        setValue('regencies_id', data.data.regencies_id)
        setValue('villages', data.data.villages)
        setValue('villages_id', data.data.villages_id)
      }
    } catch (error) {

      Logout();
    }
  }



  async function onSubmit(values: z.infer<typeof profileSchema>) {
    const province = watch('province')
    const regencies = watch('regencies')
    const district = watch('district')
    const villages = watch('villages')
    const validation = []
    if (province === '') {
      validation.push('Provinsi')
    }
    if (regencies === '') {
      validation.push('Kabupaten')
    }
    if (district === '') {
      validation.push('Kecamatan')
    }
    if (villages === '') {
      validation.push('Desa')
    }
    if (validation.length !== 0) {
      toast({
        variant: 'default',
        title: 'Data tidak lengkap',
        description: 'Mohon lengkapi data ' + validation.join(', ')
      })
      setIsEdit(true)
      return
    }
    const { data } = await POST('/v1/account/update', values)
    if (data.status === "SUCCESS") {
      getProfile()
      setIsEdit(false)
      toast({ variant: 'success', title: 'Berhasil', description: 'Profil berhasil di perbarui' })
    } else {
      // alert(data.message)
      toast({
        variant: 'destructive',
        title: 'Gagal',
        description: data?.message
      })
    }
  }

  function update() {
    setIsEdit(!isEdit)
  }

  function Logout() {
    setLoading(true);
    cookie.deleteCookie();
    router.push('/')
    return
  }
  function toSentenceCase(str: string) {
    if (!str) return str;
    // return str[0].toUpperCase() + str.slice(1).toLowerCase();
    return str
      .toLowerCase()
      .split(' ')
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  useEffect(() => {
    getProfile()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="container mt-[3%]">
        <div className="flex flex-col lg:flex-row gap-3">
          <div className="border-2 p-5 w-full max-w-lg h-fit">
            {profile ?
              <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-2 justify-between w-full">
                  <div className="flex flex-col lg:flex-row gap-3 items-center">
                    <div className="w-24 h-24 flex items-center justify-center bg-gray-300 rounded-full">
                      <FaUser className="text-4xl" />
                    </div>
                    <div>
                      <div className="flex lg:flex-row flex-col gap-3 justify-between border-b-2 mb-2 w-full items-center pb-3">
                        <h1 className="text-2xl font-bold">
                          {isEdit ?
                            <FormField
                              control={form.control}
                              name="name"
                              render={({ field }) => (
                                <FormItem className="w-full">
                                  <FormControl>
                                    <Input type="text" className="xl:text-left text-center" placeholder="Nama" {...field} />
                                  </FormControl>
                                  <FormMessage className="text-xs" />
                                </FormItem>
                              )}
                            />
                            :
                            profile?.name || <Skeleton className="h-7 w-full rounded-lg" />
                          }
                        </h1>
                        <Link href={'/point/history'}>
                          <span className="hover:text-blue w-full lg:w-fit cursor-pointer duration-300 p-1 rounded-lg border-primary border-2 text-primary hover:bg-primary hover:text-white">Point : {profile?.point >= 0 ? profile?.point : <Skeleton className="h-7 w-16 rounded-lg" />}</span>
                        </Link>
                      </div>
                      <div className="flex flex-row gap-3 flex-wrap">
                        <span className="flex gap-3 items-center">
                          <FaEnvelope />
                          <div className="flex gap-3 items-center w-full">
                            {isEdit ?
                              <FormField
                                control={form.control}
                                name="email"
                                render={({ field }) => (
                                  <FormItem className="w-full">
                                    <FormControl>
                                      {/* <Input type="email" placeholder="Email" {...field} /> */}
                                      <GoogleEmail setIsOpen={setisEmailOpen} setValue={setValue} value={watch('email')} />
                                    </FormControl>
                                    <FormMessage className="text-xs" />
                                  </FormItem>
                                )}
                              />
                              :
                              profile?.email || <Skeleton className="h-7 w-full min-w-36 rounded-lg" />
                            }
                          </div>
                        </span>
                        <span className="flex gap-3 items-center">
                          <FaPhone /> {profile?.phone || <Skeleton className="h-7 w-full min-w-36 rounded-lg" />}
                        </span>
                      </div>

                    </div>
                  </div>
                  <span className="flex gap-3 items-center">
                    <FaHome />
                    <div className="flex gap-3 items-center w-full">
                      {isEdit ?
                        <div className="flex flex-col gap-3 w-full">
                          {/* <FormField
                            control={form.control}
                            name="province"
                            render={({ field: { onChange, value } }) => (
                              <FormItem className="w-full">
                                <FormControl>
                                  {provinsi && ( */}
                          <SelectOption
                            value={String(watch('province_id'))}
                            onValueChange={(e: any) => {
                              setValue('province_id', e);
                              setValue('province', e);
                              setValue('regencies', '')
                              setValue('regencies_id', '')
                              setValue('district_id', '');
                              setValue('district', '');
                              setValue('villages_id', '');
                              setValue('villages', '');
                            }}
                            data={transformedProvinces}
                            title={"Provinsi"}
                            className="w-full"
                          />
                          {/* )}
                                </FormControl>
                                <FormMessage className="text-xs" />
                              </FormItem>

                            )}
                          /> */}
                          {/* <FormField
                            control={form.control}
                            name="regencies"
                            render={({ field: { onChange, value } }) => (
                              <FormItem className="w-full">
                                <FormControl>
                                  {kabupaten && ( */}
                          {watch('province_id') && (
                            <SelectOption
                              value={String(watch('regencies_id'))}
                              onValueChange={(e: any) => {
                                setValue('regencies_id', e);
                                setValue('regencies', e);
                                setValue('district_id', '');
                                setValue('district', '');
                                setValue('villages_id', '');
                                setValue('villages', '');
                              }}
                              data={transformedKabupaten}
                              title={"Kabupaten"}
                              className="w-full"
                            />
                          )}
                          {/* )}
                                </FormControl>
                                <FormMessage className="text-xs" />
                              </FormItem>
                            )}
                          /> */}
                          {/* <FormField
                            control={form.control}
                            name="district"
                            render={({ field: { onChange, value } }) => (
                              <FormItem className="w-full">
                                <FormControl>
                                  {kecamatan && ( */}
                          {watch('regencies_id') && (
                            <SelectOption
                              value={String(watch('district_id'))}
                              onValueChange={(e: any) => {
                                setValue('district_id', e);
                                setValue('district', e);
                                setValue('villages_id', '');
                                setValue('villages', '');
                              }}
                              data={transformedKecamatan}
                              title={"Kecamatan"}
                              className="w-full"
                            />
                          )}
                          {/* )}
                                </FormControl>
                                <FormMessage className="text-xs" />
                              </FormItem>
                            )}
                          /> */}
                          {/* <FormField
                            control={form.control}
                            name="villages"
                            render={({ field: { onChange, value } }) => (
                              <FormItem className="w-full">
                                <FormControl>
                                  {desa && ( */}
                          {watch('district_id') && (
                            <SelectOption
                              value={String(watch('villages_id'))}
                              onValueChange={(e: any) => {
                                setValue('villages_id', e);
                                setValue('villages', e);
                              }}
                              data={transformedDesa}
                              title={"Desa"}
                              className="w-full"
                            />
                          )}
                          {/* )}
                                </FormControl>
                                <FormMessage className="text-xs" />
                              </FormItem>
                            )}
                          /> */}
                        </div>
                        :
                        `${toSentenceCase(profile?.villages)}, ${toSentenceCase(profile?.district)}, ${toSentenceCase(profile?.regencies)}, ${toSentenceCase(profile?.province)}` || <Skeleton className="h-7 w-full min-w-36 rounded-lg" />
                      }
                    </div>
                  </span>
                  <div className="flex flex-row gap-3 w-full">
                    <Button variant={"primary"} type={!isEdit ? "submit" : "button"} onClick={update} disabled={loading} className="w-full">{isEdit ? "Simpan Perubahan" : "Perbarui"}</Button>
                    <Button variant={"primary"} type="button" onClick={Logout} disabled={loading}>Logout</Button>
                  </div>
                </form>
              </Form>
              :
              <Skeleton className="h-24 w-full rounded-lg" />
            }
          </div>
          <RiwayatTransaksi />
        </div>
      </div>
    </>
  )
}