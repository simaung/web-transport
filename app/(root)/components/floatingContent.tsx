"use client"
import { FaMailBulk, FaPhoneAlt, FaTelegramPlane, FaTimes, FaWhatsapp } from "react-icons/fa";
import { useEffect, useState } from "react";
import axios from "axios";
import { getItem, removeItem, store } from "@/lib/localStorage";
import config from "@/project.config";

export default function FloatingContent() {
  const [isOpen, setIsOpen] = useState(false);
  const getWa = async () => {
    try {
      const { data } = await axios.get('https://baileys-bot.arnes.co.id/admin/getWaData')

      if (data) {
        store('waData', data);
      } else {
        removeItem('waData');
      }
    } catch (error) {
      removeItem('waData');
    }
  }
  const getWa2 = async () => {
    try {
      const { data } = await axios.get('https://baileys-bot-backup.arnes.co.id/admin/getWaData')

      if (data) {
        store('waData2', data);
      } else {
        removeItem('waData2');
      }
    } catch (error) {
      removeItem('waData2');
    }
  }

  useEffect(() => {
    getWa()
    getWa2()
  }, []);

  return (
    <div className="fixed bottom-5 right-0 z-40">
      <div className="flex flex-col gap-1">
        <a href={`https://wa.me/6281333505008?text=Halo Arnes, Saya mau reservasi...`} target="_blank" rel="noopener noreferrer" className={`shadow-sm shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transform transition-transform duration-300 ${isOpen ? "translate-x-0 opacity-100" : "translate-x-full opacity-0"
          }`}>
          <FaWhatsapp />
          <span>Whatsapp</span>
        </a>
        {
          getItem('waData') && (
            <a href={`https://wa.me/${getItem('waData')?.id?.split(':')?.[0]}?text=Halo Arnes, Saya mau reservasi...`} target="_blank" rel="noopener noreferrer" className={`shadow-sm shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transform transition-transform duration-300 ${isOpen ? "translate-x-0 opacity-100" : "translate-x-full opacity-0"
              }`}>
              <FaWhatsapp />
              <span>Whatsapp 1</span>
            </a>
          )
        }
        {
          getItem('waData2') && (
            <a href={`https://wa.me/${getItem('waData2')?.id?.split(':')?.[0]}?text=Halo Arnes, Saya mau reservasi...`} target="_blank" rel="noopener noreferrer" className={`shadow-sm shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transform transition-transform duration-300 ${isOpen ? "translate-x-0 opacity-100" : "translate-x-full opacity-0"
              }`}>
              <FaWhatsapp />
              <span>Whatsapp 2</span>
            </a>
          )
        }
        {/* <a href={`https://api.whatsapp.com/send/?phone=${config.contactService?.replaceAll(' ', "")?.replaceAll('-', "")?.replaceAll('+', '')}&text=Hai%2C+Saya+mau+bertanya%3F&type=phone_number&app_absent=0`} target="_blank" rel="noopener noreferrer" className={`shadow-sm shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transform transition-transform duration-300 ${isOpen ? "translate-x-0 opacity-100" : "translate-x-full opacity-0"
          }`}>
          <FaWhatsapp />
          <span>Customer Service</span>
        </a> */}
        <a href="https://t.me/ReservasiArnesBot" target="_blank" rel="noopener noreferrer" className={`shadow-sm shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transform transition-transform duration-300 ${isOpen ? "translate-x-0 opacity-100" : "translate-x-full opacity-0"}`}>
          <FaTelegramPlane />
          <span>Telegram</span>
        </a>
        <div
          className="shadow-md shadow-white bg-primary text-white rounded-l-full text-right cursor-pointer p-3 flex flex-row gap-2 items-center w-fit self-end transition-transform duration-300"
          onClick={() => setIsOpen(!isOpen)}
        >
          {isOpen ? <FaTimes /> : <FaPhoneAlt />}
        </div>
      </div>
    </div>
  );
}
