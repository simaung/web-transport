
import React, { useEffect, useState } from 'react';
import { GoogleMap, LoadScript, Marker, StandaloneSearchBox } from '@react-google-maps/api';


interface MapComponentProps {
  onLocationChange: (lat: number, lng: number) => void;
  center: { lat: number, lng: number };
}

const GoogleGeocoderMap: React.FC<MapComponentProps> = ({ onLocationChange, center }) => {
  console.log(center, ' center google');

  const [map, setMap] = useState<any>(null);
  // const [markerPosition, setMarkerPosition] = useState(center && (center));
  // console.log(markerPosition, ' marker Postition google');


  const onMapLoad = (map: any) => {
    setMap(map);
  };

  const onMarkerDragEnd = (e: any) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    // setMarkerPosition({ lat, lng });
    onLocationChange(lat, lng);
  };

  const onPlacesChanged = () => {
    const places = (document.getElementById('search-box') as HTMLInputElement).value;

    if (places && map) {
      const service = new google.maps.places.PlacesService(map);
      service.textSearch({ query: places }, (results: any, status: any) => {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          const location = results[0].geometry.location;
          // setMarkerPosition({ lat: location.lat(), lng: location.lng() });
          onLocationChange(location.lat(), location.lng());
        }
      });
    }
  };

  return (
    <LoadScript
      googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_KEY!}
      libraries={['places']}
    >
      <GoogleMap
        onLoad={onMapLoad}
        center={center}
        zoom={15}
        mapContainerClassName='rounded-lg'
        mapContainerStyle={{ height: '50vh', width: '100%' }}
      >
        <Marker
          position={center}
          draggable={true}
          onDragEnd={onMarkerDragEnd}
        />

        <StandaloneSearchBox
          onPlacesChanged={onPlacesChanged}
        >
          <input type="text"
            className="absolute top-2 left-2 w-full max-w-sm p-3 rounded-md outline-secondary border-2 border-gray-100"
            placeholder="Cari lokasi anda..."
            id="search-box"
            onChange={onPlacesChanged}
          />
        </StandaloneSearchBox>
      </GoogleMap>
    </LoadScript>
  );
};

export default GoogleGeocoderMap;



// // components/MapComponent.tsx
// import React, { useState, useRef, useCallback } from 'react';
// import { GoogleMap, LoadScript, Marker, StandaloneSearchBox, Libraries } from '@react-google-maps/api';

// interface GoogleGeocoderMapProps {
//   onLocationChange: (lat: number, lng: number) => void;
//   center: { lat: number, lng: number };
// }

// // const libraries = ['places'];

// const GoogleGeocoderMap: React.FC<GoogleGeocoderMapProps> = ({ onLocationChange, center }) => {
//   const [markerPosition, setMarkerPosition] = useState(center);
//   const searchBoxRef = useRef<any>(null);
//   const mapRef = useRef<any>(null);

//   const handleMarkerDragEnd = (event: google.maps.MapMouseEvent) => {
//     const lat = event.latLng?.lat();
//     const lng = event.latLng?.lng();
//     if (lat && lng) {
//       setMarkerPosition({ lat, lng });
//       onLocationChange(lat, lng);
//     }
//   };

//   const onPlacesChanged = () => {
//     const places = searchBoxRef.current.getPlaces();
//     if (places && places.length > 0) {
//       const place = places[0];
//       const location = place.geometry?.location;
//       if (location) {
//         const lat = location.lat();
//         const lng = location.lng();
//         setMarkerPosition({ lat, lng });
//         onLocationChange(lat, lng);
//         mapRef.current.panTo({ lat, lng });
//       }
//     }
//   };

//   return (
//     <LoadScript googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_KEY!} libraries={['places']}>
//       <GoogleMap
//         mapContainerStyle={{ height: '50vh', width: '100%' }}
//         center={markerPosition}
//         zoom={15}
//         ref={mapRef}
//       >
//         {/* <StandaloneSearchBox onLoad={(ref) => (searchBoxRef.current = ref)} onPlacesChanged={onPlacesChanged}>
//           <input
//             type="text"
//             placeholder="Search places"
//             style={{
//               boxSizing: `border-box`,
//               border: `1px solid transparent`,
//               width: `240px`,
//               height: `32px`,
//               marginTop: `27px`,
//               padding: `0 12px`,
//               borderRadius: `3px`,
//               boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
//               fontSize: `14px`,
//               outline: `none`,
//               textOverflow: `ellipses`,
//             }}
//           />
//         </StandaloneSearchBox> */}
//         <StandaloneSearchBox
//           onLoad={(ref) => (searchBoxRef.current = ref)}
//           onPlacesChanged={onPlacesChanged}
//         >
//           <input type="text"
//             className="absolute top-2 left-2 w-full max-w-sm p-3 rounded-md outline-secondary border-2 border-gray-100"
//             placeholder="Cari lokasi anda..."
//             id="search-box"
//           // onChange={onPlacesChanged}
//           />
//         </StandaloneSearchBox>

//         <Marker
//           position={markerPosition}
//           draggable={true}
//           onDragEnd={handleMarkerDragEnd}
//         />
//       </GoogleMap>
//     </LoadScript>
//   );
// };

// export default GoogleGeocoderMap;
