import { useEffect, useState } from "react";
import GoogleGeocoderMap from "../googleGeocoderMap";

export default function LongLatMapPicker({ isOpen, setIsOpen, setLocation, dataFor, center }: any) {
  function onLocationChange(lat: number, lng: number): void {
    setLocation(lat, lng, dataFor)
  }

  return (
    <>
      <div className={`${isOpen ? "fixed" : "hidden"} top-0 left-0 h-screen z-40 bg-white bg-opacity-20 backdrop-blur-sm flex items-center justify-center w-full`}>
        <div className="max-w-6xl p-5 bg-white rounded-md shadow-md w-full">
          <div className="flex flex-row justify-between items-center pb-1 mb-3 border-b-2">
            <span>Pilih Lokasi {dataFor}</span>
            <i className="fa fa-times cursor-pointer" onClick={() => setIsOpen(false)}></i>
          </div>
          <div className="h-[50vh]">
            <GoogleGeocoderMap onLocationChange={onLocationChange} center={center} />
          </div>
          <button className="w-full py-2 bg-primary text-white rounded-md mt-5 hover:font-bold" onClick={() => setIsOpen(false)}>Selesai</button>
        </div>
      </div>
    </>
  );
}
