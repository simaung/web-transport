import React, { useRef, useEffect, useState, useCallback } from 'react';
import * as maptilersdk from '@maptiler/sdk';
import "@maptiler/sdk/dist/maptiler-sdk.css";
import maplibregl from 'maplibre-gl';

export default function Maptiler({ listOutlet, dataOutlet, latLng }: any) {

  const mapContainer = useRef<HTMLDivElement | null>(null);
  const map = useRef<maptilersdk.Map | null>(null);
  const center = { lng: 107.51556194667586, lat: -6.660440389080933 };
  const [zoom] = useState(8);
  maptilersdk.config.apiKey = String(process.env.NEXT_PUBLIC_MAPTILER_KEY);

  // function mapFunction() {
  const mapFunction = useCallback(() => {
    // Create a single custom marker element
    const customMarkerElement: HTMLImageElement = document.createElement('img');
    customMarkerElement.src = '/images/component/point.png';
    customMarkerElement.style.width = '81px';
    customMarkerElement.style.height = '81px';

    if (map.current) return;

    if (mapContainer.current) {

      if (!dataOutlet) {
        map.current = new maptilersdk.Map({
          container: mapContainer.current,
          style: maptilersdk.MapStyle.STREETS,
          center: [center.lng, center.lat],
          zoom: zoom
        });

        if (listOutlet) {
          listOutlet?.map((e: any) => {
            e?.outlets.map((x: any) => {
              return new maptilersdk.Marker({ element: customMarkerElement.cloneNode(true) as HTMLImageElement })
                .setPopup(new maptilersdk.Popup().setHTML(`<div style="font-weight: bold;color: #690067;border-bottom: solid 1px;">${x.name}</div><p>${x.address}</p><a href="https://www.google.com/maps/search/?api=1&query=${x.latitude},${x.longitude}" target='_blank'>Buka Maps</a>`))
                .setLngLat([x.longitude, x.latitude])
                .addTo(map?.current!!);
            })
          });
        }
      } else if (dataOutlet) {
        map.current = new maptilersdk.Map({
          container: mapContainer.current,
          style: maptilersdk.MapStyle.STREETS,
          center: [dataOutlet.longitude, dataOutlet.latitude],
          zoom: 18
        });

        new maptilersdk.Marker({ element: customMarkerElement.cloneNode(true) as HTMLImageElement })
          .setPopup(new maplibregl.Popup().setHTML(`<div style="font-weight: bold;color: #690067;border-bottom: solid 1px;">${dataOutlet.name}</div><p>${dataOutlet.address}</p><a href="https://www.google.com/maps/search/?api=1&query=${dataOutlet.latitude},${dataOutlet.longitude}" target='_blank'>Buka Maps</a>`))
          .setLngLat([dataOutlet.longitude, dataOutlet.latitude])
          .addTo(map?.current);
      }

      if (latLng) {
        map.current = new maptilersdk.Map({
          container: mapContainer.current,
          style: maptilersdk.MapStyle.STREETS,
          center: [latLng.lng, latLng.lat],
          zoom: 18,
          // navigationControl: false,
          // scaleControl: false,
          // geolocateControl: false,
          // terrainControl: false,
          // fullscreenControl: false,
          // attributionControl: false,
          // forceNoAttributionControl: false
        });

        new maptilersdk.Marker({ element: customMarkerElement.cloneNode(true) as HTMLImageElement })
          // .setPopup(new maplibregl.Popup().setHTML(`<div style="font-weight: bold;color: #690067;border-bottom: solid 1px;">${dataOutlet.name}</div><p>${dataOutlet.address}</p><a href="https://www.google.com/maps/search/?api=1&query=${dataOutlet.latitude},${dataOutlet.longitude}" target='_blank'>Buka Maps</a>`))
          .setLngLat([latLng.lng, latLng.lat])
          .addTo(map?.current);
      }
    }
  }, [mapContainer, map, center.lat, center.lng, dataOutlet, listOutlet, zoom, latLng])
  useEffect(() => {
    mapFunction()
  }, [center.lng, center.lat, zoom, listOutlet, mapFunction, latLng]);

  return (
    <div ref={mapContainer} className="w-full h-full" />
  );
}
