import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

interface PaginateFooterProps {
  currentPage: number;
  lastPage: number;
  onPageChange: (page: number) => void;
}

export default function PaginateFooter({ currentPage, lastPage, onPageChange }: PaginateFooterProps) {
  const handlePrevPage = () => {
    if (currentPage > 1) {
      onPageChange(currentPage - 1);
    }
  };

  const handleNextPage = () => {
    if (currentPage < lastPage) {
      onPageChange(currentPage + 1);
    }
  };

  const getPaginationRange = () => {
    const totalButtons = 5;
    let start = Math.max(1, currentPage - Math.floor(totalButtons / 2));
    let end = Math.min(lastPage, start + totalButtons - 1);

    if (end - start + 1 < totalButtons) {
      start = Math.max(1, end - totalButtons + 1);
    }

    const pages = [];
    for (let i = start; i <= end; i++) {
      pages.push(i);
    }
    return pages;
  };

  const pages = getPaginationRange();

  return (
    <div className="flex justify-between items-center mt-3">
      <div
        className={`w-7 h-7 text-xs cursor-pointer rounded-full bg-gray-100 flex items-center justify-center hover:bg-primary duration-300 hover:text-white ${currentPage === 1 ? 'cursor-not-allowed' : ''}`}
        onClick={handlePrevPage}
      >
        <FaArrowLeft />
      </div>
      <div className="w-full flex items-center justify-center gap-1 overflow-x-auto">
        {pages.map((page) => (
          <div
            key={page}
            className={`w-7 h-7 text-sm cursor-pointer rounded-full flex items-center justify-center ${currentPage === page ? 'bg-primary text-white font-bold' : 'bg-gray-100 hover:bg-primary hover:text-white hover:font-bold duration-300'}`}
            onClick={() => onPageChange(page)}
          >
            {page}
          </div>
        ))}
      </div>
      <div
        className={`w-7 h-7 text-xs cursor-pointer rounded-full bg-gray-100 flex items-center justify-center hover:bg-primary duration-300 hover:text-white ${currentPage === lastPage ? 'cursor-not-allowed' : ''}`}
        onClick={handleNextPage}
      >
        <FaArrowRight />
      </div>
    </div>
  );
}
