/* eslint-disable react/no-unescaped-entities */

export default function BCA() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN BCA Virtual Account</h3>
      <ol>
        <li>
          <p className="mb-0">
            Pembayaran dapat dilakukan melalui <b>ATM ,Mobile Banking atau Internet Banking BCA</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Setelah menyetujui Syarat & Ketentuan dan mengklik tombol "Proses Selanjutnya", Anda akan mendapatkan
            <b>Nomor Virtual Account</b>.
          </p>
        </li>
        <li>
          <p className="mb-0">
            Catat <b>Nomor Virtual Account</b> dan <b>Nominal</b> yang tertera. </p>
        </li>
        <li>
          <p className="mb-0">
            Silahkan lihat panduan pembayaran menggunakan ATM, Mobile Banking atau Internet Banking. </p>
        </li>
        ATM
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transaksi Lainnya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transfer </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ke rekening BCA Virtual Account. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Benar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ambil bukti bayar anda </p>
          </li>
        </ul>
        Mobile Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Mobile Banking </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih m-Transfer &gt; BCA Virtual Account. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan nomor Virtual Account, contoh: 123XXXXXX dan pilih <b>Send.</b> </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Periksa informasi yang tertera di layar. Pastikan informasi dan total tagihan sudah benar. Jika benar pilih
              <b>Ya.</b>
            </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan PIN m-BCA Anda dan pilih OK. </p>
          </li>
        </ul>
        Internet Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Internet Banking. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transfer Dana </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transfer Ke BCA Virtual Account </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjutkan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Respon KeyBCA Appli 1 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
      </ol>
    </div>
  )
}