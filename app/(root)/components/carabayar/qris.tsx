
export default function QRIS() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN QRIS</h3>
      <ol>
        <li>
          <p className="mb-0">
            Anda harus terdaftar sebagai pengguna aplikasi uang elektronik, dompet elektronik, atau mobile banking yang
            mendukung <b>QRIS</b> </p>
        </li>
        <li>
          <p className="mb-0">
            Anda akan diarahkan ke halaman pembayaran <b>QRIS</b> </p>
        </li>
        <li>
          <p className="mb-0">
            Cek kembali nominal transaksi kamu, lalu scan QR Code menggunakan aplikasi uang elektronik, dompet elektronik,
            atau mobile banking yang mendukung <b>QRIS</b> </p>
        </li>
        <li>
          <p className="mb-0">
            Masukan PIN untuk melanjutkan transaksi </p>
        </li>
        <li>
          <p className="mb-0">
            Transaksi selesai </p>
        </li>
      </ol>
    </div>
  )
}