
export default function BNI() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN BNI Virtual Account</h3>
      <ol>
        <li>
          <p className="mb-0">
            Pembayaran dapat dilakukan melalui <b>ATM, SMS Banking, Mobile Banking atau Internet Banking BNI</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Anda akan mendapatkan <b>Nomor Virtual Account</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Catat <b>Nomor Virtual Account</b> dan <b>Nominal</b> yang tertera. </p>
        </li>
        <li>
          <p className="mb-0">
            Silahkan lihat panduan pembayaran menggunakan ATM, SMS Banking atau Internet Banking. </p>
        </li>
        ATM
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Lain </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transfer </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ke Rekening BNI </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nominal, misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ambil bukti bayar anda </p>
          </li>
        </ul>
        SMS Banking
        <ul>
          <li>
            <p className="mb-0">
              Masuk Aplikasi SMS Banking BNI. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transfer </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Trf Rekening BNI </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Jumlah Tagihan, misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Proses </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pada Pop Up Message, Pilih Setuju </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Anda Akan Mendapatkan Pesan Konfirmasi </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan 2 Angka Dari PIN Anda Dengan Mengikuti Petunjuk Yang Tertera Pada Pesan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
        Mobile Banking
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transfer </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Antar Rekening BNI </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Rekening Tujuan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Input Rekening Baru </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account sebagai Nomor Rekening, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjut </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nominal Tagihan. , misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjut </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Periksa Detail Konfirmasi. Pastikan Data Sudah Benar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Jika Sudah Benar, Masukkan Password Transaksi </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjut </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
        Internet Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Internet Banking. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transaksi </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Info dan Administrasi </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Atur Rekening Tujuan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Tambah Rekening Tujuan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Kode Booking Sebagai Nama Singkat, misal. GRE4545TTX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account Sebagai Nomor Rekening, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Lengkapi Semua Data Yang Diperlukan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjutkan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Kode Otentikasi Token lalu, Proses </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Rekening Tujuan Berhasil Ditambahkan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transfer </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Transfer Antar Rek. BNI </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Rekening Tujuan dengan Nama Singkat Yang Sudah Anda Tambahkan., misal. GRE4545TTX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nominal., misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Kode Otentikasi Token </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
      </ol>
    </div>
  )
}