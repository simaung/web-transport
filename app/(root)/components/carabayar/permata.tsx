
export default function PERMATA() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN PermataVirtual Account</h3>
      <ol>
        <li>
          <p className="mb-0">
            Pembayaran dapat dilakukan melalui <b>ATM, Mobile Banking atau Internet Banking Bank Permata</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Anda akan mendapatkan <b>Nomor Virtual Account</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Catat <b>Nomor Virtual Account</b> dan <b>Nominal</b> yang tertera. </p>
        </li>
        <li>
          <p className="mb-0">
            Silahkan lihat panduan pembayaran menggunakan ATM, Mobile Banking atau Internet Banking. </p>
        </li>
        ATM
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transaksi Lainnya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Pembayaran </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Pembayaran Lain-lain </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Virtual Account </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Select Benar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Select Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ambil bukti bayar anda </p>
          </li>
        </ul>
        Mobile Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Mobile Banking </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Pembayaran Tagihan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Virtual Account </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nominal, misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Token </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar akan ditampilkan </p>
          </li>
        </ul>
        Internet Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Internet Banking. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Pembayaran Tagihan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Virtual Account </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nominal, misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Token </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
      </ol>
    </div>
  )
}