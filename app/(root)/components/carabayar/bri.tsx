
export default function BRI() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN BRI Virtual Account</h3>
      <ol>
        <li>
          <p className="mb-0">
            Pembayaran dapat dilakukan melalui <b>ATM, Mobile Banking atau Internet Banking BRI</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Anda akan mendapatkan <b>Nomor Virtual Account</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Catat <b>Nomor Virtual Account</b> dan <b>Nominal</b> yang tertera. </p>
        </li>
        <li>
          <p className="mb-0">
            Silahkan lihat panduan pembayaran menggunakan ATM, Mobile Banking atau Internet Banking. </p>
        </li>
        ATM
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Transaksi Lain </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Pembayaran </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Lain-lain </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu BRIVA </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ke rekening BRI Virtual Account. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ambil bukti bayar anda </p>
          </li>
        </ul>
        Mobile Banking
        <ul>
          <li>
            <p className="mb-0">
              Login BRI Mobile </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Mobile Banking BRI </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Pembayaran </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu BRIVA </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nominal , misal. 10000 </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik OK </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan PIN Mobile </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar akan dikirim melalui sms </p>
          </li>
        </ul>
        Internet Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Internet Banking. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Pembayaran </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu BRIVA </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Nomor Virtual Account, misal. 123456789012XXXX </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan Password </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Masukkan mToken </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Kirim </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
      </ol>
    </div>
  )
}