
export default function MANDIRI() {
  return (
    <div className="card-body border-0 border-gray">
      <h3 className="uppercase">CARA PEMBAYARAN MENGGUNAKAN Mandiri Virtual Account</h3>
      <ol>
        <li>
          <p className="mb-0">
            Pembayaran dapat dilakukan melalui <b>ATM, Mobile Banking atau Internet Banking Bank Mandiri</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Anda akan mendapatkan <b>Nomor Virtual Account</b>. </p>
        </li>
        <li>
          <p className="mb-0">
            Catat <b>Nomor Virtual Account</b> dan <b>Nominal</b> yang tertera. </p>
        </li>
        <li>
          <p className="mb-0">
            Silahkan lihat panduan pembayaran menggunakan ATM, Mobile Banking atau Internet Banking. </p>
        </li>
        ATM
        <ul>
          <li>
            <p className="mb-0">
              Pilih Menu Bayar/Beli </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Lainnya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Multi Payment
            </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input 71002 (Ottopay) sebagai Kode Institusi </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 71002xxxxxxxxx </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Benar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Ya </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ambil bukti bayar anda </p>
          </li>
        </ul>
        Mobile Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Mobile Banking </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Bayar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Multi Payment </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Ottopay (71002) sebagai Penyedia Jasa </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 71002xxxxxxxxx </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Lanjut </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input OTP and PIN </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih OK </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
        Internet Banking
        <ul>
          <li>
            <p className="mb-0">
              Login Internet Banking. </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Bayar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Pilih Multi Payment </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Arnes (71002) sebagai Penyedia Jasa </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Input Nomor Virtual Account, misal. 71002xxxxxxxxx sebagai Kode Bayar </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Ceklis IDR </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Klik Lanjutkan </p>
          </li>
        </ul>
        <ul>
          <li>
            <p className="mb-0">
              Bukti bayar ditampilkan </p>
          </li>
        </ul>
      </ol>
    </div>
  )
}