"use client"
import { signInWithPopup } from "firebase/auth";
import { auth, provider } from "../../../firebase.config";
import { useState } from "react";
import { GET, POST } from "@/lib/http";
import cookie from "@/lib/cookies";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FaTimes } from "react-icons/fa";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";

interface profileShcema {
  "name": string,
  "email": string,
  "phone": string,
  "point": string,
  "type": string,
  "avatar": string
}

export default function GoogleEmail({ setIsOpen, setValue, value }: any) {
  const [userEmail, setUserEmail] = useState('');
  const [profile, setProfile] = useState<profileShcema>()
  const [modalKaitkanOpen, setModalKaitkanOpen] = useState(false);
  const searchParams = useSearchParams();
  const page = searchParams.get("page") || '/';
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [nomorTelepon, setNomorTelepon] = useState('');
  const [nomorLoading, setNomorLoading] = useState(false);
  const handleGoogleLogin = async () => {
    setValue('booking_contact.email', "");

    setLoading(true);
    try {
      const result = await signInWithPopup(auth, provider);
      const user = result.user;
      // setUserEmail(user?.email || '');
      setValue('booking_contact.email', user?.email);
      setValue('email', user?.email);
      // const idToken = await user.getIdToken();
      // const { data: userApi } = await POST('/v1/auth/firebase', { idToken });

      // if (userApi.status === "SUCCESS") {
      //   cookie.storeData(userApi.success.token);
      // } else {
      //   toast({
      //     variant: 'destructive',
      //     title: "Google Gagal",
      //     description: "sedang dalam perbaikan"
      //   })
      //   setLoading(false);
      // }
      // Redirect to home page or dashboard
      setLoading(false);
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  return (
    <>
      {loading ?
        // <button
        //   className="animate-pulse w-full rounded-lg flex flex-row items-center gap-3 justify-center shadow-md py-2 bg-gray-50 hover:-translate-y-1 duration-700" type="button"
        //   onClick={() => toast({
        //     variant: 'destructive',
        //     title: "Perhatian",
        //     description: 'Selesaikain login sebelumnya'
        //   })}
        // >
        //   <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="27" height="27" viewBox="0 0 48 48">
        //     <path fill="#FFC107" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#FF3D00" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4CAF50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1976D2" d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
        //   </svg>
        //   Loading...
        // </button>
        <Input
          placeholder={`Email`}
          value={'Loading...'}
          className="mb-3 cursor-pointer"
          readOnly
          onClick={() => {
            toast({
              variant: 'destructive',
              title: "Perhatian",
              description: 'Selesaikain login sebelumnya'
            });
            window.location.reload();
          }} />
        :
        <Input placeholder={`Email`} value={value} className="mb-3 cursor-pointer" readOnly onClick={handleGoogleLogin} />

        // <button
        //   className="w-full rounded-lg flex flex-row items-center gap-3 justify-center shadow-md py-2 bg-gray-50 hover:-translate-y-1 duration-700" type="button"
        //   onClick={handleGoogleLogin}
        // >
        //   <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="27" height="27" viewBox="0 0 48 48">
        //     <path fill="#FFC107" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#FF3D00" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4CAF50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1976D2" d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
        //   </svg>
        //   Login dengan google
        // </button>
      }
    </>
  )
}