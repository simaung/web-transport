import config from "@/project.config"
import Image from "next/image"
import Link from "next/link"
import { FaFacebook, FaInstagram, FaTwitter } from "react-icons/fa"
import AppDownload from "./feature/appDownload"

export const Footer = () => {
    return (
        <div className='bg-slate-900 text-primary mt-16 pb-5'>
            <div className="container">
                <div className='md:grid pt-3'>
                    <div className='flex md:flex-row md:gap-9 flex-col md:justify-between'>
                        <div className='w-full py-4'>
                            <div className='flex flex-row items-center'>
                                <Image src={'/images/logo/logo.png'} alt={'icon'} width={190} height={190} className="" />
                                {/* <span className='ml-2 mt-2 font-bold uppercase'>{config.appName}</span> */}
                            </div>
                            <p className='text-justify mt-4 text-sm text-muted-foreground'>
                                {/* Arnes Shuttle, layanan transportasi travel yang siap mengantarkan kita menjelajahi keindahan Jawa Barat dengan harga paling murah. */}
                                {config.footDesc1}
                            </p>
                            <p className='mt-4 text-sm text-muted-foreground'>
                                {/* Temukan kenyamanan perjalanan tanpa kompromi. */}
                                {config.footDesc2}
                            </p>
                            <AppDownload />
                        </div>
                        <div className='w-full sm:flex-col'>
                            <div className="space-y-2 text-sm py-4">
                                <p className="mt-2 mb-4 font-bold text-left">Kontak</p>
                                <div className="flex flex-col">
                                    <p className="text-sm text-muted-foreground">Whatsapp:</p>
                                    <a href={`https://api.whatsapp.com/send/?phone=${config.contactService?.replaceAll(' ', "")?.replaceAll('-', "")?.replaceAll('+', '')}&text=Hai%2C+Bolehkah+saya+bertanya%3F&type=phone_number&app_absent=0`} aria-label="Our phone" title="Our phone" className="transition-colors duration-300 text-sm text-muted-foreground hover:text-primary">{config.contactService}</a>
                                </div>
                                <div className="flex flex-col">
                                    <p className="text-sm text-muted-foreground">Email:</p>
                                    <a href={`mailto:${config.emailService}`} aria-label="Our email" title="Our email" className="transition-colors duration-300 text-sm text-muted-foreground hover:text-primary">{config.emailService}</a>
                                </div>
                                <div className="flex flex-col">
                                    <p className="text-sm text-muted-foreground">Alamat:</p>
                                    <a href={`${config.mapService}`} target="_blank" rel="noopener noreferrer" aria-label="Our address" title="Our address" className="transition-colors duration-300 text-sm text-muted-foreground hover:text-primary">
                                        {config.addressService}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className='w-full'>
                            <div className="space-y-2 text-sm py-4">
                                <p className="mt-2 mb-4 font-bold text-left">Sosial Media</p>
                                <div className="flex flex-row gap-2 text-muted-foreground">
                                    <Link href={`${config.twitterService}`} target="_blank" className='hover:text-primary transition duration-300'><FaTwitter size={25} /></Link>
                                    <Link href={`${config.instagramService}`} target="_blank" className='hover:text-primary transition duration-300'><FaInstagram size={25} /></Link>
                                    <Link href={`${config.facebookService}`} target="_blank" className='hover:text-primary transition duration-300'><FaFacebook size={25} /></Link>
                                </div>
                                <div className="flex flex-col">
                                    <p className="text-sm text-muted-foreground">
                                        {/* Dengan layanan unggulan yang kami sediakan, kami berkomitmen untuk menjadikan setiap momen perjalanan Anda istimewa. */}
                                        {config.footDesc3}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr className='mt-4 border-muted-foreground' />
                <div className='hidden md:grid'>
                    <div className='flex md:flex-row sm:flex-col md:justify-between'>
                        <div className='w-full text-left pt-6'>
                            <p className="text-xs text-muted-foreground">© {new Date().getFullYear()} {config.appName}. All rights reserved.</p>
                        </div>
                        <div className='w-full text-center pt-6 flex flex-row gap-6'>
                            <Link href={'/faq'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">F.A.Q</Link>
                            <Link href={'/policy'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">Kebijakan Privasi</Link>
                            <Link href={'/term'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">Syarat dan Ketentuan</Link>
                        </div>
                        <div className='w-full text-right pt-6'>
                            <Link href={'https://me-tech.id'} target="_blank" className="text-xs text-muted-foreground hover:text-primary transition duration-300">Powered By PT. Milenial Elite Teknologi</Link>
                        </div>
                    </div>
                </div>

                <div className='md:hidden text-center'>
                    <div className='flex flex-col w-full pt-2 justify-between gap-3'>
                        <Link href={'/faq'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">F.A.Q</Link>
                        <Link href={'/policy'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">Kebijakan Privasi</Link>
                        <Link href={'/term'} className="text-sm text-muted-foreground hover:text-primary transition duration-300">Syarat dan Ketentuan</Link>
                    </div>

                </div>
                <div className='md:hidden'>
                    <div className='w-full text-center pt-6'>
                        <p className="text-xs text-muted-foreground">© {new Date().getFullYear()} {config.appName}. All rights reserved.</p>
                    </div>
                    <div className='w-full text-center'>
                        <Link href={'https://me-tech.id'} target="_blank" className="text-xs text-muted-foreground hover:text-primary transition duration-300">Powered By PT. Milenial Elite Teknologi</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}