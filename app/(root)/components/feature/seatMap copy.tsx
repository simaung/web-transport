/* eslint-disable @next/next/no-img-element */
import { Checkbox } from '@/components/ui/checkbox'
import { SeatLayout } from '@/types/outlet'
import { FC, useEffect, useState } from 'react'
import { useSearchParams } from 'next/navigation'
import { toast } from "@/components/ui/use-toast"
import Seat from '@/components/ui/seat'

interface SeatMapProps {
    data: any,
    selectSeat: any,
    setSelectSeat: any,
    append: any,
    remove: any
}

const SeatMap: FC<SeatMapProps> = ({
    data,
    selectSeat,
    setSelectSeat,
    append,
    remove
}) => {
    const searchParams = useSearchParams();

    function handleClickKursi(id_seat: any, seat_name: any) {
        const isSeatSelected = selectSeat?.some((seat: any) => seat.id_seat === id_seat);
        const penumpang = parseInt(searchParams.get("pax")!);
        if (isSeatSelected) {
            const indexUpdatedSelectKursi = selectSeat.findIndex((e: any) => e.id_seat === id_seat)
            remove(indexUpdatedSelectKursi)
            const updatedSelectedKursi = selectSeat.filter((seat: any) => seat.id_seat !== id_seat);
            setSelectSeat(updatedSelectedKursi);
        } else {
            if (penumpang > selectSeat.length) {
                append({ name: '', phone: '', seat_number: seat_name })
                setSelectSeat([...selectSeat, { id_seat: id_seat, seat_number: seat_name }]);
            } else {
                toast({
                    variant: "destructive",
                    title: "Perhatian:",
                    description: 'Anda hanya boleh memilih ' + penumpang + ' Kursi',
                })
            }
        }
    }

    return (
        <div className='border-2 rounded-lg py-5 px-3 mt-3'>
            {data.seatmap.map((deck: any, i: any) => (
                <div key={i} className='flex flex-col'>
                    {
                        deck.map((seat: any, s: any) => (
                            <div className={`flex-row flex gap-4 items-center text-center my-2`} key={s}>
                                {seat.map((layout: SeatLayout, z: any) => (
                                    <div key={String(s) + String(z)} className='flex w-full items-center justify-center'>
                                        {layout.name == '-' ?
                                            (
                                                <>
                                                    <label
                                                        htmlFor={layout.id}
                                                        className={
                                                            "flex p-5 rounded-md w-full"
                                                        }
                                                    >
                                                        <div className="m-auto">&nbsp;</div>
                                                    </label>
                                                </>
                                            )
                                            :
                                            (
                                                <>
                                                    {layout.name == 'sp' ? (
                                                        // <label
                                                        //     htmlFor={layout.id}
                                                        //     className={
                                                        //         "flex p-5 bg-primary-foreground text-white rounded-md shadow-md cursor-not-allowed w-full"
                                                        //     }
                                                        // >
                                                        //     <div className="m-auto">Supir</div>
                                                        // </label>
                                                        <img src="/images/component/stir.svg" alt="..." />
                                                    ) :
                                                        (
                                                            <>
                                                                <div
                                                                    onClick={() =>
                                                                        layout?.status === "available" && (
                                                                            handleClickKursi(layout.id, layout.name)
                                                                        )
                                                                    }
                                                                    className='relative'
                                                                >
                                                                    <Seat state={layout.status === 'booked' || layout.status === 'block' ? "booked" : selectSeat?.some((seat: any) => seat.id_seat === layout.id) ? 'active' : 'available'} label={layout.name} />
                                                                </div>
                                                                {/* <div className="flex w-full h-full items-center justify-center"> */}
                                                                {/* </div> */}
                                                                {/* <label
                                                                        className='absolute top-1/2 left-1/2 bg-blue-50'
                                                                        // className={`
                                                                        // w-full
                                                                        // absolute
                                                                        // top-0
                                                                        // p-5 rounded-md peer-checked:bg-blue-500 dark:peer-checked:text-white shadow-md
                                                                        // ${layout.status == 'available' ? 'cursor-pointer hover:bg-primary hover:text-white' : 'cursor-not-allowed bg-secondary'}
                                                                        // ${selectSeat?.some((seat: any) => seat.id_seat === layout.id) ? 'bg-primary text-white' : ''}
                                                                        // `}
                                                                        onClick={() => handleClickKursi(layout.id, layout.name)}
                                                                    >
                                                                        <div className="m-auto">{layout.name}</div>
                                                                    </label> */}
                                                                {/* <Checkbox id={layout.id} className='hidden peer' disabled={layout.status != 'available'} /> */}
                                                            </>
                                                        )
                                                    }

                                                </>
                                            )}
                                    </div>
                                ))}
                            </div>
                        ))
                    }
                </div>
            ))
            }
        </div >
    )
}

export default SeatMap
