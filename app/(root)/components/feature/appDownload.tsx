/* eslint-disable @next/next/no-img-element */
export default function AppDownload() {
  return (
    <>
      <div className="mt-3">
        <div className="flex flex-row  w-full items-center justify-start gap-3">
          <a href="https://play.google.com/store/apps/details?id=id.metech.arnes_shuttle&hl=id" target="_blank">
            <img src="/images/component/GooglePlay/Light/English.svg" alt="Google Play" className="w-auto h-11 hover:-translate-y-1 duration-200 cursor-pointer" />
          </a>
          <a href="https://apps.apple.com/id/app/arnes-shuttle-official/id6630376255?l=id" target="_blank">
            <img src="/images/component/AppStore/Light/English.svg" alt="App Store" className="w-auto h-11 hover:-translate-y-1 duration-200 cursor-pointer" />
          </a>
        </div>
        {/* <div className="text-center w-full text-xs mt-3 italic">
          Download Aplikasi
        </div> */}
      </div>
    </>
  )
}