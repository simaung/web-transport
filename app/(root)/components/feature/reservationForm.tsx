"use client"

import { Button } from "@/components/ui/button"
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { groupingDropPoint, groupingOutlet } from "@/lib/grouping"
import config from "@/project.config"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { FaSearch } from "react-icons/fa"
import useSWR from "swr"
import { z } from "zod"
import ErrorFetch from "../errorFetch"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { CalendarIcon } from "@radix-ui/react-icons"
import { Calendar } from "@/components/ui/calendar"
import { format } from "date-fns"
import { cn } from "@/lib/utils"
import { Skeleton } from "@/components/ui/skeleton"
import SelectOutlet from "@/components/selectOutlet"
import useArrivalSelect from "@/hooks/useArrivalSelect"
import { useRouter } from "next/navigation"
import SelectOption from "@/components/selectOption"
import LoadingButton from "@/components/LoadingButton"
import { useState } from "react"
import DateRangePicker from "@/components/DateRangePicker"
import { toast } from "@/components/ui/use-toast"
import { Checkbox } from "@/components/ui/checkbox"

const reservationFormSchema = z.object({
    depart: z.string().min(1, 'Outlet keberangkatan tidak boleh kosong'),
    arrival: z.string().min(1, 'Outlet Tujuan tidak boleh kosong'),
    dateDepart: z.date({
        required_error: "Tanggal tidak boleh kosong",
    }),
    returnDate: z.date().optional(),
    pax: z.number().gte(1, 'Jumlah Penumpang tidak boleh kurang dari 1'),
})

let pax = [];
for (let i: number = 1; i <= Number(config.maxPax); i++) {
    pax.push({ label: `${i} Penumpang`, value: i })
}

let today = new Date();
today.setDate(today.getDate() - 1)

const ReservationForm = () => {
    const router = useRouter()
    const [isLoading, setIsLoading] = useState(false)
    const arrivalSelect = useArrivalSelect()
    const [isRoundTrip, setIsRoundTrip] = useState(false)

    const { data: departData, error: departError, isLoading: departIsLoading } = useSWR(`${config.apiUrl}/v1/outlets`)
    const { data: arrivalData, error: arrivalError, isLoading: arrivalIsLoading } = useSWR(`${config.apiUrl}/v1/routes/${arrivalSelect.departCode}`)
    const form = useForm<z.infer<typeof reservationFormSchema>>({
        resolver: zodResolver(reservationFormSchema),
        defaultValues: {
            depart: "",
            arrival: "",
            dateDepart: new Date(),
            pax: 1,
        },
    })

    if (departError) return (
        <ErrorFetch />
    )

    var outlets = departData ? groupingOutlet(departData.points) : [];
    var arrivalPoint = arrivalData ? groupingDropPoint(arrivalData.routes) : [];

    function onSubmit(values: z.infer<typeof reservationFormSchema>) {
        if (isRoundTrip && !values?.returnDate) {
            toast({
                variant: 'warning',
                title: 'Lengkapi Data!',
                description: 'Mohon pilih tanggal pulang'
            })
            return
        }
        if (values.dateDepart >= values?.returnDate!) {
            toast({
                variant: 'warning',
                title: 'Data tidak sesuai',
                description: 'Tanggal berangkat tidak boleh lebih besar dari tanggal pulang'
            })
            return
        }
        setIsLoading(true);
        // const paramQuery = `depart=${values.depart}&arrival=${values.arrival}&date=${format(values.dateDepart, "y-MM-dd")}`
        const paramQuery = `depart=${values.depart}&arrival=${values.arrival}&date=${format(values.dateDepart, "y-MM-dd")}&pax=${values.pax}${values?.returnDate ? "&return-date=" + format(values.returnDate, "y-MM-dd") : ""}`
        router.push("/schedule?" + paramQuery)
    }

    return (
        <div className="container z-10 -mt-14">
            <div className="flex flex-row text-sm mb-3 justify-end">
                {/* <div onClick={() => setIsRoundTrip(false)} className={`cursor-pointer p-3 rounded-l-lg border-2 border-r-0 lg:w-fit w-full ${!isRoundTrip ? "bg-primary text-white underline-offset-4 underline" : "bg-white"}`}>
                    Sekali Jalan
                </div> */}
                <div onClick={() => setIsRoundTrip(!isRoundTrip)} className={`flex items-center cursor-pointer p-3 rounded-lg border-2 lg:w-fit w-full text-white ${isRoundTrip ? "bg-green-500" : "bg-primary"}`}>
                    <div className="flex flex-row items-center gap-1">
                        <Checkbox checked={isRoundTrip} className="bg-white" />
                        <span>
                            Pulang Pergi
                        </span>
                    </div>
                </div>
            </div>
            <div className="bg-white shadow-lg rounded-lg p-5">
                {departData ? (
                    <Form {...form}>
                        <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col lg:flex-row gap-2 justify-between w-full">
                            <FormField
                                control={form.control}
                                name="depart"
                                render={({ field: { onChange, value } }) => (
                                    <FormItem className="w-full">
                                        <FormControl>
                                            {departData && (
                                                <SelectOutlet
                                                    value={value}
                                                    onValueChange={onChange}
                                                    data={outlets}
                                                    title={"Outlet Keberangkatan"}
                                                    className="w-full"
                                                />
                                            )}
                                        </FormControl>
                                        <FormMessage className="text-xs" />
                                    </FormItem>
                                )}
                            />
                            {/* ini di gunakan untuk menukaar outlet keberangkatan dan tujuan */}
                            {/* <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 512 512"
                                className="w-5 lg:w-16 hover:animate-spin cursor-pointer self-center"
                                onClick={() => {
                                    // tambahkan function untuk menukar outlet berangkat dan tujuan
                                    console.log('clicked');
                                }}>
                                <path d="M142.9 142.9c-17.5 17.5-30.1 38-37.8 59.8c-5.9 16.7-24.2 25.4-40.8 19.5s-25.4-24.2-19.5-40.8C55.6 150.7 73.2 122 97.6 97.6c87.2-87.2 228.3-87.5 315.8-1L455 55c6.9-6.9 17.2-8.9 26.2-5.2s14.8 12.5 14.8 22.2l0 128c0 13.3-10.7 24-24 24l-8.4 0c0 0 0 0 0 0L344 224c-9.7 0-18.5-5.8-22.2-14.8s-1.7-19.3 5.2-26.2l41.1-41.1c-62.6-61.5-163.1-61.2-225.3 1zM16 312c0-13.3 10.7-24 24-24l7.6 0 .7 0L168 288c9.7 0 18.5 5.8 22.2 14.8s1.7 19.3-5.2 26.2l-41.1 41.1c62.6 61.5 163.1 61.2 225.3-1c17.5-17.5 30.1-38 37.8-59.8c5.9-16.7 24.2-25.4 40.8-19.5s25.4 24.2 19.5 40.8c-10.8 30.6-28.4 59.3-52.9 83.8c-87.2 87.2-228.3 87.5-315.8 1L57 457c-6.9 6.9-17.2 8.9-26.2 5.2S16 449.7 16 440l0-119.6 0-.7 0-7.6z" />
                            </svg> */}
                            <FormField
                                control={form.control}
                                name="arrival"
                                render={({ field: { onChange, value } }) => (
                                    <FormItem className="w-full">
                                        <FormControl>
                                            <>
                                                {arrivalIsLoading && (
                                                    <SelectOutlet
                                                        value={value}
                                                        onValueChange={onChange}
                                                        data={[]}
                                                        title={"Loading..."}
                                                        disabled={true}
                                                        className="w-full"
                                                    />
                                                )}
                                                {arrivalData && (
                                                    <SelectOutlet
                                                        value={value}
                                                        onValueChange={onChange}
                                                        data={arrivalPoint}
                                                        title={"Outlet Tujuan"}
                                                        disabled={arrivalSelect.isDisabled}
                                                        className="w-full"
                                                    />
                                                )}
                                            </>

                                        </FormControl>
                                        <FormMessage className="text-xs" />
                                    </FormItem>
                                )}
                            />
                            <FormField
                                control={form.control}
                                name="dateDepart"
                                render={({ field }) => (
                                    <FormItem className="flex flex-col w-full">
                                        <Popover>
                                            <PopoverTrigger asChild>
                                                <FormControl>
                                                    <Button
                                                        variant={"outline"}
                                                        className={cn(
                                                            "pl-3 text-left font-normal text-md min-w-60",
                                                            !field.value && "text-muted-foreground"
                                                        )}
                                                    >
                                                        {field.value ? (
                                                            format(field.value, "dd/MM/y")
                                                        ) : (
                                                            <span>Tanggal Keberangkatan</span>
                                                        )}
                                                        <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                    </Button>
                                                </FormControl>
                                            </PopoverTrigger>
                                            <PopoverContent className="w-auto p-0" align="start">
                                                <Calendar
                                                    mode="single"
                                                    selected={field.value}
                                                    onSelect={field.onChange}
                                                    disabled={(date) =>
                                                        date < today
                                                    }
                                                    initialFocus
                                                />
                                            </PopoverContent>
                                        </Popover>
                                        <FormMessage className="text-xs" />
                                    </FormItem>
                                )}
                            />
                            {isRoundTrip && (
                                <>
                                    <FormField
                                        control={form.control}
                                        name="returnDate"
                                        render={({ field }) => (
                                            <FormItem className="flex flex-col w-full">
                                                <Popover>
                                                    <PopoverTrigger asChild>
                                                        <FormControl>
                                                            <Button
                                                                variant={"outline"}
                                                                className={cn(
                                                                    "pl-3 text-left font-normal text-md min-w-60",
                                                                    !field.value && "text-muted-foreground"
                                                                )}
                                                            >
                                                                {field.value ? (
                                                                    format(field.value, "dd/MM/y")
                                                                ) : (
                                                                    <span>Tanggal Pulang</span>
                                                                )}
                                                                <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                            </Button>
                                                        </FormControl>
                                                    </PopoverTrigger>
                                                    <PopoverContent className="w-auto p-0" align="start">
                                                        <Calendar
                                                            mode="single"
                                                            selected={field.value}
                                                            onSelect={field.onChange}
                                                            disabled={(date) =>
                                                                date < today
                                                            }
                                                            initialFocus
                                                        />
                                                    </PopoverContent>
                                                </Popover>
                                                <FormMessage className="text-xs" />
                                            </FormItem>
                                        )}
                                    />
                                </>
                            )}
                            <FormField
                                control={form.control}
                                name="pax"
                                render={({ field: { onChange, value } }) => (
                                    <FormItem className="w-full">
                                        <FormControl>
                                            <SelectOption
                                                value={value}
                                                onValueChange={onChange}
                                                data={pax}
                                                title={"Jumlah Penumpang"}
                                                className="w-full"
                                            />
                                        </FormControl>
                                        <FormMessage className="text-xs" />
                                    </FormItem>
                                )}
                            />
                            {isLoading ?
                                <LoadingButton />
                                :
                                <Button variant="primary" type="submit" className="gap-2 w=full"><FaSearch />Cek</Button>
                            }
                        </form>
                    </Form>
                ) : (
                    <div className="flex flex-col lg:flex-row gap-2">
                        <Skeleton className="h-[43px] w-full rounded-lg" />
                        <Skeleton className="h-[43px] w-full rounded-lg" />
                        <Skeleton className="h-[43px] w-full rounded-lg" />
                        {/* <Skeleton className="h-[43px] w-full rounded-lg" /> */}
                        <Skeleton className="h-[43px] w-72 rounded-lg" />
                    </div>
                )}
            </div>
        </div>
    )
}

export default ReservationForm