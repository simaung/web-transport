import { Button } from '@/components/ui/button';
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from '@/components/ui/card';
import LoadingButton from '@/components/LoadingButton';
import { FaAngleDown, FaCarSide, FaCircle } from 'react-icons/fa';
import moment from "moment";
import { timeDuration } from '@/lib/timeDuration';
import { useRouter } from 'next/navigation';
import { CourseType, ScheduleType } from '@/types/outlet';
import { useState } from 'react';

import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@/components/ui/accordion';

interface ListScheduleProps {
  schedule: ScheduleType;
  dataQuery: any;
  isLoadingButton: any;
  setisLoadingButton: any;
}

const ListSchedule: React.FC<ListScheduleProps> = ({ schedule, dataQuery, isLoadingButton, setisLoadingButton }) => {
  const [ruteShow, setRuteShow] = useState(false);
  const router = useRouter();
  const selectSchedule = (tripCode: string, departure_time: string) => {
    // setisLoadingButton(true);
    const timeFormat = moment(departure_time, 'HH:mm:').format('HH:mm');
    // disini set untuk looping memilih jadwal berangkat dan jadwal pulang setelah klik pilih
    if (!dataQuery?.return_date) {
      setisLoadingButton(true);
      const paramQuery = `time=${timeFormat}&tripcode=${tripCode}&depart=${dataQuery.pickup_point}&arrival=${dataQuery.dropoff_point}&date=${dataQuery.departure_date}&pax=${dataQuery.pax}${dataQuery?.return_date ? "&return-date=" + dataQuery?.return_date : ""}${dataQuery?.booking_code ? "&booking_code=" + dataQuery?.booking_code : ""}`
      router.push("/book?" + paramQuery)
      return
    }
    if (dataQuery?.return_date && !dataQuery.tripcode) {
      const paramQuery = `time=${timeFormat}&tripcode=${tripCode}&depart=${dataQuery.pickup_point}&arrival=${dataQuery.dropoff_point}&date=${dataQuery.departure_date}&pax=${dataQuery.pax}${dataQuery?.return_date ? "&return-date=" + dataQuery?.return_date : ""}`
      router.push("/schedule?" + paramQuery)
      // setisLoadingButton(true);
      return
    }
    if (dataQuery?.return_date && dataQuery.tripcode) {
      setisLoadingButton(true);
      const paramQuery = `time=${dataQuery.time}&tripcode=${dataQuery.tripcode}&depart=${dataQuery.pickup_point}&arrival=${dataQuery.dropoff_point}&date=${dataQuery.departure_date}&pax=${dataQuery.pax}${dataQuery?.return_date ? "&return-date=" + dataQuery?.return_date : ""}&return-time=${timeFormat}&return-tripcode=${tripCode}`
      router.push("/book?" + paramQuery)
      return
    }
  }
  function diff_hours(etd: string, eta: string) {
    const dateTimePatern = 'YYYY-MM-DD HH:mm:ss'
    var d1 = moment(dataQuery?.departure_date, dateTimePatern).format('YYYY-MM-DD');
    var d2 = moment(dataQuery?.departure_date, dateTimePatern).format('YYYY-MM-DD');
    if (eta < etd) {
      d1 = moment(dataQuery?.departure_date, dateTimePatern).add(-1, 'day').format('YYYY-MM-DD')
    }
    const startDate = moment(d1 + ' ' + etd, dateTimePatern).format(dateTimePatern)
    const endDate = moment(d2 + ' ' + eta, dateTimePatern).format(dateTimePatern)
    return timeDuration(startDate, endDate);
  }

  let rupiah = new Intl.NumberFormat('id-ID', {
    style: 'decimal',
    currency: 'IDR',
  });

  return (
    <>
      <Card className="mb-2" key={schedule.tripcode}>
        <CardHeader>
          <CardTitle className='grid grid-cols-2'>
            <div>
              {schedule.departure_time.substring(0, 5)} WIB
            </div>
            <div className='text-right'>
              {schedule.price_discount !== 0 ?
                <>
                  <span className='text-sm text-red-500 line-through italic'>Rp. {rupiah.format(schedule.price)}</span>
                  <br />
                  Rp. {rupiah.format(schedule.price_discount)}
                </>
                :
                <>
                  Rp. {rupiah.format(schedule.price)}
                </>
              }
            </div>
          </CardTitle>
          <CardDescription>
            Estimasi {diff_hours(schedule.departure_time, schedule.arrival_time)}
            {/* Jam Kedatangan {schedule.arrival_time.substring(0, 5)} WIB */}
          </CardDescription>
        </CardHeader>
        <CardContent className='grid grid-cols-2 pb-3'>
          {schedule.remaining_seat < 1 ? (
            <div>
              Kursi Habis
            </div>
          ) : (
            <div>
              <div className='text-sm'>{schedule?.vessel_name}</div>
              <div className="text-sm text-gray-400">Tersedia {schedule.remaining_seat} Kursi</div>
            </div>
          )}
          < div className='text-right'>
            {schedule.remaining_seat < dataQuery.pax ? (
              <Button variant={"primary"} className='w-28' disabled>Pilih</Button>
            ) : (
              isLoadingButton ?
                <LoadingButton />
                :
                <Button variant={"primary"} className='w-28' onClick={(e) => selectSchedule(schedule.tripcode, schedule.departure_time)}>Pilih</Button>
            )}
          </div>
        </CardContent>
        <Accordion
          type="single"
          collapsible className="w-full"
          defaultValue={ruteShow ? "item" : ""}
        >
          <AccordionItem value="item">
            <AccordionTrigger className='px-5 py-0 pb-3 text-sm flex flex-row justify-center gap-1 text-primary' onClick={() => { setRuteShow(!ruteShow) }}>Rute Perjalanan</AccordionTrigger>
            <AccordionContent>
              <div className={`flex flex-col text-sm`}>
                {schedule?.course?.map((e: CourseType, i: number) => {
                  return (
                    <>
                      <div key={'book' + i}>
                        <div className='flex flex-wrap justify-start px-5'>
                          <div className='bg-secondary p-3 rounded-full w-10 h-10 text-white'>
                            <FaCarSide size={15} />
                          </div>
                          <div className={`${dataQuery?.pickup_point === e?.point_id || dataQuery?.dropoff_point === e?.point_id ? 'font-semibold text-primary' : 'text-gray-400'} ml-2`}>
                            <p>{e?.point_name}</p>
                            <p>{e?.departure_hour} WIB</p>
                          </div>
                        </div>
                        <div className={` ${schedule.course.length - i > 1 ? 'border-l-2 border-gray-300 min-h-5 ml-10' : ''}`}></div>
                      </div>
                    </>
                  )
                })}
              </div>
            </AccordionContent>
          </AccordionItem>
        </Accordion>
      </Card>
    </>
  )
}

export default ListSchedule;