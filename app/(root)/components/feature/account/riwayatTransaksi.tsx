import { GET } from "@/lib/http";
import config from "@/project.config";
import { useCallback, useEffect, useState } from "react";
import moment from "moment";
import 'moment/locale/id';
import Link from "next/link";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";
import SelectInput from "@/components/selectInput";
import PaginateFooter from "../../paginateFooter";
import { Skeleton } from "@/components/ui/skeleton";
import { toast } from "@/components/ui/use-toast";

interface detailTransaction {
  "id": string,
  "booking_code": string,
  "depart_at": string,
  "etd": string,
  "transaction_status": string,
  "payment_status": string,
  "orderer_name": string,
  "orderer_phone": string,
  "pax": number,
  "order_from": string,
  "expired_at": string,
  "total_price": number,
  "booking_date": string,
  "depart": {
    "code": string,
    "name": string,
    "regencies": string,
    "address": string,
    "contact": string,
    "open_time": string,
    "close_time": string,
    "latitude": string,
    "longitude": string,
    "link_maps": string,
  },
  "arrival": {
    "code": string,
    "name": string,
    "regencies": string,
    "address": string,
    "contact": string,
    "open_time": string,
    "close_time": string,
    "latitude": string,
    "longitude": string,
    "link_maps": null
  },
  "transaction_detail": [
    {
      "passenger_name": string,
      "passenger_seat": number,
      "passenger_ticket": string,
      "passenger_price": number
    }
  ]
}

interface responseTransactionSchema {
  "current_page": number,
  "data": detailTransaction[],
  "first_page_url": string,
  "from": number,
  "next_page_url": string,
  "path": string,
  "per_page": string,
  "prev_page_url": null,
  "to": number
}

export default function RiwayatTransaksi() {
  const [transaction, setTransaction] = useState<responseTransactionSchema | null>(null);
  const [pagination, setPagination] = useState({
    perPage: 5,
    page: 1,
    status: ''
  });
  const [paginationResponse, setPaginationResponse] = useState({
    lastPage: 0,
    page: 0
  });

  const getTransaction = useCallback(async () => {
    try {
      const { data } = await GET(config.apiUrl + "/v1/transaction", pagination);
      if (data.status === "SUCCESS") {
        setTransaction(data.transaction);
        setPaginationResponse({
          lastPage: data.transaction.last_page,
          page: data.transaction.current_page
        });
      }
    } catch (error) {
      toast({
        variant: 'default',
        title: 'Terdapat Kesalahan Pada Token',
        description: "anda di kembalikan ke main menu"
      })
    }
  }, [pagination]);

  useEffect(() => {
    getTransaction();
  }, [getTransaction, pagination]);

  const handleStatusChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setPagination({ ...pagination, [e.target.name]: e.target.value, page: 1 });
  };

  const handlePageChange = (newPage: number) => {
    setPagination({ ...pagination, page: newPage });
  };

  // if (!transaction) {
  //   return (
  //     <div className="border-2 p-5 w-full h-96 bg-gray-100 flex items-center justify-center animate-pulse">
  //       Loading...
  //     </div>
  //   );
  // }

  return (
    <div className="border-2 p-5 w-full">
      <div className="flex flex-col md:flex-row md:justify-between border-b-2 pb-1 md:items-center">
        <h1 className="font-bold text-lg">Riwayat Transaksi</h1>
        <div className="flex justify-between">
          <select className="w-fit outline-none text-xs p-1 focus:border-primary border-2" name="status" onChange={handleStatusChange} defaultValue="">
            <option value="">Semua Transaksi</option>
            <option value="ticketed">Lunas</option>
            <option value="book">Dipesan</option>
            <option value="cancel">Dibatalkan</option>
          </select>
          <select className="w-fit outline-none text-xs p-1 focus:border-primary border-2 text-center" name="perPage" onChange={handleStatusChange} defaultValue="">
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
        </div>
      </div>
      <div className="flex flex-col gap-3 mt-3">
        {transaction ?
          transaction.data.map((e: detailTransaction) => (
            <>
              <Link href={'/reservation?kode=' + e.booking_code}>
                <div key={e.id} className="border-2 rounded-lg p-3 flex flex-row justify-between flex-wrap">
                  <div>
                    <h2 className="font-bold text-primary">
                      {e.depart.name} - {e.arrival.name}
                    </h2>
                    <h5 className="font-bold md:hidden block">
                      {e.booking_code}
                    </h5>
                    <h5>
                      {e.pax} Penumpang
                    </h5>
                    {moment(e.depart_at, 'YYYY-MM-DD').locale('id').format('dddd, DD MMMM YYYY')}
                    <br />
                    Pukul {moment(e.etd, 'HH:mm:ss').locale('id').format('HH:mm')} WIB
                    {e.transaction_status === 'ticketed' && (
                      <div className="py-1 px-3 bg-green-500 text-center text-white rounded-lg font-bold text-xs w-fit md:hidden block">
                        Lunas
                      </div>
                    )}
                    {e.transaction_status === 'book' && (
                      <div className="py-1 px-3 bg-blue-500 text-center text-white rounded-lg font-bold text-xs w-fit md:hidden block">
                        Dipesan
                      </div>
                    )}
                    {e.transaction_status === 'cancel' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit md:hidden block">
                        Dibatalkan
                      </div>
                    )}
                    {e.transaction_status === 'expired' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit md:hidden block">
                        Dibatalkan Sistem
                      </div>
                    )}
                    {e.transaction_status === 'unseat' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit md:hidden block">
                        Tidak Dapat Kursi
                      </div>
                    )}
                  </div>
                  <div className="items-end flex-col hidden md:flex">
                    <h5 className="font-bold">
                      {e.booking_code}
                    </h5>
                    {e.transaction_status === 'ticketed' && (
                      <div className="py-1 px-3 bg-green-500 text-center text-white rounded-lg font-bold text-xs w-fit">
                        Lunas
                      </div>
                    )}
                    {e.transaction_status === 'book' && (
                      <div className="py-1 px-3 bg-blue-500 text-center text-white rounded-lg font-bold text-xs w-fit">
                        Dipesan
                      </div>
                    )}
                    {e.transaction_status === 'cancel' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit">
                        Dibatalkan
                      </div>
                    )}
                    {e.transaction_status === 'expired' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit">
                        Dibatalkan Sistem
                      </div>
                    )}
                    {e.transaction_status === 'unseat' && (
                      <div className="py-1 px-3 bg-red-500 text-center text-white rounded-lg font-bold text-xs w-fit">
                        Tidak Dapat Kursi
                      </div>
                    )}
                  </div>
                </div>
              </Link>
            </>
          ))
          :
          <>
            <Skeleton className="h-24 w-full min-w-36 rounded-lg" />
            <Skeleton className="h-24 w-full min-w-36 rounded-lg" />
            <Skeleton className="h-24 w-full min-w-36 rounded-lg" />
            <Skeleton className="h-24 w-full min-w-36 rounded-lg" />
          </>
        }

      </div>
      <PaginateFooter
        currentPage={pagination.page}
        lastPage={paginationResponse.lastPage}
        onPageChange={handlePageChange}
      />
    </div>
  );
}
