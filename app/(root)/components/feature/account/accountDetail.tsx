"use client";

import SelectOption from "@/components/selectOption";
import { Button } from "@/components/ui/button";
import { Dialog, DialogContent, DialogDescription, DialogHeader, DialogTitle } from "@/components/ui/dialog";
import { toast } from "@/components/ui/use-toast";
import { GET, POST } from "@/lib/http";
import config from "@/project.config";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import useSWR from "swr";
import { z } from "zod";
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form";
import cookie from "@/lib/cookies";
import { FaEnvelope, FaWhatsapp } from "react-icons/fa";

interface responseProfileSchema {
  "name": string,
  "email": string,
  "phone": string,
  "point": number,
  "type": string,
  "avatar": string,
  "province": string,
  "regencies": string,
  "district": string,
  "villages": string,
}

// Schema for form validation
const profileSchema = z.object({
  province: z.string().min(1, 'Pilih Provinsi'),
  regencies: z.string().min(1, 'Pilih Kabupaten'),
  district: z.string().min(1, 'Pilih Kecamatan'),
  villages: z.string().min(1, 'Pilih Desa'),
});

export default function AccountDetail() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [profile, setProfile] = useState<responseProfileSchema>()

  // Initializing form control with validation
  const form = useForm<z.infer<typeof profileSchema>>({
    resolver: zodResolver(profileSchema),
    defaultValues: {
      province: "",
      regencies: "",
      district: "",
      villages: "",
    },
  });

  const { watch, handleSubmit, setValue } = form;

  // Fetch location data using useSWR
  const province = watch("province");
  const regencies = watch("regencies");
  const district = watch("district");

  // Fetch location data using useSWR
  const { data: provinsi } = useSWR(config.apiUrl + "/v1/location/provinces");
  const { data: kabupaten, isLoading: kabupatenIsLoading } = useSWR(
    province ? config.apiUrl + `/v1/location/regencies/${province}` : null
  );
  const { data: kecamatan, isLoading: kecamatanIsLoading } = useSWR(
    regencies ? config.apiUrl + `/v1/location/districts/${regencies}` : null
  );
  const { data: desa, isLoading: desaIsLoading } = useSWR(
    district ? config.apiUrl + `/v1/location/villages/${district}` : null
  );

  useEffect(() => {
    if (cookie.getToken()) {
      getProfile();
      return;
    }
  }, []);

  const getProfile = async () => {
    try {
      const { data } = await GET("/v1/account/profile", {});
      setProfile(data?.data);
      setIsModalOpen(!data?.data?.province || !data?.data?.district || !data?.data?.regencies || !data?.data?.villages);
    } catch {
      setIsModalOpen(false);
      toast({
        variant: "destructive",
        title: "EPROFILE",
        description: "Error pada pengaturan profile homepage",
      });
    }
  };

  // Reset dependent fields when province, regencies, or district changes
  useEffect(() => {
    const subscription = watch((value, { name }) => {
      if (name === "province" && value.province) {
        // When province changes, reset regencies, district, and villages
        setValue("regencies", "");
        setValue("district", "");
        setValue("villages", "");
      } else if (name === "regencies" && value.regencies) {
        // When regencies changes, reset district and villages
        setValue("district", "");
        setValue("villages", "");
      } else if (name === "district" && value.district) {
        // When district changes, reset villages
        setValue("villages", "");
      }
    });

    return () => subscription.unsubscribe(); // Clean up subscription on unmount
  }, [watch, setValue]);

  const onSubmit = async (values: z.infer<typeof profileSchema>) => {
    try {
      const { data } = await POST("/v1/account/update", values);
      if (data.status === "SUCCESS") {
        getProfile();
      }
    } catch (error) {
      toast({
        variant: "destructive",
        title: "Update Failed",
        description: "Failed to update profile",
      });
    }
  };

  const renderSelectOption = (
    name: "province" | "regencies" | "district" | "villages",
    data: any[],
    isLoading: any,
    title: string
  ) => (
    <FormField
      control={form.control}
      name={name}
      render={({ field: { onChange, value } }) => (
        <FormItem className="w-full">
          <FormControl>
            {isLoading ? (
              <SelectOption
                value={""}
                onValueChange={() => { }}
                data={[]}
                title={"Loading"}
                className="w-full"
                disabled={true}
              />
            ) : (
              <>
                {data.length > 0 && (
                  <SelectOption
                    value={value}
                    onValueChange={onChange}
                    data={data}
                    title={title}
                    className="w-full"
                  />
                )}
              </>
            )}
          </FormControl>
          <FormMessage className="text-xs" />
        </FormItem>
      )}
    />
  );

  // Transform the data for SelectOption components
  const transformedProvinces = provinsi?.provinces.map((e: any) => ({
    label: e.name,
    value: String(e.id),
  })) || [];
  const transformedKabupaten = kabupaten?.regencies.map((e: any) => ({
    label: e.name,
    value: String(e.id),
  })) || [];
  const transformedKecamatan = kecamatan?.districts.map((e: any) => ({
    label: e.name,
    value: String(e.id),
  })) || [];
  const transformedDesa = desa?.villages.map((e: any) => ({
    label: e.name,
    value: String(e.id),
  })) || [];

  return (
    <>
      <Dialog open={isModalOpen} onOpenChange={() => { }}>
        <DialogContent className="max-w-[631px] md:min-w-[500px]">
          <DialogHeader>
            <DialogTitle className="pb-2 border-b-2">Lengkapi data diri</DialogTitle>
          </DialogHeader>
          <DialogDescription>
            <Form {...form}>
              <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-3 justify-between w-full">
                <div className="font-semibold text-lg">{profile?.name}</div>
                <div className="flex flex-wrap gap-3">
                  <div className="font-semibold flex items-center gap-2"><FaWhatsapp />{profile?.phone}</div>
                  <div className="font-semibold flex items-center gap-2"><FaEnvelope />{profile?.email}</div>
                </div>
                {renderSelectOption("province", transformedProvinces, false, "Provinsi")}
                {renderSelectOption("regencies", transformedKabupaten, kabupatenIsLoading, "Kabupaten")}
                {renderSelectOption("district", transformedKecamatan, kecamatanIsLoading, "Kecamatan")}
                {renderSelectOption("villages", transformedDesa, desaIsLoading, "Desa")}
                <small className="text-xs text-red-500 italic">
                  Form ini hanya akan tampil 1 kali saat profile anda belum lengkap, anda juga bisa mengubah data ini dalam detail profile anda.
                </small>
                <Button variant={"primary"} type="submit">
                  Simpan
                </Button>
              </form>
            </Form>
          </DialogDescription>
        </DialogContent>
      </Dialog>
    </>
  );
}
