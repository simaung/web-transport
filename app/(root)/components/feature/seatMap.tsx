/* eslint-disable @next/next/no-img-element */
import { Checkbox } from '@/components/ui/checkbox'
import { SeatLayout } from '@/types/outlet'
import { FC, useEffect, useState } from 'react'
import { useSearchParams } from 'next/navigation'
import { toast } from "@/components/ui/use-toast"
import Seat from '@/components/ui/seat'


interface SeatMapProps {
    data: any,
    selectSeat: any,
    setSelectSeat: any,
    setValue: any,
    fields: any,
    untuk: string
}

const SeatMap: FC<SeatMapProps> = ({
    data,
    selectSeat,
    setSelectSeat,
    setValue,
    fields,
    untuk
}) => {
    // disini harus ada pengecekan apakah kursi kembali sudah terisi atau belum
    // jangan lupa step normal tetap jalan
    const searchParams = useSearchParams();
    function handleClickKursi(id_seat: any, seat_name: any) {
        const isSeatSelected = selectSeat?.some((seat: any) => seat.id_seat === id_seat);
        // const penumpang = parseInt(searchParams.get("pax")!);
        const penumpang = parseInt(fields(untuk).length!);
        if (isSeatSelected) {
            const indexRemovedKursi = fields(untuk)?.findIndex((e: any) => e.seat_number === id_seat)
            // console.log(fields('passengers'), 'is remove');
            setValue(`${untuk}.${indexRemovedKursi === -1 ? 0 : indexRemovedKursi}`, { ...fields(`passengers.${indexRemovedKursi === -1 ? 0 : indexRemovedKursi}`), seat_number: '' })
            const updatedSelectedKursi = selectSeat.filter((seat: any) => seat.id_seat !== id_seat);
            setSelectSeat(updatedSelectedKursi);
        } else {
            if (penumpang > selectSeat.length) {
                const indexAppendKursi = fields(untuk)?.findIndex((e: any) => e.seat_number === "")
                // console.log(indexAppendKursi);
                setValue(`${untuk}.${indexAppendKursi === -1 ? 0 : indexAppendKursi}`, { ...fields(`passengers.${indexAppendKursi === -1 ? 0 : indexAppendKursi}`), seat_number: id_seat })
                // console.log(fields('passengers'), 'is append');
                setSelectSeat([...selectSeat, { id_seat: id_seat, seat_number: id_seat }]);
            } else {
                toast({
                    variant: "warning",
                    title: "Perhatian:",
                    description: 'Anda hanya boleh memilih ' + penumpang + ' Kursi',
                })
            }
        }
    }

    return (
        <div className='w-full flex flex-col gap-2'>
            {data.seatmap.map((deck: any, i: any) => (
                <div key={i} className={`flex flex-col gap-2 border-2 rounded-lg p-5`}>
                    {data.seatmap.length > 1 && (
                        <div className='text-left font-semibold border-b-2'>
                            {String(deck?.[i]?.[0]?.label)}
                        </div>
                    )}
                    {
                        deck.map((seat: any, s: any) => (
                            <div className={`flex-row flex gap-3 items-center text-center`} key={s}>
                                {seat.map((layout: SeatLayout, z: any) => (
                                    <div key={String(s) + String(z)} className='flex w-full items-center justify-center'>
                                        {layout.type == 'EMPTY' ?
                                            (
                                                <>
                                                    <label
                                                        htmlFor={layout.id}
                                                        className={
                                                            "flex p-5 rounded-md w-full max-w-16"
                                                        }
                                                    >
                                                        <div className="m-auto">&nbsp;</div>
                                                    </label>
                                                </>
                                            )
                                            :
                                            (
                                                <>
                                                    {layout.type == 'DRIVER' ? (
                                                        <img src="/images/component/stir.svg" alt="..." className='w-full max-w-16 h-auto ' />
                                                    ) :
                                                        (
                                                            <>
                                                                <div
                                                                    onClick={() =>
                                                                        layout?.status === "AVAILABLE" && (
                                                                            handleClickKursi(layout.id, layout.id)
                                                                        )
                                                                    }
                                                                    className='relative'
                                                                >
                                                                    {/* {String(selectSeat?.some((seat: any) => seat.id_seat === layout.id))} */}
                                                                    <Seat state={selectSeat?.some((seat: any) => seat.id_seat === layout.id) ? 'active' : layout.status === 'AVAILABLE' ? "available" : 'booked'} label={layout.id} />
                                                                </div>
                                                            </>
                                                        )
                                                    }
                                                </>
                                            )}
                                    </div>
                                ))}
                            </div>
                        ))
                    }
                </div>
            ))}
        </div >
    )
}

export default SeatMap
