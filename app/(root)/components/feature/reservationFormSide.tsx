"use client"

import { Button } from "@/components/ui/button"
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { groupingDropPoint, groupingOutlet } from "@/lib/grouping"
import config from "@/project.config"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { FaSearch } from "react-icons/fa"
import useSWR from "swr"
import { z } from "zod"
import ErrorFetch from "../errorFetch"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { CalendarIcon } from "@radix-ui/react-icons"
import { Calendar } from "@/components/ui/calendar"
import { format } from "date-fns"
import { cn } from "@/lib/utils"
import SelectOutlet from "@/components/selectOutlet"
import useArrivalSelect from "@/hooks/useArrivalSelect"
import { useRouter } from "next/navigation"
import { Card, CardContent, CardFooter, CardHeader, CardTitle } from "@/components/ui/card"
import SelectOption from "@/components/selectOption"
import { FC, useEffect, useState } from "react"
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@/components/ui/accordion';
import LoadingButton from "@/components/LoadingButton"
import { toast } from "@/components/ui/use-toast"
import { Checkbox } from "@radix-ui/react-checkbox"

const reservationFormSchema = z.object({
    depart: z.string().min(1, 'Outlet keberangkatan tidak boleh kosong'),
    arrival: z.string().min(1, 'Outlet Tujuan tidak boleh kosong'),
    dateDepart: z.date({
        required_error: "Tanggal tidak boleh kosong",
    }),
    returnDate: z.date().optional(),
    pax: z.number().gte(1, 'Jumlah Penumpang tidak boleh kurang dari 1'),
})

let pax = [];
for (let i: number = 1; i <= Number(config.maxPax); i++) {
    pax.push({ label: `${i} Penumpang`, value: i })
}

let today = new Date();
today.setDate(today.getDate() - 1)

interface ReservationFormSideProps {
    dataQuery: any,
    getSchedule: any,
    setJadwalLoading: any,
    setDataJadwal: any,
}

const ReservationFormSide: FC<ReservationFormSideProps> = ({ dataQuery, getSchedule, setJadwalLoading, setDataJadwal }) => {
    const router = useRouter()
    const [isLoading, setIsLoading] = useState(false);
    const arrivalSelect = useArrivalSelect()
    const [isRoundTrip, setIsRoundTrip] = useState(dataQuery?.return_date ? true : false)


    const { data: departData, error: departError, isLoading: departIsLoading } = useSWR(`${config.apiUrl}/v1/outlets`)
    const { data: arrivalData, error: arrivalError, isLoading: arrivalIsLoading } = useSWR(`${config.apiUrl}/v1/routes/${arrivalSelect?.departCode || dataQuery.pickup_point}`)
    const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)

    const form = useForm<z.infer<typeof reservationFormSchema>>({
        resolver: zodResolver(reservationFormSchema),
        defaultValues: {
            depart: dataQuery?.pickup_point || "",
            arrival: dataQuery?.dropoff_point || "",
            dateDepart: new Date(dataQuery?.departure_date) || "",
            returnDate: new Date(dataQuery?.return_date) || "",
            pax: dataQuery?.pax || "",
        },
    })

    const { register, handleSubmit, setValue, watch, getFieldState, getValues } = form;

    // useEffect(() => {
    //     setJadwalLoading(true)
    //     setDataJadwal(undefined)
    // }, [getValues, setJadwalLoading, setDataJadwal]);

    function onSubmit(values: z.infer<typeof reservationFormSchema>) {
        if (isRoundTrip && !values?.returnDate) {
            toast({
                variant: 'warning',
                title: 'Lengkapi Data!',
                description: 'Mohon pilih tanggal pulang'
            })
            return
        }
        if (values.dateDepart >= values?.returnDate! && isRoundTrip) {
            toast({
                variant: 'warning',
                title: 'Data tidak sesuai',
                description: 'Tanggal berangkat tidak boleh lebih besar dari tanggal pulang'
            })
            return
        }
        // kembalikan ke pencarian awal
        setIsLoading(true);
        // const paramQuery = `depart=${values.depart}&arrival=${values.arrival}&date=${format(values.dateDepart, "y-MM-dd")}&pax=${values.pax}`
        const url = `depart=${values.depart}&arrival=${values.arrival}&date=${format(values.dateDepart, "y-MM-dd")}&pax=${values.pax}${dataQuery?.return_date ? "&return-date=" + format(values?.returnDate!, "y-MM-dd") : ""}${dataQuery?.booking_code ? "&booking_code=" + dataQuery.booking_code : ""}`
        const paramQuery = url;
        router.push("/schedule?" + paramQuery)
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }

    if (departError) return <ErrorFetch />

    const outlets = departData ? groupingOutlet(departData.points) : []
    const arrivalPoint = arrivalData ? groupingDropPoint(arrivalData.routes) : []
    const outletAsal = watch('depart')
    const outletTujuan = watch('arrival')

    var getPoint = (id: string) => {
        return dataPoints?.points?.find((c: any) => c.id === id)?.name
    }

    function roundTripToggle() {
        setIsRoundTrip(!isRoundTrip);
        setIsLoading(true);
        if (!isRoundTrip) {
            const url = `depart=${getValues().depart}&arrival=${getValues().arrival}&date=${format(getValues().dateDepart, "y-MM-dd")}&pax=${getValues().pax}&return-date=${format(getValues().dateDepart, "y-MM-dd")}`
            setIsLoading(false);
            setValue("returnDate", getValues().dateDepart);
            router.push("/schedule?" + url)
        }
        if (isRoundTrip) {
            const url = `depart=${getValues().depart}&arrival=${getValues().arrival}&date=${format(getValues().dateDepart, "y-MM-dd")}&pax=${getValues().pax}`
            setIsLoading(false);
            router.push("/schedule?" + url)
        }

    }


    return (
        <Card>
            <Form {...form}>
                <Accordion type="single" collapsible className="w-full lg:hidden" defaultValue="">
                    <AccordionItem value="item-1">
                        <AccordionTrigger className="flex flex-col hover:no-underline">
                            <span className="font-bold">
                                {getPoint(outletAsal)} - {getPoint(outletTujuan)}
                            </span>
                            <small className="text-blue-500">Ubah Jadwal</small>
                        </AccordionTrigger>
                        <AccordionContent>
                            <form onSubmit={form.handleSubmit(onSubmit)}>
                                <CardHeader>
                                    <div className="flex flex-row justify-between items-center">
                                        <CardTitle>
                                            Ubah Pencarian
                                        </CardTitle>
                                        <div onClick={roundTripToggle} className={`flex items-center cursor-pointer p-3 rounded-lg border-2 lg:w-fit w-full ${isRoundTrip ? "bg-green-500 text-white" : "bg-white"}`}>
                                            <div className="flex flex-row items-center gap-1">
                                                <span>
                                                    Pulang Pergi
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </CardHeader>
                                <CardContent className="grid grid-cols-1 gap-2" >
                                    <FormField
                                        control={form.control}
                                        name="depart"
                                        render={({ field: { onChange, value } }) => (
                                            <FormItem>
                                                <FormControl>
                                                    <SelectOutlet
                                                        value={value}
                                                        onValueChange={(e: any) => { onChange(e) }}
                                                        data={departIsLoading ? [] : outlets}
                                                        title={"Outlet Keberangkatan"}
                                                        disabled={departIsLoading}
                                                    />
                                                </FormControl>
                                                <FormMessage className="text-xs" />
                                            </FormItem>
                                        )}
                                    />
                                    <FormField
                                        control={form.control}
                                        name="arrival"
                                        render={({ field: { onChange, value } }) => (
                                            <FormItem>
                                                <FormControl>
                                                    <SelectOutlet
                                                        value={value}
                                                        onValueChange={onChange}
                                                        data={arrivalIsLoading ? [] : arrivalPoint}
                                                        title={arrivalIsLoading ? "Loading..." : "Outlet Tujuan"}
                                                    // disabled={arrivalSelect.isDisabled}
                                                    />
                                                </FormControl>
                                                <FormMessage className="text-xs" />
                                            </FormItem>
                                        )}
                                    />
                                    <FormField
                                        control={form.control}
                                        name="dateDepart"
                                        render={({ field }) => (
                                            <FormItem className="flex flex-col w-full">
                                                <Popover>
                                                    <PopoverTrigger asChild>
                                                        <FormControl>
                                                            <Button
                                                                variant={"outline"}
                                                                className={cn(
                                                                    "pl-3 text-left font-normal text-md min-w-60",
                                                                    !field.value && "text-muted-foreground"
                                                                )}
                                                            >
                                                                {field.value ? (
                                                                    format(field.value, "dd/MM/y")
                                                                ) : (
                                                                    <span>Tanggal Keberangkatan</span>
                                                                )}
                                                                <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                            </Button>
                                                        </FormControl>
                                                    </PopoverTrigger>
                                                    <PopoverContent className="w-auto p-0" align="start">
                                                        <Calendar
                                                            mode="single"
                                                            selected={field.value}
                                                            onSelect={field.onChange}
                                                            disabled={(date) =>
                                                                date < today
                                                            }
                                                            initialFocus
                                                        />
                                                    </PopoverContent>
                                                </Popover>
                                                <FormMessage className="text-xs" />
                                            </FormItem>
                                        )}
                                    />
                                    {isRoundTrip && (
                                        <>
                                            <FormField
                                                control={form.control}
                                                name="returnDate"
                                                render={({ field }) => (
                                                    <FormItem className="flex flex-col w-full">
                                                        <Popover>
                                                            <PopoverTrigger asChild>
                                                                <FormControl>
                                                                    <Button
                                                                        variant={"outline"}
                                                                        className={cn(
                                                                            "pl-3 text-left font-normal text-md min-w-60",
                                                                            !field.value && "text-muted-foreground"
                                                                        )}
                                                                    >
                                                                        {field.value ? (
                                                                            format(field.value, "dd/MM/y")
                                                                        ) : (
                                                                            <span>Tanggal Pulang</span>
                                                                        )}
                                                                        <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                                    </Button>
                                                                </FormControl>
                                                            </PopoverTrigger>
                                                            <PopoverContent className="w-auto p-0" align="start">
                                                                <Calendar
                                                                    mode="single"
                                                                    selected={field.value}
                                                                    onSelect={field.onChange}
                                                                    disabled={(date) =>
                                                                        date < today
                                                                    }
                                                                    initialFocus
                                                                />
                                                            </PopoverContent>
                                                        </Popover>
                                                        <FormMessage className="text-xs" />
                                                    </FormItem>
                                                )}
                                            />
                                        </>
                                    )}
                                    <FormField
                                        control={form.control}
                                        name="pax"
                                        render={({ field: { onChange, value } }) => (
                                            <FormItem>
                                                <FormControl>
                                                    <SelectOption
                                                        value={value}
                                                        onValueChange={onChange}
                                                        data={pax}
                                                        title={"Jumlah Penumpang"}
                                                    />
                                                </FormControl>
                                                <FormMessage className="text-xs" />
                                            </FormItem>
                                        )}
                                    />
                                </CardContent>
                                <CardFooter className="flex justify-between">
                                    <Button className="w-full gap-2" variant="primary" type="submit"><FaSearch />Cek</Button>
                                </CardFooter>
                            </form>
                        </AccordionContent>
                    </AccordionItem>
                </Accordion>
                <form onSubmit={form.handleSubmit(onSubmit)} className="hidden lg:block">
                    <CardHeader>
                        <div className="flex flex-row justify-between items-center">
                            <CardTitle>
                                Ubah Pencarian
                            </CardTitle>
                            <div onClick={roundTripToggle} className={`flex items-center cursor-pointer p-3 rounded-lg border-2 lg:w-fit w-full ${isRoundTrip ? "bg-green-500 text-white" : "bg-white"}`}>
                                <div className="flex flex-row items-center gap-1">
                                    <span>
                                        Pulang Pergi
                                    </span>
                                </div>
                            </div>
                        </div>
                    </CardHeader>
                    <CardContent className="grid grid-cols-1 gap-2">
                        <FormField
                            control={form.control}
                            name="depart"
                            render={({ field: { onChange, value } }) => (
                                <FormItem>
                                    <FormControl>
                                        <SelectOutlet
                                            value={value}
                                            onValueChange={onChange}
                                            data={departIsLoading ? [] : outlets}
                                            title={"Outlet Keberangkatan"}
                                            disabled={departIsLoading}
                                        />
                                    </FormControl>
                                    <FormMessage className="text-xs" />
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="arrival"
                            render={({ field: { onChange, value } }) => (
                                <FormItem>
                                    <FormControl>
                                        <SelectOutlet
                                            value={value}
                                            onValueChange={onChange}
                                            data={arrivalIsLoading ? [] : arrivalPoint}
                                            title={arrivalIsLoading ? "Loading..." : "Outlet Tujuan"}
                                        // disabled={arrivalSelect.isDisabled}
                                        />
                                    </FormControl>
                                    <FormMessage className="text-xs" />
                                </FormItem>
                            )}
                        />
                        <FormField
                            control={form.control}
                            name="dateDepart"
                            render={({ field }) => (
                                <FormItem className="flex flex-col w-full">
                                    <Popover>
                                        <PopoverTrigger asChild>
                                            <FormControl>
                                                <Button
                                                    variant={"outline"}
                                                    className={cn(
                                                        "pl-3 text-left font-normal text-md min-w-60",
                                                        !field.value && "text-muted-foreground"
                                                    )}
                                                >
                                                    {field.value ? (
                                                        format(field.value, "dd/MM/y")
                                                    ) : (
                                                        <span>Tanggal Keberangkatan</span>
                                                    )}
                                                    <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                </Button>
                                            </FormControl>
                                        </PopoverTrigger>
                                        <PopoverContent className="w-auto p-0" align="start">
                                            <Calendar
                                                mode="single"
                                                selected={field.value}
                                                onSelect={field.onChange}
                                                disabled={(date) =>
                                                    date < today
                                                }
                                                initialFocus
                                            />
                                        </PopoverContent>
                                    </Popover>
                                    <FormMessage className="text-xs" />
                                </FormItem>
                            )}
                        />
                        {isRoundTrip && (
                            <>
                                <FormField
                                    control={form.control}
                                    name="returnDate"
                                    render={({ field }) => (
                                        <FormItem className="flex flex-col w-full">
                                            <Popover>
                                                <PopoverTrigger asChild>
                                                    <FormControl>
                                                        <Button
                                                            variant={"outline"}
                                                            className={cn(
                                                                "pl-3 text-left font-normal text-md min-w-60",
                                                                !field.value && "text-muted-foreground"
                                                            )}
                                                        >
                                                            {field.value ? (
                                                                format(field.value, "dd/MM/y")
                                                            ) : (
                                                                <span>Tanggal Pulang</span>
                                                            )}
                                                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                                        </Button>
                                                    </FormControl>
                                                </PopoverTrigger>
                                                <PopoverContent className="w-auto p-0" align="start">
                                                    <Calendar
                                                        mode="single"
                                                        selected={field.value}
                                                        onSelect={field.onChange}
                                                        disabled={(date) =>
                                                            date < today
                                                        }
                                                        initialFocus
                                                    />
                                                </PopoverContent>
                                            </Popover>
                                            <FormMessage className="text-xs" />
                                        </FormItem>
                                    )}
                                />
                            </>
                        )}
                        <FormField
                            control={form.control}
                            name="pax"
                            render={({ field: { onChange, value } }) => (
                                <FormItem>
                                    <FormControl>
                                        <SelectOption
                                            value={value}
                                            onValueChange={onChange}
                                            data={pax}
                                            title={"Jumlah Penumpang"}
                                        />
                                    </FormControl>
                                    <FormMessage className="text-xs" />
                                </FormItem>
                            )}
                        />
                    </CardContent>
                    <CardFooter className="flex justify-between">
                        {isLoading ?
                            <LoadingButton className="w-full" />
                            :
                            <Button className="w-full gap-2" variant="primary" type="submit"><FaSearch />Cek</Button>
                        }
                    </CardFooter>
                </form>
            </Form>
        </Card>
    )
}

export default ReservationFormSide
