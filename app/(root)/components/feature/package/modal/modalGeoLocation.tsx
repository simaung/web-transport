/* eslint-disable react-hooks/exhaustive-deps */
import { Button } from "@/components/ui/button";
import { FaTimes } from "react-icons/fa";
import GoogleGeocoderMap from "../../../map/googleGeocoderMap";

export default function ModalGeoLocation({ isOpen, setIsOpen, handleLocationChange, center, onValueUpdate }: any) {
  return (
    <>
      <div className={`fixed top-0 left-0 backdrop-blur-sm w-full h-screen ${isOpen ? "block" : "hidden"} z-50 overflow-y-auto`}>
        <div className="flex items-center justify-center w-full h-screen">
          <div className="container">
            <div className="bg-white rounded-lg shadow-lg p-5 gap-3 flex flex-col">
              <div className="flex flex-row justify-between items-center">
                <span className="font-bold">Pilih titik jemput/antar</span>
                <FaTimes className="text-gray-400" onClick={() => {
                  setIsOpen(false)
                  onValueUpdate()
                }} />
              </div>
              {/* {JSON.stringify(center)} */}
              <br />
              <GoogleGeocoderMap onLocationChange={handleLocationChange} center={center} />
              <Button variant={'primary'} onClick={() => {
                setIsOpen(false);
                onValueUpdate();
              }} className="w-full">Simpan Lokasi</Button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}