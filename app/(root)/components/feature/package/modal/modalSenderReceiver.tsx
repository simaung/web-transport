import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { FaTimes } from "react-icons/fa";

export default function ModalSenderReceiver({ isOpen, setIsOpen, onSendPackage, setValue, watch }: any) {
  const senderName = watch('sender.name')
  const senderPhone = watch('sender.phone')
  const receiverName = watch('receiver.name')
  const receiverPhone = watch('receiver.phone')
  const note = watch('note')
  return (
    <>
      <div className={`fixed top-0 left-0 backdrop-blur-sm w-full h-screen ${isOpen ? "block" : "hidden"} z-50`}>
        <div className="flex items-center justify-center w-full h-screen">
          <div className="container">
            <div className="bg-white rounded-lg shadow-lg p-5 flex flex-col gap-3">
              <div className="flex flex-row justify-between items-center border-b-2 pb-1">
                <span className="font-bold">Pengirim & Penerima</span>
                <FaTimes className="text-gray-300 cursor-pointer" onClick={() => setIsOpen(false)} />
              </div>
              <div className="flex flex-row gap-3">
                <div className="w-full flex flex-col gap-3">
                  <Input placeholder="Nama Pengirim" onChange={({ target }) => { setValue('sender.name', target.value) }} value={senderName} />
                  <Input placeholder="Telepon Pengirim" onChange={({ target }) => { setValue('sender.phone', target.value) }} value={senderPhone} />
                </div>
                <div className="w-full flex flex-col gap-3">
                  <Input placeholder="Nama Penerima" onChange={({ target }) => { setValue('receiver.name', target.value) }} value={receiverName} />
                  <Input placeholder="Telepon Penerima" onChange={({ target }) => { setValue('receiver.phone', target.value) }} value={receiverPhone} />
                </div>
              </div>
              <textarea className="w-full rounded-lg border-2 outline-none focus:border-primary h-20 p-3 text-sm" placeholder="Catatan (Opsional)" onChange={({ target }) => { setValue('note', target.value) }} value={note}></textarea>
              <Button variant={'primary'} onClick={onSendPackage} className="w-full">Pesan Sekarang</Button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}