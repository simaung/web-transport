/* eslint-disable @next/next/no-img-element */
"use client"
import { Button } from "@/components/ui/button"
import { groupPayment } from "@/lib/grouping"
import config from "@/project.config"
import { useState } from "react"
import useSWR from "swr"
import { useRouter } from "next/navigation"
import { toast } from "@/components/ui/use-toast"
import { POST } from "@/lib/http"

interface PackagePaymentProps {
  kode: string
}

interface selectedPaymentSchema {
  booking_code: string,
  payment_method: string,
  payment_code: string
}

export default function PackagePayment({ kode }: PackagePaymentProps) {
  const Router = useRouter();
  const method = JSON.stringify({
    VA: "Virtual Account",
    QR: "Qr Code",
    "E-Wallet": 'Dompet Online'
  })
  const [selectedPayment, setSelectedPayment] = useState<selectedPaymentSchema>({
    booking_code: kode,
    payment_method: '',
    payment_code: ''
  });
  const { data: payment, isLoading: paymentLoading } = useSWR(`${config.apiUrl}/v1/payment/${kode}/package`)

  const paymentGroup = payment?.payments && (groupPayment(payment?.payments, "type"))

  async function CreatePayment() {
    const { data: res } = await POST('/v1/payment/create/package', selectedPayment);
    if (res.status !== 'SUCCESS') {
      toast({
        variant: "destructive",
        title: res?.status,
        description: res?.message,
      });
      Router.push('/package?kode=' + kode)
      return
    }
    toast({
      variant: "success",
      title: "Berhasil membuat pembayaran",
      description: "Silahkan ikuti langkah pembayaran berikut."
    });

    const { paymentUrl, qrString, vaNumber } = res?.payments;
    const { booking_code, payment_code } = selectedPayment;

    if (paymentUrl !== '') {
      window.open(paymentUrl, '_blank');
      return
    }

    if (qrString !== '') {
      // console.log(qrString, 'qrString');
      Router.push(`/checkout/bayar?payment_code=${payment_code}&&qrString=${qrString}&&booking_code=${booking_code}&&type=package`)
      return
    }

    if (vaNumber !== '') {
      // console.log(vaNumber, 'vaNumber');
      Router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${vaNumber}&&booking_code=${booking_code}&&type=package`)
      return
    }
  }

  if (payment) {
    if (payment?.status !== "SUCCESS") {
      toast({
        variant: 'destructive',
        title: 'TERJADI KESALAHAN PEMBAYARAN',
        description: payment?.message
      })
      Router.push('/package?kode=' + kode);
      return
    } else if (payment?.payments?.length === 0) {
      toast({
        variant: 'warning',
        title: 'LIST PAYMENT KOSONG',
        description: 'tidak di temukan list payment'
      })
      Router.push('/package?kode=' + kode);
      return
    }
  }
  return (
    <>
      <div className="border-2 w-full p-3 rounded-lg bg-white shadow-md h-max flex flex-col justify-between gap-3.5">
        <div className="font-bold w-full text-center text-lg uppercase">Metode Pembayaran</div>
        <div className="overflow-y-auto h-96">
          {paymentLoading ?
            <>
              Loading
            </>
            :
            paymentGroup && (
              <div className="flex flex-col gap-7 ">
                {
                  Object?.keys(paymentGroup)?.map(u => (
                    <>
                      <div className="flex flex-col gap-3" key={u + "paymentgroup"}>
                        <span className="font-bold w-full border-b-2 pb-2">{JSON.parse(method)[u]}</span>
                        {paymentGroup[u].map((s: { name: string, code: string, image: string, type: string }) => (
                          <>
                            <div
                              className={`w-full bg-white p-3 rounded-lg border-2 hover:border-primary duration-300 cursor-pointer flex flex-row justify-between ${s.code === selectedPayment?.payment_code ? "border-primary" : ""}`}
                              key={s.name + "paymentgroup2"}
                              onClick={() => {
                                setSelectedPayment({ ...selectedPayment!, payment_code: s?.code, payment_method: s?.type })
                              }}
                            >
                              {s.name}
                              <img src={s.image} alt="..." className="h-8" />
                            </div>
                          </>
                        ))}
                      </div>
                    </>
                  ))
                }
              </div>
            )}
        </div>
        <Button variant={"primary"} className="w-full" onClick={CreatePayment}>Bayar</Button>
        {/* <p className="overflow-auto">
          {JSON.stringify(paymentGroup)}
        </p> */}
      </div>
    </>
  )
}