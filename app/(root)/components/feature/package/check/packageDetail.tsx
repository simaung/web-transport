import Barcode from "@/components/Barcode"
import { Button } from "@/components/ui/button"
import { FaMoneyBillWave } from "react-icons/fa"

/* eslint-disable @next/next/no-img-element */
interface packageDetailProps {
  data: any
}
export default function PackageDetail({ data }: packageDetailProps) {
  return (
    <>
      {/* <div className="border-2 w-full max-w-lg rounded-lg p-3">
        <p className="text-wrap overflow-auto">
          {JSON.stringify(data)}
        </p>
      </div> */}
      <div className="w-full max-w-lg mx-auto">
        <div className="bg-white p-5 rounded-lg shadow-md border-2">
          <div className="flex flex-row justify-between items-baseline border-b-2 pb-2 mb-3">
            <img src="/images/logo/logo.png" alt="" className="h-7" />
            <h3 className="text-lg font-bold uppercase">data paket</h3>
          </div>
          <div className="flex flex-col gap-3">
            <h3 className="text-lg font-bold w-full text-center border-2 py-3 border-black"><span className="hidden md:block">Nomor Resi :</span> {data?.code}</h3>
            <Barcode value={data?.code} />
            <div className="w-full flex flex-row gap-3 justify-between pb-3 border-b-2 border-dashed">
              <span className="border-2 border-black w-full text-center py-3">
                {data?.outlet_from?.name}
              </span>
              <span className="border-2 border-black w-full text-center py-3">
                {data?.outlet_to?.name}
              </span>
            </div>
            <div className="w-full flex flex-col md:flex-row gap-3 md:justify-between pb-3 border-b-2 border-dashed">
              <div>
                <span className="font-bold uppercase underline">
                  Penerima
                </span>
                <br />
                {data?.recipient?.name}
                <br />
                {data?.recipient?.phone}
                <br />
                {data?.recipient?.address}
              </div>
              <div className="md:text-right">
                <span className="font-bold uppercase underline">
                  Pengirim
                </span>
                <br />
                {data?.sender?.name}
                <br />
                {data?.sender?.phone}
                <br />
                {data?.sender?.address}
              </div>
            </div>
            <div className="w-full flex flex-row gap-3 justify-between pb-3 border-b-2 border-dashed">
              <span className="w-fit px-5 border-black font-bold uppercase text-center border-2">{data?.transaction_status}</span>
              <span className="w-full border-2 text-center border-black">
                {data?.date} {data?.time}
              </span>
            </div>
            {/* <div className="w-full flex flex-row gap-3 justify-between pb-3 border-b-2 border-dashed">
                                    detail lainya
                                </div> */}
            {/* <Button variant="primary" type="button" className="w-full flex items-center justify-center gap-1"><FaMoneyBillWave /> Bayar</Button> */}
          </div>
        </div>
      </div >
    </>
  )
}