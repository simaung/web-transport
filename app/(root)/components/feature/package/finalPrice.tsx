import { Skeleton } from "@/components/ui/skeleton";

export default function FinalPrice({ packagePrice, isLoading }: any) {
  return (
    <div>

      <div className="text-xs">
        {/* {JSON.stringify(packagePrice)} */}
        <div className="flex flex-col md:flex-row justify-between items-center border-b-[1px] border-gray-50 py-2 gap-3">
          <span className="w-full">Informasi kilogram</span>
          <div className="flex flex-col sm:flex-row gap-3 lg:justify-end w-full">
            <span className="sm:text-nowrap">Kilogram Pertama : Rp. {packagePrice.outlet_price.first_kg?.toLocaleString('id-ID')}</span>
            <span className="lg:block hidden">|</span>
            <span className="sm:text-nowrap">Kilogram Berikutnya : + Rp. {packagePrice.outlet_price.second_kg?.toLocaleString('id-ID')}</span>
          </div>
        </div>
      </div>
      <table className="w-full">
        <tbody className="space-y-6 text-center divide-y dark:divide-gray-700">
          <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Biaya Jemput</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16">
                <span>Rp.</span>
                <span>{
                  isLoading ?
                    <>
                      <Skeleton className="w-20 h-5" />
                    </>
                    :
                    packagePrice?.pickup?.price?.toLocaleString('id-ID') || '-'
                }</span>
              </span>
            </td>
          </tr>
          <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Biaya Antar</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16">
                <span>Rp.</span>
                <span>{
                  isLoading ?
                    <>
                      <Skeleton className="w-20 h-5" />
                    </>
                    :
                    packagePrice?.delivery?.price?.toLocaleString('id-ID') || '-'
                }</span>
              </span>
            </td>
          </tr>
          {/* <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Kilogram Pertama</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16">
                <span>Rp.</span>
                <span>{data.first_kg}</span>
              </span>
            </td>
          </tr>
          <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Kilogram berikutnya</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16">
                <span>Rp.</span>
                <span>{data.next_kg}</span>
              </span>
            </td>
          </tr> */}
          <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Harga Outlet</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16">
                <span>Rp.</span>
                <span>{
                  isLoading ?
                    <>
                      <Skeleton className="w-20 h-5" />
                    </>
                    :
                    parseInt(packagePrice?.outlet_price?.outlet_price).toLocaleString('id-ID')
                }</span>
              </span>
            </td>
          </tr>
          <tr>
            <th scope="row" className="text-left w-full">
              <h3 className="py-3">Total Tagihan</h3>
            </th>
            <td>
              <span className="flex justify-between text-sm gap-16 font-bold text-red-500 border-b-2 border-red-500 pb-1" title="total yang harus di bayar">
                <span>Rp.</span>
                <span>{
                  isLoading ?
                    <>
                      <Skeleton className="w-20 h-5" />
                    </>
                    :
                    (
                      parseInt(packagePrice?.outlet_price?.outlet_price) +
                      (packagePrice?.pickup?.price || 0) +
                      (packagePrice?.delivery?.price || 0)
                      // parseInt(packagePrice?.)
                    )
                      .toLocaleString('id-ID')
                }</span>
              </span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}