/* eslint-disable @next/next/no-img-element */
import { groupingDropPoint, groupingOutlet } from "@/lib/grouping"
import config from "@/project.config"
import useSWR from "swr"
import ErrorFetch from "../../errorFetch"
import SelectOutlet from "@/components/selectOutlet"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod"
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import useArrivalSelect from "@/hooks/useArrivalSelect"
import { Button } from "@/components/ui/button"
import { FaCircleNotch, FaSearch } from "react-icons/fa"
import { POST } from "@/lib/http"
import { useEffect, useRef, useState } from "react"
import { Skeleton } from "@/components/ui/skeleton"
import SendPackageTransaction from "./sendPackageTransaction"
import CheckPrice from "./checkPrice"
import { storeArray } from "@/lib/localStorage"
import { useRouter } from "next/navigation"
import { toast } from "@/components/ui/use-toast"


interface packagePriceSchema {
	outlet_from: string,
	outlet_to: string,
	packgae_type: string,
	packgae_weight: string,
	price_outlet: number,
	status: string,
	message: string
}

const sendPackageFormSchema = z.object({
	depart: z.string().min(1, 'Outlet keberangkatan tidak boleh kosong'),
	arrival: z.string().min(1, 'Outlet Tujuan tidak boleh kosong'),
	package_type: z.string().min(1, 'Mohon pilih jenis paket'),
	weight: z.string().min(1, 'Minimal berat 1 kilo'),
	width: z.string(),
	height: z.string(),
	length: z.string(),
	note: z.string(),
	is_pickup: z.boolean(),
	is_delivery: z.boolean(),
	sender: z.object({
		name: z.string(),
		phone: z.string(),
		pickup_address: z.string(), // optional
		pickup_latitude: z.string(), // optional
		pickup_longitude: z.string(), // optional
		courier_type: z.string(),
	}),
	receiver: z.object({
		name: z.string(),
		phone: z.string(),
		delivery_address: z.string(), // optional
		delivery_latitude: z.string(), // optional
		delivery_longitude: z.string(), // optional
		courier_type: z.string(),
	})
})

export const SendPackage = () => {
	const router = useRouter();
	const arrivalSelect = useArrivalSelect()
	const [packagePrice, setPackagePrice] = useState<packagePriceSchema>();
	const [isLoading, setIsLoading] = useState(false);
	const [isLoadingFinalPrice, setIsLoadingFinalPrice] = useState(false);
	const [isOpen, setIsOpen] = useState(false);
	const [isError, setIsError] = useState(false);
	const [isPackageTransaction, setIsPackageTransaction] = useState(false);
	const { data: departData, error: departError, isLoading: departIsLoading } = useSWR(`${config.apiUrl}/v1/outlets`)
	// const [arrivalData, setArrivalData] = useState();
	// const [arrivalIsLoading, setArrivalIsLoading] = useState(false);
	const { data: arrivalData, error: arrivalError, isLoading: arrivalIsLoading } = useSWR(`${config.apiUrl}/v1/routes/${arrivalSelect.departCode}`)

	const form = useForm<z.infer<typeof sendPackageFormSchema>>({
		resolver: zodResolver(sendPackageFormSchema),
		defaultValues: {
			depart: "",
			arrival: "",
			package_type: "PKT",
			weight: "1",
			width: "8",
			height: "8",
			length: "8",
			note: "", // optional
			is_pickup: false,
			is_delivery: false,
			sender: {
				name: "test",
				phone: "test",
				pickup_address: "test", // optional
				pickup_latitude: "", // optional
				pickup_longitude: "", // optional
				courier_type: "external",
			},
			receiver: {
				name: "test",
				phone: "test",
				delivery_address: "test", // optional
				delivery_latitude: "", // optional
				delivery_longitude: "", // optional
				courier_type: "external",
			}
		}
	})
	const { register, handleSubmit, setValue, watch, getFieldState, getValues } = form;

	// const package_type = watch('package_type');
	// const weight = watch('weight');

	// // dua ini di triger ketika menutup modal map 
	// const pickup_address = watch('sender.pickup_address')
	// const delivery_address = watch('receiver.delivery_address')

	// useEffect(() => {
	//     console.log(getValues());
	//     console.log('uef');

	//     onValueUpdate()
	//     // eslint-disable-next-line react-hooks/exhaustive-deps
	// }, [package_type, weight]);

	if (departError) return <ErrorFetch />

	const outlets = departData ? groupingOutlet(departData.points) : []
	const arrivalPoint = arrivalData ? groupingDropPoint(arrivalData.routes) : []


	async function onSubmit(values: z.infer<typeof sendPackageFormSchema>) {
		defaultValue();
		setIsLoading(true)
		setIsPackageTransaction(false)
		setIsError(false)
		try {
			const { data } = await POST('/v1/package/price', {
				"outlet_from": values.depart,
				"outlet_to": values.arrival,
				// "package_type": values.package_type,
				// "weight": values.weight,
				// "height": values.height,
				// "width": values.width,
				// "length": values["length"]
				"package_type": 'PKT',
				"weight": "1",
				"height": "8",
				"width": "8",
				"length": "8"
			})
			setPackagePrice(data?.package_price || data)
		} catch (error) {
			setIsError(true)
		}
		setIsLoading(false)
	}

	async function onValueUpdate() {
		setIsLoadingFinalPrice(true)
		const values = getValues();
		setIsError(false)
		try {
			const { data } = await POST('/v1/package/price', {
				"outlet_from": values.depart,
				"outlet_to": values.arrival,
				...values
			})
			setPackagePrice(data?.package_price || data)
		} catch (error) {
			setIsError(true)
		}
		setIsLoadingFinalPrice(false)
	}

	function handleLanjutKirm() {
		setIsPackageTransaction(true);
	}

	async function onSendPackage() {
		const values = getValues()
		try {
			const { data } = await POST('/v1/package/transaction', { outlet_from: values.depart, outlet_to: values.arrival, ...values })
			storeArray('packageList', data?.package_transaction || data)
			if (data.status === "SUCCESS") {
				router.push(`/package/${data?.package_transaction?.booking_code}`)
				toast({
					variant: 'success',
					title: 'Berhasil pesan paket',
					description: "Silahkan lanjutkan pembayaran"
				})
				setIsOpen(false);
			} else {
				toast({
					variant: 'destructive',
					title: 'Pesanan Gagal',
					description: data.message
				})
			}
		} catch (error) {
			setIsError(true);
		}
	}

	function defaultValue() {
		setValue("package_type", "PKT");
		setValue("weight", "1");
		setValue("width", "8");
		setValue("height", "8");
		setValue("length", "8");
	}

	return (
		<>
			<Form {...form}>
				<form onSubmit={form.handleSubmit(onSubmit)} className="w-full bg-white p-5 rounded-lg shadow-sm flex flex-col lg:flex-row gap-3 items-center">
					<FormField
						control={form.control}
						name="depart"
						render={({ field: { onChange, value } }) => (
							<FormItem className="w-full">
								<FormControl>
									{departData && (
										<SelectOutlet
											value={value}
											onValueChange={onChange}
											data={outlets}
											title={"Outlet Keberangkatan"}
											className="w-full"
										/>
									)}
								</FormControl>
								<FormMessage className="text-xs" />
							</FormItem>
						)}
					/>
					<FormField
						control={form.control}
						name="arrival"
						render={({ field: { onChange, value } }) => (
							<FormItem className="w-full">
								<FormControl className="w-full">
									<SelectOutlet
										value={value}
										onValueChange={onChange}
										data={arrivalIsLoading ? [] : arrivalPoint}
										title={arrivalIsLoading ? "Loading..." : "Outlet Tujuan"}
										disabled={arrivalSelect.isDisabled || arrivalIsLoading}
										className="w-full"
									/>
								</FormControl>
								<FormMessage className="text-xs" />
							</FormItem>
						)}
					/>
					{/* <FormField
                        control={form.control}
                        name="package_type"
                        render={({ field: { onChange, value } }) => (
                            <FormItem className="w-full">
                                <FormControl className="w-full">
                                    <SelectOption
                                        value={value}
                                        onValueChange={onChange}
                                        data={jenisPaket}
                                        title={packageIsLoading ? "Loading..." : "Jenis Paket"}
                                        className="w-full"
                                    />
                                </FormControl>
                                <FormMessage className="text-xs" />
                            </FormItem>
                        )}
                    />
                    <div className="relative w-full">
                        <FormField
                            control={form.control}
                            name="weight"
                            render={({ field }) => (
                                <FormItem className="w-full">
                                    <FormControl className="w-full">
                                        <Input placeholder="Berat" {...field} className="w-full" />
                                    </FormControl>
                                    <FormMessage className="text-xs" />
                                </FormItem>
                            )}
                        />
                        <span className="absolute text-gray-400 italic bottom-0 right-0 h-full flex items-center justify-center rounded-r-md px-5 text-sm">
                            Kilogram (KG)
                        </span>
                    </div> */}

					<Button variant="primary" type="submit" className="gap-2 w-full lg:max-w-xs"><FaSearch />Cek Harga</Button>
				</form>
			</Form>
			<div className="w-full mt-5 rounded-lg bg-white p-5 shadow-sm">
				{isError || packagePrice?.status === "FAILED" ?
					<>
						<div className="flex flex-col gap-3 items-center bg-gray-100 justify-center  rounded-lg italic uppercase font-bold py-5">
							<img src="/images/component/paket/notfoundroutepackage.png" alt="" />
							{/* {packagePrice?.message || "Terjadi Kesalahan pada server"} */}
							<span>Fitur paket tidak tersedia untuk rute ini</span>
						</div>
					</>
					:
					<>
						{isLoading ?
							<>
								<Skeleton className="h-44 w-full flex items-center justify-center"><FaCircleNotch className="text-5xl text-gray-500 animate-spin font-thin"></FaCircleNotch></Skeleton>
							</>
							:
							<>
								{isPackageTransaction ?
									<>
										<SendPackageTransaction setValue={setValue} watch={watch} onSendPackage={onSendPackage} onValueUpdate={onValueUpdate} packagePrice={packagePrice} isLoadingFinalPrice={isLoadingFinalPrice} isOpen={isOpen} setIsOpen={setIsOpen} />
									</>
									:
									<>
										{packagePrice && (
											<>
												<CheckPrice packagePrice={packagePrice} handleLanjutKirm={handleLanjutKirm} />
											</>
										)}
									</>
								}
							</>
						}
					</>
				}

			</div>
		</>
	)
}
