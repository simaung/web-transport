import { Button } from "@/components/ui/button"
import config from "@/project.config"
import useSWR from "swr"

export default function CheckPrice({ packagePrice, handleLanjutKirm }: any) {
  const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)

  var getPoint = (id: string) => {
    return dataPoints?.points?.find((c: any) => c.id === id)?.name
  }

  return (
    <>
      <div className="flex flex-col gap-3">
        <span className="font-bold">
          {getPoint(packagePrice?.outlet_from)}
          -
          {getPoint(packagePrice?.outlet_to)}
        </span>
        <span>
          Jenis Paket : {packagePrice.package_type}
        </span>
        <span>
          Berat : {packagePrice.package_weight} Kilogram (KG)
        </span>
        <span>
          Harga : Rp.{packagePrice.outlet_price.outlet_price.toLocaleString('id-ID')}
        </span>
      </div>
      <Button className="w-full mt-3" variant={"primary"} onClick={handleLanjutKirm}>Lanjutkan Kirim paket</Button>
    </>
  )
}