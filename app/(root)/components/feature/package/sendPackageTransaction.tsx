/* eslint-disable @next/next/no-img-element */
import { Button } from "@/components/ui/button"
import TypeAndDimension from "./typeAndDimension"
import DrofoffAndPickup from "./dropoffAndPickup"
import FinalPrice from "./finalPrice"
import useSWR from "swr"
import config from "@/project.config"
import { useState } from "react"
import ModalSenderReceiver from "./modal/modalSenderReceiver"

export default function SendPackageTransaction({ setValue, watch, onSendPackage, onValueUpdate, packagePrice, isLoadingFinalPrice, isOpen, setIsOpen }: any) {
  const package_type = watch('package_type');
  const { data: packageType, error: packageError, isLoading: packageIsLoading } = useSWR(`${config.apiUrl}/v1/package/type`)

  const packageList = packageType ? packageType?.package_type : []

  const selectedPackage = packageList.find((e: any) => e.code === package_type)
  const parameter = selectedPackage?.parameter || null;
  const shipping = selectedPackage?.shipping === "y" ? true : false;

  return (
    <>
      <ModalSenderReceiver
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        onSendPackage={onSendPackage}
        setValue={setValue}
        watch={watch}
      />
      <div className="flex flex-col gap-9">
        <TypeAndDimension setValue={setValue} watch={watch} packageList={packageList} parameter={parameter} onValueUpdate={onValueUpdate} />
        <DrofoffAndPickup setValue={setValue} watch={watch} shipping={shipping} onValueUpdate={onValueUpdate} />
        <FinalPrice packagePrice={packagePrice} isLoading={isLoadingFinalPrice} />
        <div className="flex flex-col gap-3 -mt-5">
          <Button variant={'primary'} className="w-full" onClick={onValueUpdate}>Kalkulasi Harga</Button>
          <small className="text-xs text-gray-600 italic"><span className="text-red-500">Fitur ini hanya untuk mengkalkulasi harga</span><br />Untuk mengirim paket anda harus membawa paketnya ke outlet asal untuk selanjutnya petugas akan mengatur pengiriman paket anda</small>
        </div>
        {/* <Button variant={"primary"} className="w-full" onClick={() => setIsOpen(true)}>Lanjutkan Pengiriman</Button> */}
      </div>
    </>
  )
}