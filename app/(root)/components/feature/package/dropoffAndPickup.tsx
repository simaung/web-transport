import { useEffect, useState } from "react"
import Maptiler from "../../map/maptiler"
import { Checkbox } from "@/components/ui/checkbox"
import { Button } from "@/components/ui/button"
import useSWR from "swr"
import config from "@/project.config"
import ModalGeoLocation from "./modal/modalGeoLocation"
import moment from "moment"

interface antarJemputSchema {
  antar: boolean,
  jemput: boolean
}
interface centerMapSchema {
  lat: number,
  lng: number
}
export default function DrofoffAndPickup({ shipping, watch, setValue, onValueUpdate }: any) {
  const currentTime = parseInt(moment().format('H'))
  const depart = watch('depart')
  const arrival = watch('arrival')
  const senderLocation = watch('sender')
  const receiverLocation = watch('receiver')

  const [antarJemput, setAntarJemput] = useState<antarJemputSchema>({ antar: false, jemput: false })
  const [centerMap, setCenterMap] = useState<centerMapSchema>()
  const [isOpen, setIsOpen] = useState(false);
  const [type, setType] = useState("")

  const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)

  var getPoint = (id: string) => {
    return dataPoints.points.find((c: any) => c.id === id)
  }

  const selectedDepart = getPoint(depart);
  const selectedArrival = getPoint(arrival)

  const handleLocationChange = (lat: number, lng: number) => {
    setCenterMap({ lat, lng });
    if (type === 'jemput') {
      setValue('sender.pickup_latitude', lat)
      setValue('sender.pickup_longitude', lng)
    }
    if (type === 'antar') {
      setValue('receiver.delivery_latitude', lat)
      setValue('receiver.delivery_longitude', lng)
    }
  };

  function handleOpenModal(name: string) {
    setType(name);
    if (name === 'jemput') {
      setCenterMap({ lat: parseFloat(senderLocation?.pickup_latitude) || parseFloat(selectedDepart.latitude), lng: parseFloat(senderLocation?.pickup_longitude) || parseFloat(selectedDepart.longitude) })
    }
    if (name === 'antar') {
      setCenterMap({ lat: parseFloat(receiverLocation?.delivery_latitude) || parseFloat(selectedArrival.latitude), lng: parseFloat(receiverLocation?.delivery_longitude) || parseFloat(selectedArrival.longitude) })
    }
    setIsOpen(true);
  }

  return (
    <>
      <ModalGeoLocation isOpen={isOpen} setIsOpen={setIsOpen} handleLocationChange={handleLocationChange} center={centerMap} onValueUpdate={onValueUpdate} />
      <div className={shipping ? 'block' : 'hidden'}>
        <div className="font-bold text-xl">
          Jemput & Antar (opsional)
          <br />
          <small className="font-normal text-sm text-red-500">Jam operasional layanan jemput dimulai dari jam 8.00 s/d 15.00 WIB.</small>
        </div>
        <div className="flex lg:flex-row flex-col lg:gap-9 gap-5">
          <div className="md:w-96 w-full">
            <div className="flex flex-row items-center gap-3 lg:mt-3">
              <Checkbox
                id="jemput"
                disabled={(currentTime < 8 || currentTime > 15) ? true : (selectedDepart?.latitude === null || selectedDepart?.longitude === null) ? true : false}
                onCheckedChange={(e: boolean) => {
                  setValue('is_pickup', e)
                  setAntarJemput({ ...antarJemput, jemput: e });
                  if (e) {
                    setCenterMap({ lat: parseFloat(senderLocation?.pickup_latitude) || parseFloat(selectedDepart.latitude), lng: parseFloat(senderLocation?.pickup_longitude) || parseFloat(selectedDepart.longitude) })
                  }
                }}
              />
              <label htmlFor="jemput">Jemput</label>
            </div>
            {
              selectedDepart?.latitude === null || selectedDepart?.longitude === null && (
                <>
                  <span className="text-red-500">
                    Maaf fitur jemput tidak tersedia untuk outlet ini <span className="font-bold">{selectedDepart.name}</span>
                  </span>
                </>
              )
            }
            {antarJemput.jemput && (
              <>
                <div className="h-52 w-full bg-gray-400 mt-3">
                  <Maptiler latLng={{ lat: parseFloat(senderLocation?.pickup_latitude) || parseFloat(selectedDepart.latitude), lng: parseFloat(senderLocation?.pickup_longitude) || parseFloat(selectedDepart.longitude) }} key={parseFloat(senderLocation?.pickup_longitude) || parseFloat(selectedDepart.longitude)} />
                </div>
                <Button
                  variant={'primary'}
                  className="w-full mt-3"
                  onClick={() => handleOpenModal('jemput')}
                >
                  Ganti lokasi jemput
                </Button>
              </>
            )}
            <textarea name="sender_address" id="sender_address" placeholder="Alamat jemput (alamat, patokan, warna rumah)" className="border-2 outline-secondary p-3 rounded-md w-full mt-3"
              onChange={(e) => {
                setValue('sender.pickup_address', e.target.value)
              }}
              disabled={!antarJemput.jemput}
            >
            </textarea>
          </div>
          <div className="md:w-96 w-full">
            <div className="flex flex-row items-center gap-3 lg:mt-3">
              <Checkbox
                id="antar"
                disabled={selectedArrival?.latitude === null || selectedArrival?.longitude === null ? true : false}
                onCheckedChange={(e: boolean) => {
                  setValue('is_delivery', e)
                  setAntarJemput({ ...antarJemput, antar: e });
                  if (e) {
                    setCenterMap({ lat: parseFloat(receiverLocation?.delivery_latitude) || parseFloat(selectedArrival.latitude), lng: parseFloat(receiverLocation?.delivery_longitude) || parseFloat(selectedArrival.longitude) })
                  }
                }} />
              <label htmlFor="antar">Antar</label>
            </div>
            {
              selectedArrival?.latitude === null || selectedArrival?.longitude === null && (
                <>
                  <span className="text-red-500">
                    Maaf fitur antar tidak tersedia untuk outlet <span className="font-bold">{selectedArrival.name}</span>
                    {/* {JSON.stringify(centerMap) + ' latlng outlet antar'} */}
                  </span>
                </>
              )
            }
            {antarJemput.antar && (
              <>
                <div className="h-52 w-full bg-gray-400 mt-3">
                  <Maptiler latLng={{ lat: parseFloat(receiverLocation?.delivery_latitude) || parseFloat(selectedArrival.latitude), lng: parseFloat(receiverLocation?.delivery_longitude) || parseFloat(selectedArrival.longitude) }} key={parseFloat(receiverLocation?.delivery_longitude) || parseFloat(selectedArrival.longitude)} />
                </div>
                <Button variant={'primary'} className="w-full mt-3" onClick={() => handleOpenModal('antar')}>Ganti lokasi antar</Button>
              </>
            )}
            <textarea
              name="recipient_address"
              id="recipient_address"
              placeholder="Alamat antar (alamat, patokan, warna rumah)"
              className="border-2 outline-secondary p-3 rounded-md w-full mt-3"
              onChange={(e) => {
                setValue('receiver.delivery_address', e.target.value)
              }}
              disabled={!antarJemput.antar}
            >
            </textarea>
          </div>
        </div>
      </div>
    </>
  )
}