/* eslint-disable @next/next/no-img-element */
import PackageSvg from "@/components/image/package";
import SelectOption from "@/components/selectOption";
import { Button } from "@/components/ui/button";
import config from "@/project.config";
import useSWR from "swr";

export default function TypeAndDimension({ setValue, watch, packageList, parameter, onValueUpdate }: any) {
  const package_type = watch('package_type');
  const weight = watch('weight');
  const width = watch('width');
  const length = watch('length');
  const height = watch('height');

  let jenisPaket: { label: any; value: any }[] = [];
  packageList.forEach((e: any) => {
    jenisPaket.push({ label: e.name, value: e.code })
  });

  // const handleDimensi = (width: number, height: number, length: number, weight: number) => {
  //   setValue('width', String(width))
  //   setValue('height', String(height))
  //   setValue('length', String(length))
  //   setValue('weight', String(weight))
  //   onValueUpdate()
  // }

  const handleDimensi = ({ name, value }: any) => {
    setValue(name, String(value))
    // onValueUpdate()
  }

  // const kalkulasi = () => {
  //   onValueUpdate();
  // }


  return (
    <div>
      <div className="font-bold text-lg">
        Jenis & Dimensi
      </div>
      <div className="flex flex-col gap-5 mt-3">
        <SelectOption
          value={package_type}
          onValueChange={(val: string) => {
            setValue('package_type', val);
            onValueUpdate();
          }}
          data={jenisPaket}
          title={"Jenis Paket"}
          className="w-full md:max-w-xl"
        />
        <div className={`w-full ${!parameter ? "hidden" : "block"}`}>
          {/* <div className="flex flex-row flex-wrap gap-3 justify-start">
            <div
              className={`md:w-56 w-full p-3 rounded-md shadow-md hover:shadow-lg duration-150 cursor-pointer hover:border-secondary border-2 ${weight === "1" ? "border-secondary" : ""}`}
              onClick={() => {
                handleDimensi(8, 8, 8, 1);
              }}
            >
              <div className="flex flex-row gap-3 items-center">
                <img src="/images/component/paket/Document.png" alt="..." className="w-16 h-16" />
                <div>
                  <small className="text-xs">
                    {"< 1 KG"}
                  </small>
                  <div className="font-bold">Dokumen</div>
                  <small>(Maks amplop A3)</small>
                </div>
              </div>
            </div>
            <div
              className={`md:w-56 w-full p-3 rounded-md shadow-md hover:shadow-lg duration-150 cursor-pointer hover:border-secondary border-2 ${weight === "5" ? "border-secondary" : ""}`}
              onClick={() => {
                handleDimensi(20, 20, 8, 5)
              }}
            >
              <div className="flex flex-row gap-3 items-center">
                <img src="/images/component/paket/Small.png" alt="..." className="w-16 h-16" />
                <div>
                  <small className="text-xs">
                    {"+- 5 KG"}
                  </small>
                  <div className="font-bold">Kecil</div>
                  <small>20x20x8</small>
                </div>
              </div>
            </div>
            <div
              className={`md:w-56 w-full p-3 rounded-md shadow-md hover:shadow-lg duration-150 cursor-pointer hover:border-secondary border-2 ${weight === "10" ? "border-secondary" : ""}`}
              onClick={() => {
                handleDimensi(40, 40, 4, 10)
              }}
            >
              <div className="flex flex-row gap-3 items-center">
                <img src="/images/component/paket/Medium.png" alt="..." className="w-16 h-16" />
                <div>
                  <small className="text-xs">
                    {"+- 10 KG"}
                  </small>
                  <div className="font-bold">Sedang</div>
                  <small>40x40x4</small>
                </div>
              </div>
            </div>
            <div
              className={`md:w-56 w-full p-3 rounded-md shadow-md hover:shadow-lg duration-150 cursor-pointer hover:border-secondary border-2 ${weight === "15" ? "border-secondary" : ""}`}
              onClick={() => {
                handleDimensi(35, 30, 25, 15)
              }}
            >
              <div className="flex flex-row gap-3 items-center">
                <img src="/images/component/paket/Large.png" alt="..." className="w-16 h-16" />
                <div>
                  <small className="text-xs">
                    {"+- 15 KG"}
                  </small>
                  <div className="font-bold">Besar</div>
                  <small>35x30x25</small>
                </div>
              </div>
            </div>
            <div
              className={`md:w-56 w-full p-3 rounded-md shadow-md hover:shadow-lg duration-150 cursor-pointer hover:border-secondary border-2 ${weight === "20" ? "border-secondary" : ""}`}
              onClick={() => {
                handleDimensi(40, 40, 40, 20)
              }}
            >
              <div className="flex flex-row gap-3 items-center">
                <img src="/images/component/paket/exs-large.png" alt="..." className="w-16 h-16" />
                <div>
                  <small className="text-xs">
                    {"+- 20 KG"}
                  </small>
                  <div className="font-bold">Lebih Besar</div>
                  <small>40x40x40</small>
                </div>
              </div>
            </div>
          </div> */}
          {/* <div className="relative w-fit bg-red-50">
            <PackageSvg />
          </div> */}
          <div className="w-full flex flex-col gap-3 border-t-2 pt-5 max-w-xl text-xs">
            <div className="flex flex-row gap-3 w-full">
              <div>
                <label htmlFor="length">Panjang</label>
                <input type="number" name="length" id="length" className="text-center border-2 rounded-lg text-sm p-3 w-full" placeholder="Panjang(cm)" onChange={({ target }) => { handleDimensi(target) }} value={length} />
              </div>
              <div>
                <label htmlFor="width">Lebar</label>
                <input type="number" name="width" id="width" className="text-center border-2 rounded-lg text-sm p-3 w-full" placeholder="Lebar(cm)" onChange={({ target }) => { handleDimensi(target) }} value={width} />
              </div>
              <div>
                <label htmlFor="height">Tinggi</label>
                <input type="number" name="height" id="height" className="text-center border-2 rounded-lg text-sm p-3 w-full" placeholder="Tinggi(cm)" onChange={({ target }) => { handleDimensi(target) }} value={height} />
              </div>
            </div>
            <div>
              <label htmlFor="weight">Berat (kg)</label>
              <input type="number" name="weight" id="weight" className="text-center border-2 rounded-lg text-sm p-3 w-full" placeholder="Berat(kg)" onChange={({ target }) => { handleDimensi(target) }} value={weight} />
            </div>
          </div>
        </div>
      </div>
    </div >
  )
}