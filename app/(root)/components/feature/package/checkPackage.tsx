/* eslint-disable @next/next/no-img-element */
"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"

import { Button } from "@/components/ui/button"
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { FaCircle, FaMoneyBillWave, FaSearch } from "react-icons/fa"
import config from "@/project.config"
import { GET, POST } from "@/lib/http"
import { useEffect, useState } from "react"
import { toast } from "@/components/ui/use-toast"
import Barcode from "@/components/Barcode"
import Link from "next/link"
// import moment from "moment"
// import 'moment/locale/id'

const formSchema = z.object({
    packageCode: z.string()
        .min(14, {
            message: "Kode Paket minimal harus 14 karakter.",
        })
        .max(14, {
            message: "Kode Paket maksimal 14 karakter.",
        }),
})

interface packageDataSchema {
    "code": string,
    "pickup": string,
    "delivery": string,
    "time": string,
    "date": string,
    "transaction_status": string,
    "payment_status": string,
    "total_price": number,
    "sender": {
        "name": string,
        "phone": string,
        "address": string,
        "latitude": number,
        "longitude": number
    },
    "recipient": {
        "name": string,
        "phone": string,
        "address": string,
        "latitude": number,
        "longitude": number
    },
    "outlet_from": {
        "code": string,
        "name": string,
        "address": string,
        "contact": string,
        "open_time": string,
        "close_time": string,
        "latitude": string,
        "longitude": string,
        "regency_id": string,
        "regencies": {
            "id": string,
            "name": string
        }
    },
    "outlet_to": {
        "code": string,
        "name": string,
        "address": string,
        "contact": string,
        "open_time": string,
        "close_time": string,
        "latitude": string,
        "longitude": string,
        "regency_id": number,
        "regencies": {
            "id": number,
            "name": string
        }
    },
    "tracking": []
}

export const CheckPackage = ({ kode }: any) => {
    const [packageData, setPackageData] = useState<packageDataSchema>()
    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            packageCode: "",
        },
    })
    const { register, handleSubmit, setValue, watch, getFieldState, getValues } = form;
    useEffect(() => {
        if (kode) {
            setValue('packageCode', kode || "");
            document.getElementById('btnCekPaket')?.click();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    async function onSubmit(values: z.infer<typeof formSchema>) {
        try {
            const { data } = await GET('/v1/package/check', { no_resi: values?.packageCode })
            if (data.status === 'SUCCESS') {
                setPackageData(data?.packages)
                // toast({
                //     variant: "success",
                //     title: 'Berhasil',
                //     description: 'Berhasil mendapatkan data cek paket'
                // })
            } else {
                setPackageData(undefined)
                toast({
                    variant: 'destructive',
                    title: 'Gagal',
                    description: data.message
                })
            }
        } catch (error) {
            setPackageData(undefined)
            toast({
                variant: 'destructive',
                title: 'Gangguan',
                description: "Fitur paket sedang dalam perbaikan"
            })
        }
    }

    return (
        <>
            <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="p-5 rounded-lg shadow-sm bg-white">
                    <FormField
                        control={form.control}
                        name="packageCode"
                        render={({ field }) => (
                            <FormItem className="">
                                {/* <FormLabel className="">
                                Cek Paket
                            </FormLabel> */}
                                <FormMessage className="" />
                                <div className="flex md:flex-row flex-col gap-3">
                                    <FormControl className="">
                                        <Input placeholder={`Kode berawalan ${'P' + config.appCode}`} {...field} className="uppercase" />
                                    </FormControl>
                                    <Button variant="primary" type="submit" className="w-full md:max-w-56" id="btnCekPaket"><FaSearch />Cek</Button>
                                </div>
                            </FormItem>
                        )}
                    />
                </form>
            </Form>
            {
                packageData && (
                    <div className="w-full mt-4 max-w-lg mx-auto">
                        {watch('packageCode').length === 14 && (
                            <div className="bg-white p-5 rounded-lg shadow-md border-2">
                                <div className="flex flex-row justify-between items-baseline border-b-2 pb-2 mb-3">
                                    <img src="/images/logo/logo.png" alt="" className="h-7" />
                                    <h3 className="text-lg font-bold uppercase">data paket</h3>
                                </div>
                                <div className="flex flex-col gap-3">
                                    <h3 className="text-lg font-bold w-full text-center border-2 py-3 border-black"><span className="hidden md:block">Nomor Resi :</span> {packageData?.code}</h3>
                                    <Barcode value={packageData?.code} />
                                    <div className="w-full flex flex-row gap-3 justify-between pb-3 border-b-2 border-dashed">
                                        <span className="border-2 border-black w-full text-center py-3">
                                            {packageData?.outlet_from?.name}
                                        </span>
                                        <span className="border-2 border-black w-full text-center py-3">
                                            {packageData?.outlet_to?.name}
                                        </span>
                                    </div>
                                    <div className="w-full flex flex-col md:flex-row gap-3 md:justify-between pb-3 border-b-2 border-dashed">
                                        <div>
                                            <span className="font-bold uppercase underline">
                                                Penerima
                                            </span>
                                            <br />
                                            {packageData?.recipient?.name}
                                            <br />
                                            {packageData?.recipient?.phone}
                                            <br />
                                            {packageData?.recipient?.address}
                                        </div>
                                        <div className="md:text-right">
                                            <span className="font-bold uppercase underline">
                                                Pengirim
                                            </span>
                                            <br />
                                            {packageData?.sender?.name}
                                            <br />
                                            {packageData?.sender?.phone}
                                            <br />
                                            {packageData?.sender?.address}
                                        </div>
                                    </div>
                                    <div className="w-full flex flex-row gap-3 justify-between pb-3 border-b-2 border-dashed">
                                        <span className="w-fit px-5 border-black font-bold uppercase text-center border-2">{packageData?.transaction_status}</span>
                                        <span className="w-full border-2 text-center border-black">
                                            {packageData?.date} {packageData?.time}
                                        </span>
                                    </div>
                                    {
                                        packageData?.payment_status !== 'paid'
                                            ?
                                            <>
                                                <Link href={`/package/${packageData?.code}`}>
                                                    <Button variant="primary" type="button" className="w-full flex items-center justify-center gap-1"><FaMoneyBillWave /> Bayar</Button>
                                                </Link>
                                            </>
                                            :
                                            <>
                                                <div className="flex flex-col">
                                                    {
                                                        // JSON.stringify(packageData.tracking)
                                                        packageData.tracking.map((e: any, i: number) => (
                                                            <>
                                                                <div className="flex justify-between">
                                                                    <div className="flex items-center justify-center w-11 relative">
                                                                        <div className={`h-full w-[2px] bg-primary absolute ${i === 0 ? 'bg-primary' : ''}`}></div>
                                                                        <FaCircle className={`text-xs text-primary ${i === 0 ? 'text-primary' : ''}`} />
                                                                    </div>
                                                                    <div className="w-full py-2">
                                                                        {/* <span className="text-primary font-bold text-sm">{e.status}</span> */}
                                                                        <p className={`text-sm border-b-2 border-gray-100 border-dotted ${i === 0 ? 'font-bold text-primary' : ''}`}>{e.note}</p>
                                                                        <small className="text-xs italic">{e.date + ' Pukul ' + e.time + ' WIB'}</small>
                                                                    </div>
                                                                </div>
                                                            </>
                                                        ))
                                                    }
                                                </div>
                                            </>
                                    }
                                </div>
                            </div>
                        )}
                    </div >
                )
            }
        </>
    )
}