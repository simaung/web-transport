"use client"

import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Input } from "@/components/ui/input";
import useSWR from "swr";
import config from "@/project.config";
import { Checkbox } from "@/components/ui/checkbox";
import { useState } from "react";

const TermUseModal = ({ isOpen, setIsOpen, onSubmit }: any) => {
  const [agre, setAgre] = useState(false);
  const { data, isLoading } = useSWR(`${config.apiUrl}/v1/term-condition/payment`)
  return (
    <Dialog open={isOpen} onOpenChange={() => { setIsOpen(false); onSubmit(false); setAgre(false) }}>
      <DialogContent className="max-w-[630px] md:min-w-[500px]">
        <DialogHeader>
          <DialogTitle className="pb-2 border-b-2">Syarat & Ketentuan</DialogTitle>
        </DialogHeader>
        <DialogDescription>
          {/* {JSON.stringify(data)} */}
          <div className="text-sm text-justify text-gray-800 m-0 max-w-[630px] md:min-w-[500px] leading-relaxed " dangerouslySetInnerHTML={{ __html: data?.term_condition?.value?.replace(/\n/g, "").replace('<ol>', '<ol className="list-decimal list-inside" style="margin:0;padding:0;padding-left:15px">') }}></div>
          {/* <div className="flex flex-row gap-2 items-center mt-3">
            <Checkbox id={'agre'} className='' checked={agre} disabled={false} onClick={() => setAgre(!agre)} />
            <label htmlFor="agre" className="text-sm">Setuju dengan Syarat & Ketentuan</label>
          </div> */}
        </DialogDescription>
        <DialogFooter>
          <Button variant={'primary'} className="w-full"
            onClick={() => {
              setIsOpen(false);
              // onSubmit(true);
            }}
          // disabled={!agre}
          >
            Selesai
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}

export default TermUseModal