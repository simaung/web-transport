"use client"

import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Input } from "@/components/ui/input";
import { GET, POST } from "@/lib/http";
import useSWR from "swr";
import config from "@/project.config";
import cookie from "@/lib/cookies";
import { useEffect, useState } from "react";
import { getItem } from "@/lib/localStorage";

const CheckoutModal = ({ isOpen, setIsOpen, onChange, data, onSubmit }: any) => {
  const [listVoucher, setListVoucher] = useState([])
  const bookingForm = getItem('bookingForm');
  useEffect(() => {
    if (isOpen && cookie.getToken()) {
      getVoucher();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOpen]);

  const getVoucher = async () => {
    console.log("Calling getVoucher...");
    const data = await GET("/v1/vouchers", {
      depart_code: bookingForm.pickup_point,
      arrival_code: bookingForm.dropoff_point,
      depart_date: bookingForm.departure_date
    });
    setListVoucher(data.data.voucher)
  };
  return (
    <Dialog open={isOpen} onOpenChange={() => setIsOpen(false)}>
      <DialogContent className="max-w-[380px] md:min-w-[500px]">
        <DialogHeader>
          <DialogTitle className="pb-2 border-b-2">KODE VOUCHER</DialogTitle>
        </DialogHeader>
        <DialogDescription>
          <Input
            type="primary"
            placeholder="Kode Voucher"
            className="text-center outline-none ring-0 border-2"
            value={data?.voucher_code}
            onChange={(e) => {
              onChange({ ...data!, voucher_code: e?.target?.value });
            }}
          />
          {cookie.getToken() && (
            <div className="flex flex-col gap-3 mt-3">
              {listVoucher.length > 0 ?
                listVoucher.map((e: any) => (
                  <>
                    <div className={`p-3 border-2 rounded-lg hover:border-primary cursor-pointer hover:text-black duration-200 ${e?.promotion_code === data?.voucher_code ? 'border-primary text-black' : ''}`} onClick={() => { onChange({ ...data!, voucher_code: e?.promotion_code }) }}>
                      <div className="font-bold">
                        {e?.name || "-"}
                      </div>
                      <div className="text-xs">
                        {e?.description || "-"}
                      </div>
                    </div>
                  </>
                ))
                :
                <>
                  <div className="text-center italic text-sm">
                    Anda tidak memiliki voucher...
                  </div>
                </>
              }
            </div>
          )}

        </DialogDescription>
        <DialogFooter>
          <Button variant={'primary'} className="w-full"
            onClick={() => {
              setIsOpen(false);
              onSubmit(false);
            }}
          >
            Simpan Voucher
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}

export default CheckoutModal