"use client"

import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { Input } from "@/components/ui/input";

const BoardingModal = ({ isOpen, setIsOpen, onChange, data, onSubmit }: any) => {
  return (
    <Dialog open={isOpen} onOpenChange={() => setIsOpen(false)}>
      <DialogContent className="max-w-[380px] md:min-w-[500px]">
        <DialogHeader>
          <DialogTitle className="pb-2 border-b-2">Tiket Boarding</DialogTitle>
        </DialogHeader>
        <DialogDescription>
          <Input
            type="primary"
            placeholder="Boarding Tiket"
            className="text-center outline-none ring-0 mt-3 border-2"
            value={data?.boarding_ticket}
            onChange={(e) => {
              onChange({ ...data!, boarding_ticket: e?.target?.value });
            }}
          />
        </DialogDescription>
        <DialogFooter>
          <Button variant={'primary'} className="w-full"
            onClick={() => {
              setIsOpen(false);
              onSubmit(false);
            }}
          >
            Simpan Tiket Boarding
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}

export default BoardingModal