import { Label } from "@radix-ui/react-dropdown-menu";

/* eslint-disable @next/next/no-img-element */
export default function MainBanner({ label }: any) {
  return (
    <>
      <div className="h-[40vh] w-full relative -mb-11 -z-10">
        <img src="/images/background/bgarnes.png" alt="" className="absolute top-0 left-0 h-full w-full object-cover z-0 opacity-50" />
        <div className="flex h-full w-full bg-gradient-to-b from-orange-300 to-white items-center justify-center">
          {label ?
            <span className="text-2xl font-bold uppercase z-10">{label}</span>
            :
            <img src="/images/logo/logo.png" alt="..." className="z-0 pb-11" />
          }
        </div>
      </div>
    </>
  )
}