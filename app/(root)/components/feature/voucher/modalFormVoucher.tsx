import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { FaTimes } from "react-icons/fa";
import * as EmailValidator from 'email-validator';


export default function ModalFormVoucher({ isOpen, setIsOpen, onSubmit, value, setValue }: any) {
  function handleChange(e: any) {
    const { target } = e
    setValue({ ...value, [target.name]: target.value })
  }
  return (
    <>
      <div className={`fixed top-0 left-0 bg-gray-900 bg-opacity-85 backdrop-blur-sm w-full h-screen ${isOpen ? "block" : "hidden"} z-50`}>
        <div className="flex items-center justify-center w-full h-screen">
          <div className="container">
            <div className="bg-white rounded-lg shadow-lg p-5 flex flex-col gap-3">
              <div className="flex flex-row justify-between items-center border-b-2 pb-1">
                <span className="font-bold">Data Pemesan</span>
                <FaTimes className="text-gray-300 cursor-pointer" onClick={() => setIsOpen(false)} />
              </div>
              <div className="flex flex-col gap-3">
                <Input type="text" placeholder="Nama" name="name" onChange={handleChange} value={value?.name} />
                <Input type="number" placeholder="Nomor Telepon" name="phone" onChange={handleChange} value={value?.phone} />
                <Input type="email" placeholder="Email" name="email" onChange={handleChange} value={value?.email} />
              </div>
              <Button variant={'primary'} onClick={onSubmit} className="w-full" disabled={value.name && EmailValidator.validate(value.email) ? false : true}>Lanjutkan Pesan Voucher</Button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}