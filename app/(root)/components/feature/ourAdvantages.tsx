import { FaHeadset, FaMoneyBill, FaMoneyBillWave, FaRegSmileWink, FaRoad, FaShieldAlt, FaStopwatch, FaUserShield } from "react-icons/fa";

export default function OurAdvantages() {
  return (
    <>
      <div className="container">
        <div className="font-bold text-lg lg:text-3xl text-center w-full mb-5 lg:mb-16 uppercase">
          Arnes siap menemani semangatmu!
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-5 lg:gap-11">
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaMoneyBillWave className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Perjalanan Nyaman Tanpa Merogoh Kocek
            </div>
            <span>
              Kami yakin, perjalanan yang menyenangkan tidak perlu menguras dompet anda. Layanan Arnes Shuttle memberikan solusi transportasi yang ramah dikantong tanpa mengorbankan kualitas layanan.
            </span>
          </div>
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaRegSmileWink className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Kenyamanan Utama, Perjalanan Tak Terlupakan
            </div>
            <span>
              Jangan lagi berkompromi dengan kenyamanan perjalanan. Arnes Shuttle hadir untuk memastikan setiap momen perjalanan anda diliputi oleh kesenangan yang tidak terlupakan.
            </span>
          </div>
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaUserShield className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Prioritas Utama Keamanan Anda
            </div>
            <span>
              Dipandu oleh tim pengemudi berlisensi profesional, anda dapat yakin bahwa perjalanan tidak hanya nyaman tetapi juga dipandu oleh ahli yang berdedikasi pada keamanan dan kenyamanan penumpang.
            </span>
          </div>
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaRoad className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Solusi Transportasi
            </div>
            <span>
              Arnes Shuttle, jembatan menuju petualangan tanpa batas! Nikmati kenyamanan perjalanan dengan rute luas kami yang melibatkan destinasi populer, memberikan anda akses cepat dan efisien ke setiap tujuan impian Anda.
            </span>
          </div>
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaHeadset className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Layanan Customer Service Profesional
            </div>
            <span>
              Nikmati layanan customer service Arnes Shuttle yang ramah dan profesional, siap membantu setiap langkah perjalanan Anda. Dengan kami, kepuasan pelanggan bukan hanya tujuan, melainkan suatu jaminan.
            </span>
          </div>
          <div className="w-full p-11 rounded-lg shadow-lg text-center h-full">
            <center>
              <div className="w-fit p-3 rounded-full shadow-lg">
                <FaStopwatch className="text-primary text-7xl text-center" />
              </div>
            </center>
            <div className="text-lg font-bold my-3 h-12">
              Fleksibilitas Tinggi
            </div>
            <span>
              Dengan berbagai jadwal keberangkatan, anda memiliki keleluasaan untuk memilih waktu yang paling sesuai dengan agenda anda, tak perlu khawatir tentang keberangkatan yang terbatas, karena Arnes Shuttle siap mengakomodasi setiap jadwal perjalanan.
            </span>
          </div>
        </div>
      </div>
    </>
  )
}