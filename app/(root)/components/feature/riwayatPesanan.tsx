"use client";
import { Button } from "@/components/ui/button";
import { getItem } from "@/lib/localStorage";
import { useState, useEffect } from "react";
import moment from "moment";
import 'moment/locale/id';
import Link from "next/link";
import { storeArray, removeItem } from "@/lib/localStorage";
import { useRouter } from "next/navigation";
// const cron = require('node-cron')

interface bookHistorySchema {
  "booking_id": string,
  "transaction_status": string,
  "payment_status": string,
  "expiration_time": string,
  "expiration_time_zone": string,
  "orderer_name": string,
  "orderer_phone": string,
  "orderer_email": string,
  "pax": number,
  "depart_date": string,
  "price": number,
  "sub_price": number,
  "admin_fee": number,
  "voucher_price": number,
  "total_price": number,
  "route": {
    "depart_code": string,
    "depart_name": string,
    "depart_address": string,
    "arrival_code": string,
    "arrival_name": string,
    "arrival_address": string,
  },
  "payment_data": null,
  "passengers": passengerSchema[],
  "etd": string,
  "eta": string,
  "voucher_code": null,
  "voucher_message": null,
  "tripcode": string,
  "departure_date": string,
  "departure_time": string,
  "pickup_point": string,
  "dropoff_point": string,
  "booking_contact": passengerSchema,
  "depart": { departure_date: string }
}

interface passengerSchema {
  "name": string,
  "phone": string,
  "seat_number": string,
}

export default function RiwayatPesanan() {
  // cron.schedule('*/5 * * * * *', () => {
  //   console.log('Running a task every 5 seconds');
  // });
  const router = useRouter();
  const [bookHistory, setBookHistory] = useState<bookHistorySchema[]>();
  const [loading, setLoading] = useState(true);
  const recent = moment().format('YYYY-MM-DD HH:mm')

  function getHistory() {
    setLoading(true)
    const history = getItem('bookHistory');
    setBookHistory(history);
    setLoading(false);
  }

  useEffect(() => {
    getHistory()
  }, []);

  useEffect(() => {
    if (!loading && bookHistory?.[bookHistory?.length - 1]?.expiration_time) {
      const d1 = new Date(bookHistory[bookHistory?.length - 1]?.expiration_time);
      const d2 = new Date(recent);
      if (d1 <= d2) {
        // console.log(bookHistory, 'ini kadaluarsa ini');
        // alert('asdfasdf')
        removeItem('bookHistory')
        getHistory()
        // setBookHistory(null)
      }
    }
  }, [bookHistory, loading, recent]);

  return (
    <>
      <div className="container">
        <div className="font-bold">
          Pesanan Terakhir
        </div>
        {loading ? (
          <div className="w-full p-5 bg-gray-100 animate-pulse shadow-lg rounded-lg mt-3 h-32 flex items-center justify-center">
            <span>Loading...</span>
          </div>
        ) : (
          <>
            {bookHistory ? (
              <>
                <div className="w-full p-5 bg-white shadow-lg rounded-lg mt-3">
                  <div className="flex flex-col lg:flex-row justify-between gap-3 items-center">
                    <div className="flex flex-col gap-2 w-full">
                      <span className="font-bold text-primary text-xl border-b-2 border-primary w-fit cursor-pointer" onClick={() => router.push(`/reservation?kode=${bookHistory[bookHistory?.length - 1]?.["booking_id"]}`)}>
                        {bookHistory[bookHistory?.length - 1]?.["booking_id"]}
                      </span>
                      <span className="font-bold">
                        {bookHistory[bookHistory?.length - 1]?.["route"]?.['depart_name']} - {bookHistory[bookHistory?.length - 1]?.["route"]?.['arrival_name']}
                      </span>
                      <div className="text-xs">
                        {moment(bookHistory[bookHistory?.length - 1]?.["departure_date"] || bookHistory[bookHistory?.length - 1]?.depart?.departure_date, 'YYYY-MM-DD').locale('id').format('dddd, DD MMMM YYYY')}
                      </div>
                    </div>
                    {/* <Link href={`/reservation?kode=${bookHistory[bookHistory?.length - 1]?.["booking_id"]}`}> */}
                    <Button className="w-full lg:w-fit cursor-pointer" variant={"primary"} onClick={() => router.push(`/reservation?kode=${bookHistory[bookHistory?.length - 1]?.["booking_id"]}`)}>Detail</Button>
                    {/* </Link> */}
                  </div>
                </div>
                {/* <button className="w-full py-2 text-blue-500 underline italic text-xs hover:text-blue-800 duration-300">Lihat Semua Riwayat</button> */}
              </>
            ) : (
              <div className="w-full p-5 bg-gray-50 shadow-lg rounded-lg mt-3 h-32 flex items-center justify-center">
                <span className="italic">Tidak ada pesanan terakhir</span>
              </div>
            )}
          </>
        )}
      </div>
    </>
  );
}
