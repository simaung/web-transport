"use client";
import config from "@/project.config";
import useSWR from "swr";
import ErrorFetch from "../errorFetch";
import moment from 'moment';
import 'moment/locale/id';
import { FaMoneyBillWave } from "react-icons/fa";
import { IoMdPeople, IoMdTime, IoMdTimer } from "react-icons/io";
import DataNotFound from "../dataNotFound";
import { Button } from "@/components/ui/button";
import SkeletonCard from "../skeleton/skeletonCard";
import { timeDuration } from "@/lib/timeDuration";
import Link from "next/link";
import { toast } from "@/components/ui/use-toast";
import { useQRCode } from 'next-qrcode';
import { useRouter } from "next/navigation";
import cookie from "@/lib/cookies";
import { useEffect, useState } from "react";
import { GET } from "@/lib/http";
interface DetailReservationProps {
    bookingCode: string;
    setBookingCode: any;
    setIsLoading: (isLoading: boolean) => void;
}

interface responseProfileSchema {
    "name": string,
    "email": string,
    "phone": string,
    "point": number,
    "type": string,
    "avatar": string,
    "province": string,
    "regencies": string,
    "district": string,
    "villages": string,
}

const DetailReservation: React.FC<DetailReservationProps> = ({ bookingCode, setBookingCode, setIsLoading }) => {
    const Router = useRouter();
    // const { setIsLoading } = bookingCode;
    const [profileData, setProfileData] = useState<responseProfileSchema>();
    const { Canvas } = useQRCode();
    const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/booking/${bookingCode}`)
    const getProfile = async () => {
        const { data } = await GET("/v1/account/profile", {})
        if (data.status === 'SUCCESS') {
            setProfileData(data.data);
        }
    }
    useEffect(() => {
        if (cookie.getToken()) {
            getProfile();
        }
    }, [])

    if (isLoading) return (
        <div className="pt-10">
            <SkeletonCard times={1} />
        </div>
    )
    if (error) return (
        <ErrorFetch />
    )
    setIsLoading(false);
    function diff_hours() {
        const dateTimePatern = 'YYYY-MM-DD HH:mm:ss'
        var d1 = moment(data?.result?.depart_date, dateTimePatern).format('YYYY-MM-DD');
        var d2 = moment(data?.result?.depart_date, dateTimePatern).format('YYYY-MM-DD');
        if (data.result.eta < data.result.etd) {
            d1 = moment(data?.result?.depart_date, dateTimePatern).add(-1, 'day').format('YYYY-MM-DD')
        }
        const startDate = moment(d1 + ' ' + data.result.etd, dateTimePatern).format(dateTimePatern)
        const endDate = moment(d2 + ' ' + data.result.eta, dateTimePatern).format(dateTimePatern)
        return timeDuration(startDate, endDate);
    }

    const copyText = (entryText: string) => {
        navigator.clipboard.writeText(entryText);
        toast({
            variant: "default",
            title: "Clipboard ditambahkan",
            description: "Nomor Virtual Account di tambahkan ke clipboard"
        });
    };

    const isNotFromAllowedSources =
        !["whatsapp", "web", "mobile", "outlet"].includes(data?.result?.order_from);

    const isNotTicketed = data?.result?.transaction_status !== "ticketed";

    const isPaymentOvertime =
        moment(data?.result?.expiration_time, "YYYY-MM-DD HH:mm:ss").format("YYYYMMDDHHmmss") >=
        moment(data?.result?.depart_date, "YYYY-MM-DD HH:mm:ss").format("YYYYMMDDHHmmss");

    const roundTrip = data?.result?.round_trip;

    return (
        <div>
            {data && (
                <div>
                    {(data.status == 'SUCCESS') ? (
                        <div className="flex bg-secondary p-4 text-slate-500 rounded-lg mt-11">
                            <div className="flex w-full bg-white">
                                <div className="flex md:flex-row flex-col w-full justify-between">
                                    <div className="p-4 w-full">
                                        <div className="flex flex-col md:flex-row justify-between text-center text-sm items-center p-2 border-b-2 md:border-b-0 border-dashed border-secondary space-y-4">
                                            <div className=" w-full md:text-start">
                                                <p>Kode Booking</p>
                                                <span className="text-lg font-bold">{data.result.booking_id}</span>
                                            </div>
                                            <div className="text-sm w-full">
                                                <p>Tanggal Keberangkatan</p>
                                                <span className="font-bold text-lg">{moment(data.result.depart_date).format('dddd, DD MMMM YYYY')}</span>
                                            </div>
                                            <div className="w-full md:text-end">
                                                <p>Status Transaksi</p>
                                                <span className={`
                                                    font-semibold text-lg
                                                    ${data.result.transaction_status == 'ticketed' ? 'text-green-600' : 'text-red-500'}
                                                `}
                                                >
                                                    {data.result?.transaction_status?.toUpperCase()}
                                                </span>
                                            </div>
                                        </div>
                                        <div className="flex flex-col md:flex-row justify-between text-center text-sm items-center p-2 space-y-4">
                                            <div className="md:text-left w-full">
                                                Outlet Asal
                                                <br />
                                                <span className="font-bold text-lg">{data.result.route.depart_name}</span>
                                            </div>
                                            <div className="text-center w-full">
                                                <span>Lama Perjalanan</span><br />
                                                <span className="font-bold text-lg">
                                                    {diff_hours()}
                                                </span>
                                            </div>
                                            <div className="md:text-right w-full">
                                                Outlet Tujuan
                                                <br />
                                                <span className="font-bold text-lg">{data.result.route.arrival_name}</span>
                                            </div>
                                        </div>
                                        <div className="md:flex md:flex-row grid grid-cols-2 gap-3 justify-between text-center text-xs md:text-sm items-center md:p-2 mt-4 pb-4 md:border-b-0 border-b-2 border-dashed border-secondary">
                                            <p className="flex flex-col text-center items-center">
                                                <IoMdPeople size={25} />
                                                <span>Penumpang</span>
                                                <span className="font-semibold">{data.result.pax} orang</span>
                                            </p>
                                            <p className="flex flex-col text-center items-center">
                                                <IoMdTime size={25} />
                                                <span>Jam Berangkat</span>
                                                <span className="font-semibold">{moment(data.result.depart_date).format('HH:mm')} WIB</span>
                                            </p>
                                            <p className="flex flex-col text-center items-center">
                                                <FaMoneyBillWave size={25} />
                                                <span>Total Harga</span>
                                                <span className="font-semibold">Rp. {data.result.total_price.toLocaleString("id-ID")}</span>
                                            </p>
                                            <p className="flex flex-col text-center items-center">
                                                <IoMdTimer size={25} />
                                                <span>
                                                    Kadaluarsa
                                                </span>
                                                <span className="font-semibold">{data.result.payment_status === 'pending' ? moment(data.result.expiration_time, 'YYYY-MM-DD HH:mm:ss').format('HH:mm') + ' WIB' : '-'} </span>
                                            </p>
                                        </div>
                                        {isNotFromAllowedSources && isNotTicketed ? (
                                            <Button variant="primary" disabled className="w-full mt-4">
                                                Mohon selesaikan pembayaran di{" "}
                                                <span className="font-semibold ml-1 uppercase">
                                                    {data?.result?.order_from}
                                                </span>
                                            </Button>
                                        ) : (
                                            <>
                                                {data?.result?.transaction_status === "ticketed" && (
                                                    isNotFromAllowedSources ?
                                                        <>
                                                            <Button variant="primary" disabled className="w-full mt-4">
                                                                ETicket tersedia di{" "}
                                                                <span className="font-semibold ml-1 uppercase">
                                                                    {data?.result?.order_from}
                                                                </span>
                                                            </Button>
                                                        </>
                                                        :
                                                        <>
                                                            <Link
                                                                href={`${process.env.NEXT_PUBLIC_API_URL}/v1/ticket/digital/${bookingCode}`}
                                                                target="_blank"
                                                                download={`tiket-${bookingCode}`}
                                                            >
                                                                <Button variant="primary" className="w-full mt-4">
                                                                    Cetak E-Ticket
                                                                </Button>
                                                            </Link>
                                                        </>
                                                )}
                                                {data?.result?.transaction_status === "book" && !isPaymentOvertime && (
                                                    <Link href={data?.result?.round_trip ? `/checkout/roundtrip/${data?.result?.round_trip?.transaction_round_trip_code}` : `/checkout/${bookingCode}`}>
                                                        <Button variant="primary" className="w-full mt-4">
                                                            Bayar
                                                        </Button>
                                                    </Link>
                                                )}
                                                {data?.result?.transaction_status === "book" && isPaymentOvertime && (
                                                    <Button variant="primary" disabled className="w-full mt-4">
                                                        Mohon selesaikan pembayaran di{" "}
                                                        <span className="font-semibold ml-1 uppercase">
                                                            {data?.result?.order_from}
                                                        </span>
                                                    </Button>
                                                )}
                                            </>
                                        )}
                                    </div>
                                    <div className="flex flex-col md:border-l-2 md:border-t-0 text-sm border-dashed border-secondary p-4 md:min-w-96">
                                        {roundTrip && (
                                            <div
                                                className="mb-2 cursor-pointer hover:text-primary duration-200 group bg-gray-100 p-3 rounded-lg"
                                                onClick={() => {
                                                    if (roundTrip) {
                                                        const nextCode = roundTrip.transaction_depart_code === bookingCode
                                                            ? roundTrip.transaction_return_code
                                                            : roundTrip.transaction_depart_code;
                                                        Router.push('/reservation?kode=' + String(nextCode));
                                                        setBookingCode('bookingCode', String(nextCode) || "");
                                                    }
                                                }}
                                            >
                                                <div className="flex flex-col sm:flex-row justify-between sm:items-center">
                                                    <div>
                                                        <p>Kode Booking {roundTrip.transaction_depart_code === bookingCode ? "Pulang" : "Berangkat"}</p>
                                                        <p className="font-semibold capitalize">
                                                            {roundTrip.transaction_depart_code === bookingCode
                                                                ? roundTrip.transaction_return_code
                                                                : roundTrip.transaction_depart_code}
                                                        </p>
                                                    </div>
                                                    <p className="p-1 text-sm bg-gray-300 sm:max-w-40 mt-2 bg-primary duration-200 text-white rounded-lg w-full text-center">Cek</p>
                                                </div>
                                            </div>
                                        )}
                                        <div>
                                            <p>Nama Pemesan</p>
                                            <p className="font-semibold capitalize">{data.result.orderer_name}</p>
                                        </div>
                                        <div className="mt-2">
                                            <p>Nomor Telepon</p>
                                            <p className="font-semibold">{data.result.orderer_phone}</p>
                                        </div>
                                        <div className="mt-2">
                                            <p>Nomor Kursi yang dipesan</p>
                                            <div className="flex flex-row font-semibold">
                                                {data.result.passengers.map((pax: any, key: number) => (
                                                    <div key={key}>
                                                        {key == 0 ? '' : ','} {pax.seat_number}
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                        {data.result.transaction_status === 'book' && data?.result?.payment_data && (
                                            <div className="mt-5">
                                                <h3 className="border-b-2">Metode Pembayaran</h3>
                                                {
                                                    data?.result?.payment_data?.vaNumber && (
                                                        <>
                                                            <span className="font-bold">{data.result.payment_method_data.name}</span>
                                                            <br />
                                                            <span className="font-bold">{data.result.payment_data.vaNumber}</span>
                                                            <br />
                                                            <Button
                                                                variant={"primary"}
                                                                title='Copy'
                                                                className="p-1 w-full h-9"
                                                                onClick={() => { copyText(data.result.payment_data.vaNumber) }}
                                                            >
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="16"
                                                                    height="16"
                                                                    fill="white"
                                                                    className="bi bi-files"
                                                                    viewBox="0 0 16 16"
                                                                >
                                                                    <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2m0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1M3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1z" />
                                                                </svg>
                                                            </Button>
                                                        </>
                                                    )
                                                }
                                                {
                                                    data?.result?.payment_data?.qrString && (
                                                        <>
                                                            Qr Code :
                                                            <br />
                                                            <Canvas
                                                                text={data.result.payment_data.qrString}
                                                                options={{
                                                                    errorCorrectionLevel: 'M',
                                                                    width: 150,
                                                                    color: {
                                                                        dark: '#030302',
                                                                        light: '#faf9f7',
                                                                    },
                                                                }}
                                                            />
                                                        </>
                                                    )
                                                }
                                                {
                                                    data?.result?.payment_data?.paymentUrl && (
                                                        <>
                                                            Link Pembayaran : <br />
                                                            <Link href={data.result.payment_data.paymentUrl}>
                                                                <Button variant={'primary'} title="Bayar" className="w-full font-bold">
                                                                    {data.result.payment_method_data.name}
                                                                </Button>
                                                            </Link>
                                                        </>
                                                    )
                                                }
                                            </div>
                                        )}

                                        {data.result.transaction_status === 'ticketed' && (
                                            <>
                                                {!isNotFromAllowedSources && (
                                                    <div className="mt-2">
                                                        <p>Metode Pembayaran</p>
                                                        <p className="font-semibold">{data.result.payment_method_data.name}</p>
                                                    </div>
                                                )}
                                                <div className="mt-2">
                                                    <p>Pemesanan Via</p>
                                                    <p className="font-semibold uppercase">{data.result.order_from}</p>
                                                </div>
                                            </>
                                        )}
                                        {/* nanti bakal di pake lagi */}
                                        {/* {(profileData && profileData.phone === data.result.orderer_phone) && (
                                            <div className="mt-5">
                                                <span>Ubah jadwal</span>
                                                <Button variant={'primary'} className="w-full" onClick={() => Router.push(`/schedule?depart=${data.result.route.depart_code}&arrival=${data.result.route.arrival_code}&date=${moment(data.result.depart_date,'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD')}&pax=${data.result.pax}&booking_code=${data.result.booking_id}`)}>Re-Schedule</Button>
                                            </div>
                                        )} */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="mt-10">
                            <DataNotFound />
                        </div>
                    )}
                </div>
            )}
        </div>
    )
}

export default DetailReservation;
