"use client";
import config from "@/project.config";
import useSWR from "swr";
import ErrorFetch from "../errorFetch";
import moment from 'moment';
import 'moment/locale/id';
import { FaMoneyBillWave } from "react-icons/fa";
import { IoMdPeople, IoMdTime, IoMdTimer } from "react-icons/io";
import DataNotFound from "../dataNotFound";
import { Button } from "@/components/ui/button";
import SkeletonCard from "../skeleton/skeletonCard";
import { timeDuration } from "@/lib/timeDuration";
import Link from "next/link";
import { toast } from "@/components/ui/use-toast";
import { useQRCode } from 'next-qrcode';



interface responseSchema {
    "payment_name": string,
    "transaction_code": string,
    "total_pax": number,
    "total_sub_price": number,
    "total_voucher_price": number,
    "total_point_used": number,
    "grand_total_price": number,
    "payment_status": string,
    "expired_at": string,
    "depart": roundtripSchema,
    "return": roundtripSchema,
    "order_from": string
}

interface roundtripSchema {
    "orderer_name": string,
    "orderer_email": string,
    "orderer_phone": string,
    "payment_status": string;
    "booking_code": string,
    "depart": string,
    "arrival": string,
    "depart_at": string,
    "etd": string,
    "eta": string,
    "transaction_status": string,
    "original_price": string,
    "total_price": string,
    "price_data": priceDataSchema,
    "payment_method_data": paymentMethodShcema,
    "point_earned": number,
    "pax": number,
    "order_from": string,
    "expired_at": string,
    "passenger": passengerSchema[]
}

interface passengerSchema {
    "passenger_name": string,
    "passenger_seat": number,
    "passenger_ticket": string
}

interface priceDataSchema {
    "unit_price": number,
    "pax": number,
    "price": number,
    "charg_bagasi": number,
    "discount": number,
    "discount_promo": number,
    "discount_voucher": number,
    "point_used": number,
    "total_deduction": number,
    "total_price": number,
}

interface paymentMethodShcema {
    "id": 11,
    "name": string,
    "code": string,
    "cost_type": string,
    "cost": number,
    "amount": number,
    "type": string,
    "payment_method": string,
    "provider": string,
    "type_fee": string,
    "type_device": string,
    "merchant": string,
    "status": string,
    "settleman": number,
    "image": string
}

export const DetailReservationRoundtrip = (bookingCode: any) => {
    const { setIsLoading } = bookingCode;
    const { Canvas } = useQRCode();
    const { data, error, isLoading } = useSWR<{ status: string; result: responseSchema }>(`${config.apiUrl}/v1/booking/${bookingCode.code}`)
    if (isLoading) return (
        <div className="pt-10">
            <SkeletonCard times={1} />
        </div>
    )
    if (error) return (
        <ErrorFetch />
    )
    const bookingForm = data?.result && data.result;

    setIsLoading(false);

    const diff_hours = (etd: string, eta: string) => {
        const dateTimePatern = 'YYYY-MM-DD HH:mm:ss'
        var d1 = moment(etd, dateTimePatern).format('YYYY-MM-DD');
        var d2 = moment(etd, dateTimePatern).format('YYYY-MM-DD');
        if (eta < etd) {
            d1 = moment(etd, dateTimePatern).add(-1, 'day').format('YYYY-MM-DD')
        }
        const startDate = moment(d1 + ' ' + etd, dateTimePatern).format(dateTimePatern)
        const endDate = moment(d2 + ' ' + eta, dateTimePatern).format(dateTimePatern)
        return timeDuration(startDate, endDate);
    }

    const copyText = (entryText: string) => {
        navigator.clipboard.writeText(entryText);
        toast({
            variant: "default",
            title: "Clipboard ditambahkan",
            description: "Nomor Virtual Account di tambahkan ke clipboard"
        });
    };

    const isNotFromAllowedSources =
        !["whatsapp", "web", "mobile", "outlet"].includes(bookingForm!.depart?.order_from || bookingForm!.return?.order_from);

    const isNotTicketed = (bookingForm?.depart?.transaction_status && bookingForm?.return?.transaction_status) !== "ticketed";

    const isPaymentOvertime = (depart_date: string, expired_time: string) => {
        return moment(expired_time, "YYYY-MM-DD HH:mm:ss").format("YYYYMMDDHHmmss") >=
            moment(depart_date, "YYYY-MM-DD HH:mm:ss").format("YYYYMMDDHHmmss");
    }

    const renderDetail = (data: roundtripSchema) => {
        return (
            <div className="flex w-full bg-white">
                <div className="flex md:flex-row flex-col w-full justify-between">
                    <div className="w-full">
                        <div className="flex flex-col md:flex-row justify-between text-center text-sm items-center border-b-2 md:border-b-0 border-dashed border-secondary space-y-4">
                            <div className=" w-full md:text-start">
                                <p>Kode Booking</p>
                                <span className="text-lg font-bold">{data?.booking_code}</span>
                            </div>
                            <div className="text-sm w-full">
                                <p>Tanggal Keberangkatan</p>
                                <span className="font-bold text-lg">{moment(data.depart_at + ' ' + data?.etd, 'YYYY-MM-DD HH:mm:ss').format('dddd, DD MMMM YYYY')}</span>
                            </div>
                            <div className="w-full md:text-end">
                                <p>Status Transaksi</p>
                                <span className={`
                                            font-semibold text-lg
                                            ${data?.transaction_status == 'ticketed' ? 'text-green-600' : 'text-red-500'}
                                        `}
                                >
                                    {data?.transaction_status?.toUpperCase()}
                                </span>
                            </div>
                        </div>
                        <div className="flex flex-col md:flex-row justify-between text-center text-sm items-center space-y-4">
                            <div className="md:text-left w-full">
                                Outlet Asal
                                <br />
                                <span className="font-bold text-lg">{data?.depart}</span>
                            </div>
                            <div className="text-center w-full">
                                <span>Lama Perjalanan</span><br />
                                <span className="font-bold text-lg">
                                    {diff_hours(data.etd, data.eta)}
                                </span>
                            </div>
                            <div className="md:text-right w-full">
                                Outlet Tujuan
                                <br />
                                <span className="font-bold text-lg">{data?.arrival}</span>
                            </div>
                        </div>
                        <div className="md:flex md:flex-row grid grid-cols-2 gap-3 justify-between text-center text-xs md:text-sm items-center mt-4 pb-4 md:border-b-0 border-b-2 border-dashed border-secondary">
                            <p className="flex flex-col text-center items-center">
                                <IoMdPeople size={25} />
                                <span>Penumpang</span>
                                <span className="font-semibold">{data?.pax} orang</span>
                            </p>
                            <p className="flex flex-col text-center items-center">
                                <IoMdTime size={25} />
                                <span>Jam Berangkat</span>
                                <span className="font-semibold">{moment(data?.etd, 'HH:mm').format('HH:mm')} WIB</span>
                            </p>
                            <p className="flex flex-col text-center items-center">
                                <FaMoneyBillWave size={25} />
                                <span>Total Harga</span>
                                <span className="font-semibold">Rp. {parseInt(data?.total_price)?.toLocaleString("id-ID")}</span>
                            </p>
                            <p className="flex flex-col text-center items-center">
                                <IoMdTimer size={25} />
                                <span>
                                    Kadaluarsa
                                </span>
                                <span className="font-semibold">{data?.transaction_status === 'book' ? moment(data?.expired_at, 'YYYY-MM-DD HH:mm:ss').format('HH:mm') + ' WIB' : '-'} </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <>
            {(data!.status == 'SUCCESS') ? (
                <>
                    <div className="flex flex-col lg:flex-row mt-10 rounded-lg p-3 bg-primary">
                        <div className="p-3 bg-red-200 w-full lg:border-r-2 border-dashed border-primary">
                            <div className="flex flex-col">
                                {renderDetail(bookingForm?.depart!)}
                                {renderDetail(bookingForm?.return!)}
                            </div>
                            {isNotFromAllowedSources && isNotTicketed ? (
                                <Button variant="primary" disabled className="w-full mt-4">
                                    Mohon selesaikan pembayaran di{" "}
                                    <span className="font-semibold ml-1 uppercase">
                                        {bookingForm?.depart?.order_from}
                                    </span>
                                </Button>
                            ) : (
                                <>
                                    {bookingForm?.payment_status === "paid" && (
                                        isNotFromAllowedSources ?
                                            <>
                                                <Button variant="primary" disabled className="w-full mt-4">
                                                    ETicket tersedia di{" "}
                                                    <span className="font-semibold ml-1 uppercase">
                                                        {bookingForm?.order_from}
                                                    </span>
                                                </Button>
                                            </>
                                            :
                                            <>
                                                <Link
                                                    href={`${process.env.NEXT_PUBLIC_API_URL}/v1/ticket/digital/${bookingCode.code}/roundtrip`}
                                                    target="_blank"
                                                    download={`tiket-${bookingCode.code}`}
                                                >
                                                    <Button variant="primary" className="w-full mt-4">
                                                        Cetak E-Ticket
                                                    </Button>
                                                </Link>
                                            </>
                                    )}
                                    {bookingForm?.payment_status === "pending" && !isPaymentOvertime((bookingForm?.depart?.depart_at + bookingForm?.depart?.etd), bookingForm?.depart?.expired_at) && !isPaymentOvertime((bookingForm?.return?.depart_at + bookingForm?.return?.etd), bookingForm?.return?.expired_at) && (
                                        <Link href={`/checkout/roundtrip/${bookingCode.code}`}>
                                            <Button variant="primary" className="w-full mt-4">
                                                Bayar
                                            </Button>
                                        </Link>
                                    )}
                                    {bookingForm?.payment_status === "pending" && isPaymentOvertime((bookingForm?.depart?.depart_at + bookingForm?.depart?.etd), bookingForm?.depart?.expired_at) && isPaymentOvertime((bookingForm?.return?.depart_at + bookingForm?.return?.etd), bookingForm?.return?.expired_at) && (
                                        <Button variant="primary" disabled className="w-full mt-4">
                                            Mohon selesaikan pembayaran di{" "}
                                            <span className="font-semibold ml-1 uppercase">
                                                {bookingForm?.order_from}
                                            </span>
                                        </Button>
                                    )}
                                </>
                            )}
                        </div>
                        <div className="p-3 bg-blue-200 w-full lg:max-w-md">
                            <div>
                                <p>Nama Pemesan</p>
                                <p className="font-semibold capitalize">{bookingForm?.depart?.orderer_name || bookingForm?.return?.orderer_name}</p>
                            </div>
                            <div className="mt-2">
                                <p>Nomor Telepon</p>
                                <p className="font-semibold">{bookingForm?.depart?.orderer_phone || bookingForm?.return?.orderer_phone}</p>
                            </div>
                            {/* {data?.payment_status === 'book' && data?.payment_data && (
                                <div className="mt-5">
                                    <h3 className="border-b-2">Metode Pembayaran</h3>
                                    {
                                        data?.payment_data?.vaNumber && (
                                            <>
                                                <span className="font-bold">{data?.payment_method_data.name}</span>
                                                <br />
                                                <span className="font-bold">{data?.payment_data.vaNumber}</span>
                                                <br />
                                                <Button
                                                    variant={"primary"}
                                                    title='Copy'
                                                    className="p-1 w-full h-9"
                                                    onClick={() => { copyText(data?.payment_data.vaNumber) }}
                                                >
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="16"
                                                        height="16"
                                                        fill="white"
                                                        className="bi bi-files"
                                                        viewBox="0 0 16 16"
                                                    >
                                                        <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2m0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1M3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1z" />
                                                    </svg>
                                                </Button>
                                            </>
                                        )
                                    }
                                    {
                                        data?.payment_data?.qrString && (
                                            <>
                                                Qr Code :
                                                <br />
                                                <Canvas
                                                    text={data?.payment_data.qrString}
                                                    options={{
                                                        errorCorrectionLevel: 'M',
                                                        // margin: 3,
                                                        // scale: 4,
                                                        width: 150,
                                                        color: {
                                                            dark: '#030302',
                                                            light: '#faf9f7',
                                                        },
                                                    }}
                                                />
                                            </>
                                        )
                                    }
                                    {
                                        data?.payment_data?.paymentUrl && (
                                            <>
                                                Link Pembayaran : <br />
                                                <Link href={data?.payment_data.paymentUrl}>
                                                    <Button variant={'primary'} title="Bayar" className="w-full font-bold">
                                                        {data?.payment_method_data.name}
                                                    </Button>
                                                </Link>
                                            </>
                                        )
                                    }
                                </div>
                            )} */}

                            {bookingForm?.payment_status === 'paid' && (
                                <>
                                    {!isNotFromAllowedSources && (
                                        <div className="mt-2">
                                            <p>Metode Pembayaran</p>
                                            <p className="font-semibold">{bookingForm?.payment_name}</p>
                                        </div>
                                    )}
                                    <div className="mt-2">
                                        <p>Pemesanan Via</p>
                                        <p className="font-semibold uppercase">{bookingForm?.order_from}</p>
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                </>
            ) : (
                <div className="mt-10">
                    <DataNotFound />
                </div>
            )}
        </>
    )
}