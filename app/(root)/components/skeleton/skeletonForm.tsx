export default function SkeletonForm() {
  return (
    <>
      <span className="w-max h-max bg-gray-200 animate-pulse duration-500">&nbsp;</span>
    </>
  )
}