import { Skeleton } from "@/components/ui/skeleton"

const SkeletonContent = () => {
    return (
        <div className="container pt-4 space-y-4">
            <div className="flex flex-col items-center">
                <Skeleton className="h-[30px] w-[320px] rounded-xl" />
            </div>
            <Skeleton className="h-[30px] w-[320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
            <Skeleton className="h-[30px] w-[1320px] rounded-xl" />
        </div>
    )
}

export default SkeletonContent