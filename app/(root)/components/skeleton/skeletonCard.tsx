import { Skeleton } from "@/components/ui/skeleton"
import { FC } from "react"

interface SkeletonCardProps {
    times: number
    type?: string
    height?: string
    width?: string
}

const SkeletonCard: FC<SkeletonCardProps> = ({
    times,
    type = 'cols',
    height = 'h-[200px]',
    width = 'w-[320px]'
}) => {
    return (
        <div className='py-4'>
            <div className={`grid md:grid-${type}-${times} gap-2 items-center justify-center`}>
                {Array.from({ length: times }).map((_, index) => (
                    <Skeleton
                        key={index}
                        className={`${height} ${width} rounded-xl`}
                    />
                )
                )}
            </div>
        </div>
    )
}

export default SkeletonCard