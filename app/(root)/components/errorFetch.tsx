import Image from "next/image"
import ErrorSvg from "@/components/image/errorSvg"
const ErrorFetch = () => {
    return (
        <div className='flex flex-col items-center pt-14'>
            {/* <Image src={'/error.svg'} alt={'error'} width={250} height={250} /> */}
            <div className="h-96 w-96 flex items-center justify-center">
                <ErrorSvg />
            </div>
            <span className='md:text-lg text-md text-main text-center font-semibold py-2'>Ooooops! sepertinya sedang terjadi masalah pada sistem kita.</span>
            <span className='font-light text-slate-400 text-sm'>Silakan coba refresh kembali halamannya ya.</span>
        </div>
    )
}

export default ErrorFetch