import Image from "next/image"
import Menu from "./menu"
import Link from "next/link"

export const Header = () => {
    return (
        <header className="sticky top-0 h-18 w-full bg-primary py-3 z-40 shadow-lg">
            <nav className="container flex items-center justify-between mx-auto text-primary-foreground">
                <Link href={"/"}>
                    <Image src={"/images/logo/logo-white.png"} alt={""} width={150} height={100} />
                </Link>
                <Menu />
            </nav>
        </header>
    )
}