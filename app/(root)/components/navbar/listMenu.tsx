import { FaBook, FaHome, FaNewspaper, FaScroll, FaSearch, FaStore } from "react-icons/fa";
import { LuPackageCheck } from "react-icons/lu";

const ListMenu = [
    { id: 1, name: 'Beranda', path: '/', icon: <FaHome /> },
    { id: 2, name: 'Outlet', path: '/outlet', icon: <FaStore /> },
    { id: 3, name: 'Paket', path: '/package', icon: <LuPackageCheck /> },
    { id: 4, name: 'Berita', path: '/news', icon: <FaNewspaper /> },
    // { id: 7, name: 'Karir', path: '/career', icon: <FaScroll /> },
    { id: 5, name: 'Cek Reservasi', path: '/reservation', icon: <FaSearch /> },
    { id: 6, name: 'Tentang Kami', path: '/about', icon: <FaBook /> }
]

export default ListMenu;