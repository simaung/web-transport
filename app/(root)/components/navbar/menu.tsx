"use client"

import Link from "next/link"
import ListMenu from "./listMenu"
import { usePathname } from "next/navigation"
import MobileMenu from "./mobileMenu"

const Menu = () => {
    const pathname = usePathname()
    const isActive = (path: string) => path === pathname

    return (
        <>
            <div className="hidden lg:block space-x-6">
                {ListMenu.map((list) => (
                    <Link
                        key={list.id}
                        className={`
                        text-sm hover:bg-white hover:text-primary rounded-full p-3 duration-300
                        ${isActive(list.path) ? "bg-white text-primary" : 'text-white'}
                        `}
                        href={list.path}
                    >
                        {list.name}
                    </Link>
                ))}
            </div>
            <div className="pt-1.5">
                <MobileMenu />
            </div>
        </>
    )
}

export default Menu