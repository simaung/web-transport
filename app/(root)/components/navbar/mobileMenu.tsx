"use client"
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import Link from "next/link"
import ListMenu from "./listMenu"
import { FiAlignJustify } from "react-icons/fi"
import { BiSolidLogInCircle } from "react-icons/bi"
import cookie from "@/lib/cookies"
import { FaUser } from "react-icons/fa"

const MobileMenu = () => {
    return (
        <div>
            <DropdownMenu>
                <DropdownMenuTrigger>
                    <FiAlignJustify size={27} className="text-white outline-none" />
                </DropdownMenuTrigger>
                <DropdownMenuContent className="flex flex-col w-full">
                    {ListMenu.map((link) => (
                        <Link key={link.id} href={link.path} className="lg:hidden">
                            <DropdownMenuItem className="flex flex-row gap-3 text-sm">{link.icon}{link.name}</DropdownMenuItem>
                        </Link>
                    ))}
                    <DropdownMenuSeparator className="lg:hidden" />
                    {cookie.getToken() ?
                        <DropdownMenuItem className="">
                            <Link href={'/account/profile'} className="flex flex-row gap-3 text-sm lg:text-base items-center">
                                <FaUser />Profil
                            </Link>
                        </DropdownMenuItem>
                        :
                        <DropdownMenuItem className="">
                            <Link href={'/auth/login'} className="flex flex-row gap-3 text-sm lg:text-base items-center">
                                <BiSolidLogInCircle />Sign In
                            </Link>
                        </DropdownMenuItem>
                    }
                </DropdownMenuContent>
            </DropdownMenu>
        </div>
    )
}

export default MobileMenu