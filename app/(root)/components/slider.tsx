/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
"use client"
import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious } from "@/components/ui/carousel"
import Autoplay from "embla-carousel-autoplay"
import config from "@/project.config"
import useSWR from "swr"
import SkeletonCard from "./skeleton/skeletonCard"
import ErrorFetch from "./errorFetch"
import { Link } from "lucide-react"

const Slider = () => {
    const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/sliders`)

    if (error) return (
        <ErrorFetch />
    )

    if (isLoading) return (
        <SkeletonCard times={4} />
    )

    return (
        <>
            <Carousel
                opts={{
                    align: "start",
                    loop: true,
                }}
                plugins={[
                    Autoplay({
                        delay: 5000,
                    }),
                ]}
                className='lg:container'
            >
                <CarouselContent className="z-10 ">
                    {data.sliders.map((slide: any) => {
                        return (
                            <CarouselItem className="h-full flex items-center justify-center" key={slide.id}>
                                <div className="w-full">
                                    {slide?.id_promo ?
                                        <a href={"/voucher/beli/" + slide?.id_promo} target="_blank" className="">
                                            <div className="relative group">
                                                <img src={slide?.image} alt={slide?.name} className="w-full h-auto object-cover self-center" />
                                                <span className="absolute bg-primary py-1 text-white bottom-0 left-0 w-full text-center opacity-50 group-hover:opacity-100 duration-300 text-xs">Klik untuk melihat detail</span>
                                            </div>
                                        </a>
                                        :
                                        <img src={slide?.image} alt={slide?.name} className="w-full h-auto object-cover self-center" />
                                    }
                                    <div className="bg-primary w-full text-center text-xs text-white italic">{slide?.description}</div>
                                </div>
                            </CarouselItem>
                        )
                    })}
                </CarouselContent>
                <CarouselPrevious className='left-10 opacity-20 hover:opacity-80' />
                <CarouselNext className='right-10 opacity-20 hover:opacity-80' />
            </Carousel>
        </>
    )
}

export default Slider