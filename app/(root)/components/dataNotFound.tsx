import DataNotFoundImage from "@/components/image/DataNotFoundImage"
import Image from "next/image"
import { FC } from "react"

interface DataNotFoundProps {
    title?: string,
    desc?: string
}
const DataNotFound: FC<DataNotFoundProps> = ({
    title = "Aduuh maaf, data yang kamu cari tidak bisa kami temukan.",
    desc = "Silakan masukan kembali data yang sesuai ya."
}) => {
    return (
        <div className='flex flex-col items-center'>
            {/* <Image src={'/notFound.svg'} alt={'error'} width={250} height={250} /> */}
            <div className="w-auto h-80">
                <DataNotFoundImage />
            </div>
            <span className='md:text-lg text-md text-main text-center font-semibold py-2'>{title}</span>
            <span className='font-light text-slate-400 text-sm text-center'>{desc}</span>
        </div>
    )
}

export default DataNotFound