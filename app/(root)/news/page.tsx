/* eslint-disable @next/next/no-img-element */
"use client"
import useSWR from "swr";
import MainBanner from "../components/feature/mainBanner";
import DataNotFound from "../components/dataNotFound";
import { Skeleton } from "@/components/ui/skeleton";
import Link from "next/link";
import config from "@/project.config";

export default function News() {
  const { data } = useSWR(config.apiUrl + '/v1/blogs?page=1&per_page=1')
  return (
    <>
      <MainBanner label="Berita Terbaru Arnes" />
      <div className="container">
        <div className="bg-white w-full rounded-lg p-5 shadow-md">
          {/* {JSON.stringify(data)} */}
          {data ?
            data.data.length !== 0 ?
              <>
                <div className="flex flex-row justify-between gap-3" key={'berita-container'}>
                  {data.data.data.map((e: any, i: number) => {
                    return (
                      <>
                        <Link href={'/news/' + e.slug} key={'beritaa' + i}>
                          <div className="bg-white border-[1px] p-3 cursor-pointer hover:shadow-md duration-300" key={'berita' + i}>
                            <img src={e.image} alt="..." className="w-auto h-40 object-cover" />
                            <div className="font-bold">
                              {e.title}
                            </div>
                          </div>
                        </Link>
                      </>
                    )
                  })}
                </div>
              </>
              :
              <>
                <DataNotFound title="Tidak ada berita terbaru" desc="Terimakasih sudah berlangganan berita/informasi terbaru dari arnes" />
              </>
            :
            <>
              <Skeleton className="h-64"></Skeleton>
            </>
          }
        </div>
      </div>
    </>
  )
}