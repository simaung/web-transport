/* eslint-disable @next/next/no-img-element */
"use client"

import useSWR from "swr";
import { useRouter } from "next/navigation";
import config from "@/project.config";

export default function NewsDetail({ params }: any) {
  const Router = useRouter();
  const { id } = params;
  const { data } = useSWR(config.apiUrl + '/v1/blogs/' + id)

  // if (!data) {
  //   Router.push('/news')
  // }
  return (
    <>
      <div className="container mt-[3%]">
        {/* {JSON.stringify(data)} */}
        {data && (
          <>
            <div className="flex flex-col gap-3">
              <h3 className="font-bold text-xl text-center">
                {data.data.title}
              </h3>
              <img src={data.data.image} alt="..." />
              <div className="default-header p-5 bg-white shadow-lg rounded-lg" dangerouslySetInnerHTML={{ __html: data.data.content }}></div>
            </div>
          </>
        )}
      </div>
    </>
  )
}