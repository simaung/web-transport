/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/exhaustive-deps */
"use client"
import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";
import { groupPayment } from "@/lib/grouping";
import { GET, POST } from "@/lib/http";
import config from "@/project.config";
import moment from "moment";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { FaEnvelope, FaMailBulk, FaStopwatch, FaUser } from "react-icons/fa";
import useSWR from "swr";
require('moment/locale/id');

interface selectedPaymentSchema {
  booking_code: string,
  payment_method: string,
  payment_code: string
}


interface detailVoucherSchema {
  "transaction_code": string,
  "transaction_status": string,
  "payment_status": string,
  "orderer_name": string,
  "orderer_phone": string,
  "orderer_email": string,
  "pax": 1,
  "order_from": string,
  "expired_at": string,
  "total_price": number,
  "transaction_date": string,
  "voucher": {
    "id": string,
    "promotion_code": string,
    "name": string,
    "price": number,
    "image": null,
    "type": string,
    "start_periode": string,
    "end_periode": string
  }
}

export default function Page({ params }: any) {
  const { id } = params;
  const method = JSON.stringify({
    VA: "Virtual Account",
    QR: "Qr Code",
    "E-Wallet": 'Dompet Online'
  })
  const [selectedPayment, setSelectedPayment] = useState<selectedPaymentSchema>({
    booking_code: id,
    payment_method: '',
    payment_code: ''
  })
  const [loadingBayar, setLoadingBayar] = useState(false);
  const router = useRouter();
  const [detailVoucher, setDetailVoucher] = useState<detailVoucherSchema>()
  const { data: payment, isLoading: paymentLoading } = useSWR(`${config.apiUrl}/v1/payment/${id}/voucher`)
  const paymentGroup = payment?.payments ? (groupPayment(payment?.payments, "type")) : [];


  async function getDetail() {
    const { data: detailVoucher } = await GET('/v1/vouchers/check/' + id, {})
    if (detailVoucher.status === 'SUCCESS') {
      setDetailVoucher(detailVoucher.voucher)
    } else {
      toast({
        variant: 'destructive',
        title: 'Gagal mendapatkan detail voucher',
        description: detailVoucher.message
      })
    }
  }
  async function CreatePayment() {
    setLoadingBayar(true)
    const { data: res } = await POST('/v1/payment/create/voucher', selectedPayment);
    if (res.status !== 'SUCCESS') {
      toast({
        variant: "destructive",
        title: res?.status,
        description: res?.message,
      });
      setLoadingBayar(false);
      router.push('/voucher?kode=' + id)
      return
    }
    toast({
      variant: "success",
      title: "Berhasil",
      description: "Silahkan ikuti langkah pembayaran berikut"
    });

    const { paymentUrl, qrString, vaNumber } = res?.payments;
    const { booking_code, payment_code } = selectedPayment;

    if (paymentUrl !== '') {
      window.open(paymentUrl, '_blank');
      return
    }

    if (qrString !== '') {
      router.push(`/checkout/bayar?payment_code=${payment_code}&&qrString=${qrString}&&booking_code=${booking_code}&&type=voucher`)
      return
    }

    if (vaNumber !== '') {
      router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${vaNumber}&&booking_code=${booking_code}&&type=voucher`)
      return
    }
  }
  useEffect(() => {
    getDetail()
  }, []);
  return (
    <>
      <div className="container mt-[3%]">
        <div className="flex lg:flex-row flex-col gap-3 w-full">

          <div className="bg-white p-5 border-2 w-full lg:max-w-lg h-fit">
            <div className="text-center font-bold mb-3 border-y-2">
              {detailVoucher?.transaction_code}
            </div>
            <div className="flex lg:flex-row flex-col justify-between mt-t">
              <div className="font-bold lg:text-base text-sm">
                {detailVoucher?.voucher.name}
              </div>
              <div className="font-bold text-primary lg:text-base text-sm">
                Rp. {detailVoucher?.total_price.toLocaleString('id-ID')}
              </div>
            </div>
            <div className="flex flex-col gap-3 mt-5 border-b-2 pb-3 lg:text-base text-sm">
              <h5 className="border-b-2 pb-3 flex flex-row gap-1 items-center">
                <FaUser /> {detailVoucher?.orderer_name}
              </h5>
              <h5 className="border-b-2 pb-3 flex flex-row gap-1 items-center">
                <FaEnvelope /> {detailVoucher?.orderer_email}
              </h5>
              <h5 className="flex flex-row gap-1 items-center">
                <FaStopwatch /> {detailVoucher?.expired_at ? <>{moment(detailVoucher?.expired_at, 'YYYY-MM-DD HH:mm:ss').locale('id').format('dddd, DD MMM YY HH:mm')} WIB</> : <>-</>}
              </h5>
              <small className="text-xs text-red-500 italic -mt-4">Batas Pembayaran</small>
            </div>
            <small className="text-xs italic">Setelah melakukan pembayaran, tiket akan di kirimkan lewat email yang anda masukan.</small>
          </div>

          <div className="border-2 p-5 bg-white w-full">
            <div className="text-lg font-bold">
              Metode Pembayaran
            </div>
            <div className="h-96 overflow-y-auto my-3">
              {paymentLoading ?
                (
                  <>
                    Loading
                  </>
                )
                :
                <>
                  {payment?.payments?.length === 0 ?
                    <>
                      Payment tidak di temukan
                    </>
                    :
                    <>
                      <div className="flex flex-col gap-7 ">
                        {Object?.keys(paymentGroup)?.map(u => (
                          <>
                            <div className="flex flex-col gap-3" key={u + "paymentgroup"}>
                              <span className="font-bold w-full border-b-2 pb-2">{JSON.parse(method)[u]}</span>
                              {paymentGroup[u].map((s: { name: string, code: string, image: string, type: string }) => (
                                <>
                                  <div
                                    className={`w-full bg-white p-3 rounded-lg border-2 hover:border-primary duration-300 cursor-pointer flex flex-row justify-between ${s.code === selectedPayment.payment_code ? "border-primary" : ""}`}
                                    key={s.name + "paymentgroup2"}
                                    onClick={() => {
                                      setSelectedPayment({ ...selectedPayment, payment_code: s.code, payment_method: s.type })
                                    }}
                                  >
                                    {s.name}
                                    <img src={s.image} alt="..." className="h-8" />
                                  </div>
                                </>
                              ))}
                            </div>
                          </>
                        ))}
                      </div>
                    </>
                  }
                </>
              }
            </div>
            <Button type="button" variant="primary" className="w-full" onClick={CreatePayment} disabled={loadingBayar}>{loadingBayar ? <>Loading</> : <>Lanjutkan</>}</Button>
          </div>
        </div>
      </div>
    </>
  )
}