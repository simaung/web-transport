/* eslint-disable @next/next/no-img-element */
"use client"
import { Input } from "@/components/ui/input";
import MainBanner from "../components/feature/mainBanner";
import { Button } from "@/components/ui/button";
import { useEffect, useState } from "react";
import { useSearchParams } from "next/navigation";
import { GET } from "@/lib/http";
import { toast } from "@/components/ui/use-toast";
import Link from "next/link";
import config from "@/project.config";
import { FaPrint } from "react-icons/fa";

interface detailVoucherSchema {
  "transaction_code": string,
  "transaction_status": string,
  "payment_status": string,
  "orderer_name": string,
  "orderer_phone": string,
  "orderer_email": string,
  "pax": 1,
  "order_from": string,
  "expired_at": string,
  "total_price": number,
  "transaction_date": string,
  "voucher": {
    "id": string,
    "promotion_code": string,
    "name": string,
    "price": number,
    "image": null,
    "type": string,
    "start_periode": string,
    "end_periode": string
  }
}

export default function Page() {
  const searchParams = useSearchParams();
  const kode = searchParams.get('kode');
  const [id, setId] = useState(kode || '');
  const [dataVoucher, setDataVoucher] = useState<detailVoucherSchema>()
  async function checkVoucher() {
    const { data: responseVoucher } = await GET('/v1/vouchers/check/' + id, {})
    if (responseVoucher.status === "SUCCESS") {
      setDataVoucher(responseVoucher.voucher)
    } else {
      toast(
        {
          variant: 'destructive',
          title: "Kesalahan",
          description: responseVoucher.voucher
        }
      )
    }
  }
  useEffect(() => {
    if (id) {
      checkVoucher();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <MainBanner />
      <div className="container">
        <div className="bg-white rounded-lg p-5 shadow-md flex lg:flex-row flex-col gap-3 w-full max-w-xl mx-auto">
          <Input type="text" className="w-full uppercase" placeholder="VARNXXXXXXXXX" value={id} onChange={(e: any) => setId(e.target.value)} />
          <Button type="button" variant={'primary'} onClick={checkVoucher}>Cek</Button>
        </div>
        <div className="bg-white rounded-lg p-5 shadow-md w-full max-w-xl mx-auto mt-[3%]">
          <div className="flex flex-col gap-3 border-2 p-3">
            <img src={dataVoucher?.voucher?.image || "/noimage.png"} alt="" />
            <span className="font-bold">{dataVoucher?.transaction_code}</span>
            <span className="">{dataVoucher?.voucher.name}</span>
            <span className="">Rp. {dataVoucher?.total_price.toLocaleString('id-ID')}</span>
            <span className={`uppercase font-bold text-white ${dataVoucher?.transaction_status === 'cancel' ? 'bg-red-500' : dataVoucher?.transaction_status === 'book' ? "bg-yellow-500" : "bg-green-500"} text-center py-2 px-3 text-xs rounded-lg w-full`}>{dataVoucher?.transaction_status}</span>
            <span className="font-bold">Data Pemesan</span>
            <span>{dataVoucher?.orderer_name}</span>
            <span>{dataVoucher?.orderer_email}</span>
            <small className="text-xs italic">note : Setelah melakukan pembayaran, <span className="text-red-500">voucher akan di kirimkan melalui email</span>. pastikan email anda aktif</small>
          </div>
          {
            dataVoucher?.transaction_status === 'book' && (
              <Link href={'/voucher/bayar/' + id}>
                <Button className="w-full mt-3" variant={"primary"}>Bayar</Button>
              </Link>
            )
          }
          {
            dataVoucher?.transaction_status === 'ticketed' && (
              <Link href={config.apiUrl + `/v1/vouchers/${dataVoucher.transaction_code}/pdf`} target="_blank">
                <Button className="w-full mt-3" variant={"primary"}><FaPrint className="mr-3" /> Cetak Voucher</Button>
              </Link>
            )
          }
        </div>

      </div>
    </>
  )
}