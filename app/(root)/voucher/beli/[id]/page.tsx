/* eslint-disable @next/next/no-img-element */
"use client"
import ModalFormVoucher from "@/app/(root)/components/feature/voucher/modalFormVoucher";
import { Skeleton } from "@/components/ui/skeleton";
import { toast } from "@/components/ui/use-toast";
import cookie from "@/lib/cookies";
import { GET, POST } from "@/lib/http";
import config from "@/project.config";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import useSWR from "swr";

interface responseProfileSchema {
  "name": string,
  "email": string,
  "phone": string,
  "point": number,
  "type": string,
  "avatar": string
}
export default function Page({ params }: any) {
  const router = useRouter()
  const { id } = params;
  const { data: detailVoucher, isLoading: detailLoading } = useSWR(config.apiUrl + '/v1/vouchers/' + id)
  const [profile, setProfile] = useState<responseProfileSchema>();
  const [openModal, setOpenModal] = useState(false)
  const [voucherForm, setVoucherForm] = useState({
    "voucher_id": id,
    "email": "",
    "name": "",
    "phone": ""
  })
  async function getProfile() {
    const { data } = await GET("/v1/account/profile", {})
    if (data.status === "SUCCESS") {
      setProfile(data.data)
      setVoucherForm({ ...voucherForm, email: data.data.email, name: data.data.name, phone: data.data.phone });
    }
  }

  async function handleBeli() {
    const { data: buyVoucher } = await POST('/v1/vouchers/buy', voucherForm)
    if (buyVoucher.status === "SUCCESS") {
      toast({
        variant: 'success',
        title: "Berhasil",
        description: "Silahkan lanjutkan pembayaran"
      })
      router.push('/voucher/bayar/' + buyVoucher.voucher.transaction_code)
    } else {
      toast({
        variant: 'warning',
        title: "Pembelian Voucher Gagal",
        description: buyVoucher.message
      })
    }
  }

  useEffect(() => {
    if (cookie?.getToken()) {
      getProfile()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <ModalFormVoucher isOpen={openModal} setIsOpen={setOpenModal} onSubmit={handleBeli} value={voucherForm} setValue={setVoucherForm} />
      <div className="container mx-auto h-auto p-5">
        {!detailLoading ? (
          <>
            <img src={detailVoucher?.voucher.image || "/noimage.png"} alt="" className="w-full" />
            <div className="w-full bg-white shadow-lg p-5 mt-5 rounded-lg flex flex-col gap-3">
              <div className="flex items-baseline border-b-2 pb-2 justify-between flex-wrap">
                <h4 className="font-bold lg:text-xl text-sm">
                  {detailVoucher?.voucher.name}
                </h4>
                <span className="font-bold text-orange-500 lg:text-base text-xs">
                  Rp. {detailVoucher?.voucher.price.toLocaleString("id-ID")}
                </span>
              </div>
              <p className="default-heading" dangerouslySetInnerHTML={{ __html: detailVoucher?.voucher?.description || "-" }}></p>
              {voucherForm?.name && voucherForm?.email ?
                <button className="w-full py-2 text-white bg-orange-500 rounded-lg hover:bg-orange-700 duration-200" onClick={handleBeli}>Beli Sekarang</button>
                :
                <button className="w-full py-2 text-white bg-orange-500 rounded-lg hover:bg-orange-700 duration-200" onClick={() => setOpenModal(true)}>Lengkapi Data</button>
              }
            </div>
          </>
        )
          :
          (
            <>
              <div className="flex items-center justify-center mt-[3%]">
                <Skeleton className="w-full rounded-lg h-96" />
              </div>
            </>
          )
        }
      </div>
    </>
  )
}