"use client"

import useDetailOutletModal from '@/hooks/useDetailOutletModal';
import SelectInput from '@/components/selectInput';
import { Button } from '@/components/ui/button';
import { Card, CardDescription, CardFooter, CardHeader, CardTitle } from '@/components/ui/card';
import { Skeleton } from '@/components/ui/skeleton';
import { groupingOutlet } from '@/lib/grouping';
import config from '@/project.config';
import { Outlet as OutletType } from '@/types/outlet';
import useSWR from 'swr';
import ErrorFetch from '../components/errorFetch';
import SkeletonCard from '../components/skeleton/skeletonCard';

function Outlet() {
    const detailOutletModal = useDetailOutletModal()

    const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/outlets`)

    if (data) {
        var outlets = groupingOutlet(data.points)
    }

    if (error) return (
        <ErrorFetch />
    )

    if (isLoading) return (
        <SkeletonCard times={4} />
    )

    return (
        <>
            <div className='fixed w-full border-slate-200 bg-white py-4'>
                <div className='text-center font-bold text-lg uppercase pt-2'>
                    LOKASI OUTLET {config.appName}
                </div>
                <div className='flex flex-col justify-center items-center mt-4'>
                    <SelectInput title={'Pilih Kota'} data={outlets} />
                </div>
            </div>
            <div className='container flex flex-col'>

                {data && (
                    <div className="flex flex-col mt-24 items-center">
                        {outlets.map((outlet: any) => (
                            <div key={outlet.city} id={outlet.city} className='scroll-mt-44'>
                                <div className='font-bold text-primary'>
                                    <hr className='border-1 border-primary my-4' />
                                    {outlet.city}
                                </div>
                                <div className='grid md:grid-cols-4 grid-cols-1 gap-2'>
                                    {
                                        outlet.sortData.map((point: OutletType) =>
                                        (
                                            <Card key={point.id} className="w-[320px] mt-2">
                                                <CardHeader className='h-[130px]'>
                                                    <CardTitle className='text-lg'>{point.name}</CardTitle>
                                                    <CardDescription className='text-xs capitalize'>{point.address.toLowerCase()}</CardDescription>
                                                </CardHeader>
                                                <CardFooter className="flex justify-between">
                                                    <Button variant="primary" className='w-full' onClick={() => { detailOutletModal.onOpen(point) }}>Detail</Button>
                                                </CardFooter>
                                            </Card>
                                        )
                                        )}
                                </div >
                            </div>
                        )
                        )}
                    </div>
                )
                }
            </div >
        </>
    );
}

export default Outlet;
