/* eslint-disable @next/next/no-img-element */
// import FormSample from "./components/feature/formSample";
import ReservationForm from "./components/feature/reservationForm";
import Slider from "./components/slider";
import RiwayatPesanan from "./components/feature/riwayatPesanan";
import MainBanner from "./components/feature/mainBanner";
import OurAdvantages from "./components/feature/ourAdvantages";
import AppDownload from "./components/feature/appDownload";
import AccountDetail from "./components/feature/account/accountDetail";

export default function Home() {

  return (
    <main>
      <AccountDetail />
      <MainBanner />
      <div className="flex flex-col justify-between gap-5 lg:gap-11">
        <ReservationForm />
        {/* <div className="container w-full flex items-center justify-center">
          <AppDownload />
        </div> */}
        <RiwayatPesanan />
        <Slider />
        <OurAdvantages />
        <div className="container">
          <div className="font-bold text-lg lg:text-3xl text-center w-full mb-3 lg:mb-16 uppercase">
            Postingan Terakhir Kami
          </div>
          <iframe src="https://www.instagram.com/arnesshuttle/embed" frameBorder="0" className="w-full h-[430px] md:h-[1100px]"></iframe>
        </div>
      </div>
    </main>
  );
}
