"use client"
import useSWR from "swr";
import MainBanner from "../../components/feature/mainBanner";
import config from "@/project.config";
import PackageDetail from "../../components/feature/package/check/packageDetail";
import PackagePayment from "../../components/feature/package/check/packagePayment";

export default function Page({ params }: any) {
  const { kode } = params;
  const data = useSWR(`${config.apiUrl}/v1/package/check?no_resi=${kode}`)
  const { isLoading } = data
  const detailPaket = data ? data?.data?.packages : null
  return (
    <>
      {/* <MainBanner label={"DETAIL PAKET ANDA"} /> */}
      <div className="container mt-[3%]">
        {/* <div className="bg-white rounded-lg shadow-md min-h-96 p-5"> */}
        {
          isLoading ?
            <>
              <div className="flex flex-row lg:flex-col gap-3"></div>
            </>
            :
            detailPaket ?
              <>
                <div className="flex lg:flex-row flex-col gap-3">
                  <PackageDetail data={detailPaket} />
                  <PackagePayment kode={kode} />
                </div>
              </>
              :
              <>
                data tidak ditemukan
              </>
        }
        {/* </div> */}
      </div>
    </>
  )
}