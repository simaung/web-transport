"use client"

import Image from "next/image"
import { useEffect, useState } from "react"
import { FaCheckCircle } from "react-icons/fa"
import { SendPackage } from "../components/feature/package/sendPackage"
import { CheckPackage } from "../components/feature/package/checkPackage"
import MainBanner from "../components/feature/mainBanner"
import { useSearchParams } from 'next/navigation'

const Package = () => {
    const searchParams = useSearchParams();
    const [pick, setPick] = useState("cek")

    const kode_booking = searchParams?.get('kode');

    useEffect(() => {
        if (kode_booking) {
            setPick("cek")
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <>
            <MainBanner />
            <div className="container flex justify-center mb-7">
                <div className="flex flex-row bg-white p-5 rounded-lg justify-between gap-4 w-full lg:max-w-3xl shadow-sm">
                    <div
                        className={`
                            md:w-full shadow-md shadow-primary p-3 cursor-pointer
                            ${pick == 'cek' ? 'shadow-md' : 'shadow-sm'}
                        `}
                        onClick={() => setPick('cek')}
                    >
                        <div className="flex flex-row gap-2 items-center">
                            <p className="md:text-lg text-sm text-primary font-bold">Cek Paket</p>
                            {pick == 'cek' && (
                                <FaCheckCircle size={20} className="text-primary" />
                            )}
                        </div>
                        <p className="text-xs md:text-sm text-slate-500">Cek status paket yang sudah anda kirimkan</p>
                    </div>
                    <div
                        className={`
                            md:w-full shadow-primary p-3 cursor-pointer
                            ${pick == 'kirim' ? 'shadow-md' : 'shadow-sm'}
                        `}
                        onClick={() => setPick('kirim')}
                    >
                        <div className="flex flex-row gap-2 items-center">
                            <p className="md:text-lg text-sm text-primary font-bold">Kirim Paket</p>
                            {pick == 'kirim' && (
                                <FaCheckCircle size={20} className="text-primary" />
                            )}
                        </div>
                        <p className="text-xs md:text-sm text-slate-500">Kirim paket ke berbagai daerah di Indonesia</p>
                    </div>
                </div>
            </div>
            <div className="container">
                {pick == 'cek' ? (
                    <CheckPackage kode={kode_booking} />
                ) : (
                    <SendPackage />
                )}
            </div>
        </>
    )
}

export default Package