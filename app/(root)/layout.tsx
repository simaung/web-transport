import OutletModal from "@/components/modals/outletModal";
import { Footer } from "./components/footer";
import { Header } from "./components/navbar/header";
import { SWRProvider } from "../provider/swrProvider";
import { Toaster } from "@/components/ui/toaster";
import { Suspense } from 'react'
import FloatingContent from "./components/floatingContent";

type Props = {
  children: React.ReactNode;
};

const RootLayout = ({ children }: Props) => {
  return (
    <Suspense>
      <div className="min-h-screen flex flex-col">
        <Toaster />
        <Header />
        <main className="flex-1 flex flex-col">
          <SWRProvider>
            {children}
          </SWRProvider>
        </main>
        <OutletModal />
        <Footer />
        <FloatingContent />
      </div>
    </Suspense>
  );
};

export default RootLayout;