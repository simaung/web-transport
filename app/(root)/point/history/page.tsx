'use client'
import { GET } from "@/lib/http"
import moment from "moment"
import 'moment/locale/id';
import { useCallback, useEffect, useState } from "react"
import PaginateFooter from "../../components/paginateFooter";

interface historySchema {
  "id": string,
  "type": string,
  "point": string,
  "description": string,
  "last_point": string,
  "created_at": string
}
export default function Page() {
  const [history, setHistory] = useState<historySchema[]>();
  const [pagination, setPagination] = useState({
    perPage: 5,
    page: 1,
    status: ''
  });
  const [paginationResponse, setPaginationResponse] = useState({
    lastPage: 0,
    page: 0
  });
  const getPoint = useCallback(async () => {
    const { data } = await GET('/v1/account/points', pagination)
    setHistory(data.transaction.data)
    setPaginationResponse({
      lastPage: data.transaction.last_page,
      page: data.transaction.current_page
    });
  }, [pagination])

  useEffect(() => {
    getPoint()
  }, [getPoint, pagination]);


  const handlePageChange = (newPage: number) => {
    setPagination({ ...pagination, page: newPage });
  };
  return (
    <>
      <div className="container mt-[3%]">
        <div className="bg-white p-5 rounded-lg shadow-md">
          <div className="flex flex-col md:flex-row md:justify-between border-b-2 pb-1 md:items-center">
            <h1 className="font-bold text-lg">Riwayat Transaksi</h1>
            <div className="flex justify-between">
              <select className="w-fit outline-none text-xs p-1 focus:border-primary border-2" name="status" defaultValue="" onChange={({ target }) => setPagination({ ...pagination, status: target.value })}>
                <option value="">Semua</option>
                <option value="debet">Masuk</option>
                <option value="kredit">Keluar</option>
              </select>
              <select className="w-fit outline-none text-xs p-1 focus:border-primary border-2 text-center" name="perPage" defaultValue="" onChange={({ target }) => setPagination({ ...pagination, perPage: Number(target.value) })}>
                <option value={5}>5</option>
                <option value={10}>10</option>
                <option value={20}>20</option>
                <option value={50}>50</option>
              </select>
            </div>
          </div>
          <div className="flex flex-col gap-3 mt-3">
            {history?.map(e => (
              <>
                <div className="w-full p-3 border-2 rounded-lg">
                  <div className="flex flex-row justify-between items-center">
                    <div>
                      <h5 className="text-sm">
                        {/* {e.type === 'debet' ? "Penambahan Point" : "Pengurangan Point"} */}
                        {e.description || 'Tidak ada deskripsi'}
                      </h5>
                      <small className="text-xs">{moment(e.created_at).locale('id').format('dddd, DD MMMM YYYY HH:mm')} WIB</small>
                    </div>
                    <span className={`${e.type === 'debet' ? "text-green-500" : "text-red-500"} font-bold`}>
                      {e.type === 'debet' ? "+" : "-"} {e.point}
                    </span>
                  </div>
                </div>
              </>
            ))
            }
          </div>
          <PaginateFooter
            currentPage={pagination.page}
            lastPage={paginationResponse.lastPage}
            onPageChange={handlePageChange}
          />
        </div>
      </div>
    </>
  )
}