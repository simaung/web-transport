'use client'
import { Button } from "@/components/ui/button"
import config from "@/project.config"
import Link from "next/link"
import useSWR from "swr"

export default function Page({ params }: any) {
  const { id } = params
  const { data, isLoading: careerLoading } = useSWR(config.apiUrl + '/v1/careers/' + id)
  const career = data ? data.careers : null;
  return (
    <>
      <div className="container">
        <div className="bg-white p-5 rounded-lg shadow-md mt-[3%]">
          {careerLoading ?
            <>
              loading
            </>
            :
            career ?
              <>
                <div className="font-bold text-center uppercase text-lg mb-3">dibutuhkan</div>
                <p>
                  Posisi : <span className="font-bold">{career.name}</span>
                  <br />
                  Lokasi : {career.location}
                  <br />
                  Jenis pekerjaan : {career.type}
                </p>
                <br />
                Deskripsi pekerjaan :
                <div dangerouslySetInnerHTML={{ __html: career.description }}></div>
                <Link href={'/career/' + id + '/apply'}>
                  <Button variant={'primary'} className="w-full">Ajukan lamaran</Button>
                </Link>
              </>
              :
              <>
                tidak ditemukan karir
              </>
          }
        </div>
      </div>
    </>
  )
}