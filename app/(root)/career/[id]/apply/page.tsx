"use client";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import { z } from "zod";
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form";
import config from "@/project.config";
import { POST } from "@/lib/http";
import { toast } from "@/components/ui/use-toast";
import { useRouter } from "next/navigation";

// Custom file validation
const fileSchema = z
  .any()
  .refine((files) => files instanceof FileList && files?.length > 0, "Mohon lampirkan PDF.")
  .refine(
    (files) => [
      "application/pdf",
      // "application/msword"
    ].includes(files?.[0]?.type),
    "Hanya PDF yang kami terima"
  )
  .refine((files) => files?.[0]?.size <= 1 * 1024 * 1024, "Maksimal ukuran file adalah 1 MB")

const reservationFormSchema = z.object({
  name: z.string().min(1, 'Mohon isi dengan nama lengkap anda'),
  email: z.string().email('Mohon masukan email yang benar'),
  phone: z.string().min(9, 'Masukan nomor telepon yang masih aktif'),
  city: z.string().min(1, 'Masukan alamat domisili anda'),
  resume: fileSchema,
});

export default function Apply({ params }: any) {
  const router = useRouter();
  const { id } = params;
  const form = useForm<z.infer<typeof reservationFormSchema>>({
    resolver: zodResolver(reservationFormSchema),
    defaultValues: {
      name: "",
      email: "",
      phone: "",
      city: "",
      // resume: null,
    },
  });

  function onSubmit(values: z.infer<typeof reservationFormSchema>) {
    const formData = new FormData();
    formData.append("name", values.name);
    formData.append("email", values.email);
    formData.append("phone", values.phone);
    formData.append("city", values.city);
    formData.append("resume", values.resume[0]); // Assuming single file upload
    fetch(config.apiUrl + `/v1/careers/${id}/apply`, {
      method: 'POST',
      body: formData,
    }).then(response => response.json())
      .then(data => {
        // console.log(data);
        const { status, message } = data;
        if (status === "SUCCESS") {
          toast({
            variant: "success",
            title: 'Berhasil',
            description: "Lamaran berhasil dikirim, mohon tunggu informasi selanjutnya dari kami melalui email dan whatsapp"
          })
          router.push('/')
        } else {
          toast({
            variant: 'destructive',
            title: "Gagal",
            description: message
          })
        }
      })
      .catch(error => console.error(error));
  }

  return (
    <>
      <div className="container">
        <div className="bg-white p-5 rounded-lg shadow-md mt-[3%]">
          <div className="text-center font-bold text-lg uppercase">data lamaran</div>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-2 justify-between w-full mt-7">
              <div>
                <label htmlFor="name" className="text-sm">Nama Lengkap</label>
                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormControl>
                        <Input type="text" placeholder="..." {...field}></Input>
                      </FormControl>
                      <FormMessage className="text-xs" />
                    </FormItem>
                  )}
                />
              </div>
              <div>
                <label htmlFor="email" className="text-sm">Email</label>
                <FormField
                  control={form.control}
                  name="email"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormControl>
                        <Input type="email" placeholder="...@gmail.com" {...field}></Input>
                      </FormControl>
                      <FormMessage className="text-xs" />
                    </FormItem>
                  )}
                />
              </div>
              <div>
                <label htmlFor="phone" className="text-sm">Nomor Telepon</label>
                <FormField
                  control={form.control}
                  name="phone"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormControl>
                        <Input type="number" placeholder="08xxx..." {...field}></Input>
                      </FormControl>
                      <FormMessage className="text-xs" />
                    </FormItem>
                  )}
                />
              </div>
              <div>
                <label htmlFor="city" className="text-sm">Alamat Domisili</label>
                <FormField
                  control={form.control}
                  name="city"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormControl>
                        <textarea className="w-full rounded-lg hover:border-primary border-2 outline-none duration-300 text-sm p-3" placeholder="Jl. xxx..." {...field}></textarea>
                      </FormControl>
                      <FormMessage className="text-xs" />
                    </FormItem>
                  )}
                />
              </div>
              <div>
                <label htmlFor="resume" className="text-sm">Lampiran</label>
                <FormField
                  control={form.control}
                  name="resume"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormControl>
                        <Input
                          type="file"
                          className="w-full"
                          onChange={(e) => {
                            const files = e.target.files;
                            field.onChange(files);
                          }}
                        />
                      </FormControl>
                      <FormMessage className="text-xs" />
                    </FormItem>
                  )}
                />
                <small className="text-xs italic">Kirimkan file PDF dengan ukuran maksimal 1 MB. klik link berikut untuk <a href="https://www.ilovepdf.com/compress_pdf" target="_blank" rel="noopener noreferrer" className="text-blue-500 hover:text-blue-700">kompres pdf</a></small>
              </div>
              <Button variant="primary" type="submit" className="w-full">Kirimkan Lamaran</Button>
            </form>
          </Form>
        </div>
      </div>
    </>
  );
}
