/* eslint-disable @next/next/no-img-element */
"use client"

import useSWR from "swr"
import MainBanner from "../components/feature/mainBanner"
import DataNotFound from "../components/dataNotFound"
import config from "@/project.config"
import Link from "next/link"

export default function Career() {
  const { data } = useSWR(config.apiUrl + '/v1/careers')
  return (
    <>
      <MainBanner label="Info Karir Arnes" />
      <div className="container">
        <div className="bg-white w-full rounded-lg p-5 shadow-md">
          {data ?
            data.careers.length !== 0 ?
              <>
                <div className="flex flex-col justify-between gap-3">
                  {data.careers.map((e: any, i: number) => {
                    return (
                      <>
                        <Link href={'/career/' + e.id}>
                          <div className="bg-white border-[1px] p-3 cursor-pointer hover:shadow-md duration-300" id={'karir' + i}>
                            <div className="flex flex-row items-center justify-between">
                              <div>
                                <span className="font-bold">{e.name} - {e.location}</span>
                                <div className="default-header" dangerouslySetInnerHTML={{ __html: e.description }}></div>
                              </div>
                              {/* <span className="italic">
                                Detail
                              </span> */}
                            </div>
                          </div>
                        </Link>
                      </>
                    )
                  })}
                </div>
              </>
              :
              <>
                <DataNotFound title="Tidak ditemukan karir" desc="Untuk saat ini tidak ada informasi karir dari kami" />
              </>
            :
            <>
              Loading
            </>
          }
        </div>
      </div>
    </>
  )
}