"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { Button } from "@/components/ui/button"
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { useState, useEffect } from "react"
import { FaSearch } from "react-icons/fa"
import config from "@/project.config"
import { useRouter, useSearchParams } from "next/navigation"
import MainBanner from "../components/feature/mainBanner"
import CalendarSvg from "@/components/image/CalendarSvg"
import LoadingButton from "@/components/LoadingButton"
import DetailReservation from "../components/feature/detailReservation"
const formSchema = z.object({
    bookingCode: z.string().refine(
        (val) => val.length === 14 || val.length === 16,
        {
            message: "Kode Booking harus 14 karakter.",
        }
    ),
});

const Reservation = () => {
    const Router = useRouter();
    const searchParams = useSearchParams();
    const [isLoading, setIsLoading] = useState(false);
    const [detailReservation, setDetailReservation] = useState(false);
    const [isRoundtrip, setIsRoundtrip] = useState(false);
    const [bookingCode, setBookingCode] = useState(searchParams?.get('kode') || '');

    const form = useForm<z.infer<typeof formSchema>>({
        resolver: zodResolver(formSchema),
        defaultValues: {
            bookingCode: searchParams?.get('kode') || "",
        },
    })

    const { register, handleSubmit, setValue, watch, getFieldState, getValues } = form;

    function onSubmit(values: z.infer<typeof formSchema>) {
        setIsLoading(true);
        setBookingCode(values.bookingCode)
        Router.push('/reservation?kode=' + values.bookingCode.toUpperCase());
        setDetailReservation(true)
    }

    useEffect(() => {
        const code = searchParams?.get('kode')
        if (code) {
            onSubmit({ bookingCode: searchParams.get('kode')! })
            const leftCode = code.substring(0, 2);
            if (leftCode === 'RT') {
                setIsRoundtrip(true);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchParams?.get('kode')]);

    return (
        <>
            <MainBanner />
            <div className="container">
                <Form {...form}>
                    <form onSubmit={form.handleSubmit(onSubmit)}>
                        <FormField
                            control={form.control}
                            name="bookingCode"
                            render={({ field }) => (
                                <FormItem className="shadow-lg p-5 rounded-lg bg-white w-full max-w-4xl mx-auto">
                                    <FormMessage className="text-xs" />
                                    <div className="flex flex-col md:flex-row gap-2 min-h-fit">
                                        <FormControl className="">
                                            <Input placeholder={`Kode berawalan ${'B' + config.appCode}`} {...field} className="uppercase" />
                                        </FormControl>
                                        {isLoading ?
                                            <LoadingButton />
                                            :
                                            <Button variant="primary" type="submit" id="buttonCek" className="gap-2 w-full md:max-w-xs"><FaSearch />Cek</Button>
                                        }
                                    </div>
                                </FormItem>
                            )}
                        />
                    </form>
                </Form>
                {detailReservation && (
                    <>
                        <DetailReservation bookingCode={bookingCode} setBookingCode={setValue} setIsLoading={setIsLoading} />
                    </>
                )}
                <div className="flex flex-row text-justify mt-11 gap-9 items-center">
                    <div className={``}>
                        <h1 className="text-3xl italic font-bold text-primary mb-4 tracking-wide">Cek Reservasi</h1>
                        <p className="text-muted-foreground">
                            Dengan fitur cek reservasi, Kamu dapat dengan mudah dan cepat memverifikasi status reservasi kamu. Tidak perlu lagi khawatir tentang kebingungan di terminal atau mencari konfirmasi email. Sekarang, kontrol perjalanan kamu hanya dengan beberapa sentuhan jari!
                        </p>
                    </div>
                    <div className="hidden md:block w-full">
                        <CalendarSvg />
                    </div>
                </div>
            </div >
        </>
    )
}

export default Reservation