"use client"

import {
    Accordion,
    AccordionContent,
    AccordionItem,
    AccordionTrigger,
} from "@/components/ui/accordion"
import config from "@/project.config";
import useSWR from "swr";
import ErrorFetch from "../components/errorFetch";
import SkeletonContent from "../components/skeleton/skeletonContent";
import MainBanner from "../components/feature/mainBanner";

const Faq = () => {
    const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/faq`)

    if (error) return (
        <ErrorFetch />
    )

    if (isLoading) return (
        <SkeletonContent />
    )

    return (
        <>
            <MainBanner label={"Frequently Asked Questions"} />
            <div className="container">
                <div className="bg-white w-full rounded-lg shadow-md p-5">
                    {/* <div className="text-lg font-bold w-full border-b-2 pb-1 text-center uppercase">Frequently Asked Questions</div> */}
                    {data && (
                        <Accordion type="single" collapsible className="w-full">
                            {data.faq.map((f: any) => (
                                <>
                                    <AccordionItem value={f.id}>
                                        <AccordionTrigger className="text-left">{f.question}</AccordionTrigger>
                                        <AccordionContent className="text-slate-600">
                                            <p className="font-bold">Jawaban :</p>
                                            {f.answer}
                                        </AccordionContent>
                                    </AccordionItem>

                                </>
                            ))}
                        </Accordion>
                    )}
                </div>

            </div>
        </>
    )
}

export default Faq