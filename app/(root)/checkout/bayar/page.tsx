"use client"
import { Button } from '@/components/ui/button';
import BCA from '../../components/carabayar/bca';
import BNI from '../../components/carabayar/bni';
import BRI from '../../components/carabayar/bri';
import MANDIRI from '../../components/carabayar/mandiri';
import PERMATA from '../../components/carabayar/permata';
import QRIS from '../../components/carabayar/qris';
import { useRouter, useSearchParams } from 'next/navigation';
import Link from 'next/link';
import { toast } from "@/components/ui/use-toast";
import { useQRCode } from 'next-qrcode';
import { useState } from 'react';
import LoadingButton from '@/components/LoadingButton';


export default function Page() {
  const [isLoading, setIsLoading] = useState(false);
  const { Canvas } = useQRCode();
  const Router = useRouter();
  const searchParams = useSearchParams()
  const payment_code = searchParams.get('payment_code')
  const vaNumber = searchParams.get('vaNumber')
  const booking_code = searchParams.get('booking_code')
  const qrString = searchParams.get('qrString')
  const type = searchParams.get('type')

  const copyText = (entryText: string) => {
    navigator.clipboard.writeText(entryText);
    // alert('Nomor VA di tambahkan ke clipboard...')
    toast({
      variant: "default",
      title: "Clipboard ditambahkan",
      description: "Nomor Virtual Account di tambahkan ke clipboard"
    });
  };

  if (payment_code === null || (vaNumber === null && qrString === null) || booking_code === null) {
    Router.push('/')
  }

  return (
    <>
      <div className="container mt-11">
        <div className="bg-white w-full shadow-lg rounded-lg p-5 border-t-2 border-primary">
          {vaNumber !== null && (
            <>
              <h3 className="text-xl">Virtual Account</h3>
              <div className="flex flex-col md:flex-row md:items-center gap-3 justify-start mt-2 md:mt-1">
                <h2 className="text-2xl font-bold">{vaNumber}</h2>
                <Button
                  variant={"primary"}
                  title='Copy'
                  onClick={() => { copyText(vaNumber) }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="white"
                    className="bi bi-files"
                    viewBox="0 0 16 16"
                  >
                    <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2m0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1M3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1z" />
                  </svg>
                </Button>
              </div>
            </>
          )}
          {qrString !== null && (
            <>
              <div>
                <div className="flex items-center justify-center">
                  <Canvas
                    text={qrString}
                    options={{
                      errorCorrectionLevel: 'M',
                      // margin: 3,
                      // scale: 4,
                      width: 200,
                      color: {
                        dark: '#030302',
                        light: '#faf9f7',
                      },
                    }}
                  />
                </div>
              </div>
            </>
          )}
        </div>
        {isLoading ?
          <LoadingButton className='w-full mt-5' />
          :
          <>
            {type === "trip" && (
              <Link href={`/reservation?kode=${booking_code}`}>
                <Button className='text-white w-full mt-5' onClick={() => setIsLoading(true)} variant={'primary'}>
                  Cek Pembayaran
                </Button>
              </Link>
            )}
            {type === "package" && (
              <Link href={`/package?kode=${booking_code}`}>
                <Button className='text-white w-full mt-5' onClick={() => setIsLoading(true)} variant={'primary'}>
                  Cek Pembayaran
                </Button>
              </Link>
            )}
            {type === "voucher" && (
              <Link href={`/voucher?kode=${booking_code}`}>
                <Button className='text-white w-full mt-5' onClick={() => setIsLoading(true)} variant={'primary'}>
                  Cek Pembayaran
                </Button>
              </Link>
            )}
          </>
        }
        <div className='p-5 rounded-md shadow-md mt-5 border-t-orange-500 border-t-2 default-heading'>
          {payment_code === 'MANDIRI' && (
            <MANDIRI />
          )}
          {payment_code === 'BCA' && (
            <BCA />
          )}
          {payment_code === 'BNI' && (
            <BNI />
          )}
          {payment_code === 'BRI' && (
            <BRI />
          )}
          {payment_code === 'PERMATA' && (
            <PERMATA />
          )}
          {payment_code === 'QR' && (
            <QRIS />
          )}

        </div>
      </div>
    </>
  )
}