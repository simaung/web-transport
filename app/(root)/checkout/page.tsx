/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
"use client"
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import config from "@/project.config"
import moment from "moment"
import 'moment/locale/id';
import { useEffect, useState } from "react"
import { FaEnvelope, FaPhoneAlt, FaTimes, FaUser } from "react-icons/fa"
import useSWR from "swr"
import Link from "next/link"
import { GET, POST } from "@/lib/http";
import { useRouter } from "next/navigation";
import { toast } from "@/components/ui/use-toast";
import { storeArray, getItem } from "@/lib/localStorage";
import LoadingButton from "@/components/LoadingButton";
import CheckoutModal from "../components/feature/checkout/checkoutModal";
import cookie from "@/lib/cookies";
import { Checkbox } from "@/components/ui/checkbox";
import TermUseModal from "../components/feature/checkout/termUseModal";
import BoardingModal from "../components/feature/checkout/boardingModal";

interface sendRequestFormShcema {
  departure_date: string,
  departure_time: string,
  pickup_point: string,
  dropoff_point: string,
  booking_contact: {
    name: string,
    phone: string,
    email: string
  },
  voucher_code: string,
  passengers: passengersSchema[],
  is_point: boolean,
  boarding_ticket: string
}

interface passengersSchema {
  name: string,
  phone: string,
  seat_number: string,
  selling_price: string
}

interface checkoutSchema {
  is_booking_continued: boolean,
  orderer_name: string,
  orderer_phone: string,
  pax: number,
  depart_date: string,
  price: number,
  sub_price: number,
  admin_fee: number,
  voucher_price: number,
  total_price: number,
  point_used: number,
  route: {
    depart_code: string,
    depart_name: string,
    depart_address: string,
    arrival_code: string,
    arrival_name: string,
    arrival_address: string
  },
  payment_data: null,
  passengers: passengersSchema[],
  etd: string,
  eta: string
}

interface responseProfileSchema {
  "name": string,
  "email": string,
  "phone": string,
  "point": number,
  "type": string,
  "avatar": string
}

export default function Page() {
  const Router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [bookingForm, setBookingForm] = useState<sendRequestFormShcema>()
  const [checkoutResponse, setCheckoutResponse] = useState<checkoutSchema>()
  const [profile, setProfile] = useState<responseProfileSchema>();
  const [modalOpen, setModalOpen] = useState(false)
  const [modalTermOpen, setModalTermOpen] = useState(false)
  const [agreTerm, setAgreTerm] = useState(false)
  const [modalBoarding, setModalBoarding] = useState(false)

  const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)
  var getPoint = (id: string) => {
    return dataPoints?.points?.find((c: any) => c.id === id)?.name
  }

  async function book() {
    setIsLoading(true);
    const { data } = await POST('/v1/booking', bookingForm || {})
    const { booking_detail, status_code, status, message } = data
    if (status === "SUCCESS") {
      storeArray('bookHistory', { ...data?.booking_detail, ...bookingForm });
      if (booking_detail.transaction_status === 'book') {
        Router.push(`/checkout/${booking_detail.booking_id}`)
        toast({
          variant: "success",
          title: "Berhasil melakukan booking",
          description: "Silahkan lanjutkan pembayaran",
        })
      } else {
        Router.push(`/reservation?kode=${booking_detail.booking_id}`)
        toast({
          variant: "success",
          title: "Berhasil melakukan booking",
          description: "Transaksi ini sudah lunas menggunakan point, silahkan lanjut cetak tiket.",
        })
      }
    } else {
      toast({
        variant: "warning",
        title: "Perhatian",
        description: message,
      })
      setIsLoading(false);
    }
  }

  async function checkout(deleteVoucher: any) {
    if (bookingForm?.dropoff_point) {
      const { data } = await POST('/v1/checkout', {
        ...bookingForm,
        voucher_code: deleteVoucher ? "" : bookingForm?.voucher_code
      });
      if (data?.checkout?.voucher_message !== null) {
        toast({
          variant: data?.checkout?.voucher_status ? "success" : "warning",
          title: data?.checkout?.voucher_status ? "Berhasil" : "Perhatian",
          description: data?.checkout?.voucher_message || 'Kesalahan Server Checkout',
        })
      }
      setCheckoutResponse(data.checkout)
    }
  }

  async function getProfile() {
    const { data } = await GET("/v1/account/profile", {})
    if (data.status === "SUCCESS") {
      setProfile(data.data)
    }
  }
  useEffect(() => {
    if (cookie.getToken()) {
      getProfile();
    }
    setBookingForm(getItem('bookingForm'));
    // const data = getItem('bookHistory');
    // if (data) {
    //   const booking_code = data[0]['booking_id']
    //   // const response = await GET('/v1/booking/' + booking_code, {})
    //   Router.push('/checkout/' + booking_code)
    // }
  }, []);

  useEffect(() => {
    checkout(false)
  }, [bookingForm?.dropoff_point, bookingForm?.is_point]);

  return (
    <>
      <TermUseModal isOpen={modalTermOpen} setIsOpen={setModalTermOpen} onSubmit={setAgreTerm} />
      <CheckoutModal isOpen={modalOpen} setIsOpen={setModalOpen} onChange={setBookingForm} data={bookingForm} onSubmit={checkout} />
      <BoardingModal isOpen={modalBoarding} setIsOpen={setModalBoarding} onChange={setBookingForm} data={bookingForm} onSubmit={checkout} />
      <div className="container">
        {
          bookingForm ? (
            <>
              <div className="bg-white shadow-lg mt-5 p-5 w-full max-w-lg mx-auto min-h-[500px]">
                <div className="flex flex-row md:justify-between justify-center items-center border-b-2 border-dashed border-gray-200 uppercase">
                  <h4 className="font-bold py-3 hidden md:block">detail pesanan</h4>
                  <img src="/images/logo/logo.png" alt="" className="w-36 pb-5 md:pb-0" />
                </div>
                <div className="flex flex-col sm:flex-row gap-1 md:gap-0 items-start justify-between mt-5">
                  <span className="font-bold">{getPoint(bookingForm?.pickup_point!) || "-"}</span>
                  <span className="font-bold">{getPoint(bookingForm?.dropoff_point!) || "-"}</span>
                </div>
                <div className="flex flex-row items-start justify-between mt-1 text-sm">
                  <span>{moment(bookingForm?.departure_date, 'YYYY-MM-DD').locale('id').format("dddd, DD MMMM YYYY") || "-"}</span>
                  <span>{bookingForm?.departure_time + " WIB" || "-"}</span>
                </div>
                <div className="text-sm mt-7 pt-3 border-t-2 border-dashed">
                  <span className="uppercase font-bold md:block hidden">
                    Data Pemesan
                  </span>
                  <div className="flex flex-col md:flex-row gap-1 md:mt-1 justify-between md:items-center">
                    <div>
                      <span className="text-xs md:block hidden">
                        Nama Pemesan
                      </span>
                      {/* <br /> */}
                      <span className="text-base font-bold flex flex-row items-center gap-3">
                        <FaUser />
                        <span>
                          {bookingForm?.booking_contact?.name}
                        </span>
                      </span>
                    </div>
                    <div className="flex flex-col gap-1">
                      <span className="flex items-center gap-3"><FaPhoneAlt />{bookingForm?.booking_contact?.phone}</span>
                      <span className="flex items-center gap-3"><FaEnvelope />{bookingForm?.booking_contact?.email}</span>
                    </div>
                  </div>
                  <div className="overflow-x-auto">
                    <table className="border-[1px] p-3 mt-3 w-full rounded-lg">
                      <thead>
                        <tr>
                          <th className="border-2 py-3 w-12">No.</th>
                          <th className="border-2 p-3">Nama</th>
                          {/* <th className="border-2 p-3">Telepon</th> */}
                          <th className="border-2 py-3 w-16">Kursi</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          bookingForm?.passengers.map((e: passengersSchema, i: number) => {
                            return (
                              <>
                                <tr>
                                  <td className="border-2 py-3 w-fit text-center">{i + 1}</td>
                                  <td className="border-2 p-3">{e.name}</td>
                                  {/* <td className="border-2 p-3">{e.phone}</td> */}
                                  <td className="border-2 py-3 text-center">{e.seat_number}</td>
                                </tr>
                              </>
                            )
                          })
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="mt-3 border-y-2 border-dashed border-gray-300 py-3">
                  <div className="border-b-[1px] border-gray-50 pb-2 flex lg:flex-row flex-col gap-1 lg:items-center justify-between text-sm">
                    {cookie.getToken() ?
                      <>
                        <div className="flex justify-start items-center gap-2 lg:self-center">
                          <Checkbox
                            id={'is-point'}
                            className=''
                            checked={bookingForm.is_point}
                            disabled={profile?.point! < checkoutResponse?.price!}
                            onCheckedChange={(e: boolean) => {
                              if (bookingForm?.voucher_code) {
                                // alert('Anda tidak bisa menggunakan point dan voucher secara bersamaan')
                                toast({
                                  variant: "warning",
                                  title: "Peringatan!",
                                  description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                                })
                              } else {
                                // setModalOpen(true)
                                setBookingForm({ ...bookingForm, is_point: e });
                              }
                            }} />
                          <label htmlFor="is-point" className="text-xs text-gray-600">Gunakan (<span className="text-primary font-bold italic">{profile?.point.toLocaleString('id-ID')}</span>) Point</label>
                        </div>
                      </>
                      :
                      <>
                        &nbsp;
                      </>
                    }
                    {/* <div className="flex lg:flex-row flex-col w-full justify-end items-end gap-3"> */}
                    {/* <div className="flex flex-row justify-between">
                        <div className="relative">
                          <input
                            type="text"
                            placeholder="Boarding / E-tiket"
                            className={`bg-gray-50 cursor-pointer p-3 py-2 w-fit text-xs border-b-2 text-right rounded-lg ${bookingForm.boarding_ticket ? "pr-9" : ""}`}
                            value={bookingForm.boarding_ticket}
                            onClick={() => setModalBoarding(true)}
                            readOnly
                          />
                          {bookingForm?.boarding_ticket && (
                            <FaTimes
                              className="absolute right-3 top-[9px] text-red-500 hover:font-bold duration-300 hover:bg-gray-300 cursor-pointer"
                              onClick={() => {
                                setBookingForm({ ...bookingForm!, boarding_ticket: "" });
                                checkout(true);
                              }}
                            />
                          )}
                        </div>
                      </div> */}
                    <div className="flex flex-row justify-between">
                      <div className="relative">
                        <input
                          type="text"
                          placeholder="Tambahkan Voucher"
                          className={`bg-gray-50 cursor-pointer text-right p-3 py-2 text-xs border-b-2 rounded-lg ${bookingForm.voucher_code ? "pr-9" : ""}`}
                          value={bookingForm.voucher_code}
                          onClick={() => {
                            if (bookingForm.is_point === true) {
                              // alert('Anda tidak bisa menggunakan point dan voucher secara bersamaan')
                              toast({
                                variant: "warning",
                                title: "Peringatan!",
                                description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                              })
                            } else {
                              setModalOpen(true)
                            }
                          }}
                          readOnly
                        />
                        {bookingForm?.voucher_code && (
                          <FaTimes
                            className="absolute right-3 top-[9px] text-red-500 hover:font-bold duration-300 hover:bg-gray-300 cursor-pointer"
                            onClick={() => {
                              setBookingForm({ ...bookingForm!, voucher_code: "" });
                              checkout(true);
                            }}
                          />
                        )}
                      </div>
                      {/* <Button type="button" variant="primary" className="w-full mt-3" onClick={checkout}>Masukan</Button> */}
                    </div>
                    {/* </div> */}
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between text-sm">
                    <span>Harga Tiket</span>
                    <div className="w-24 flex flex-row justify-between">
                      <span>Rp. </span>
                      <span>{checkoutResponse?.price.toLocaleString('id-ID')}</span>
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Jumlah Penumpang</span>
                    <span>{"X " + bookingForm?.passengers?.length || "..."}</span>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Sub Total</span>
                    <div className="w-24 flex flex-row justify-between">
                      <span>Rp. </span>
                      <span>{checkoutResponse?.sub_price.toLocaleString('id-ID')}</span>
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Diskon Voucher</span>
                    <div className="flex">
                      <span>-</span>
                      <div className="w-24 flex flex-row justify-between">
                        <span>Rp. </span>
                        <span>{checkoutResponse?.voucher_price.toLocaleString('id-ID')}</span>
                      </div>
                    </div>
                  </div>
                  {bookingForm?.is_point && (
                    <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                      <span>Diskon Point</span>
                      <div className="flex">
                        <span>-</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.point_used.toLocaleString('id-ID')}</span>
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Total Harga</span>
                    <div className="w-24 flex flex-row justify-between font-bold text-red-500">
                      <span>Rp. </span>
                      <span className="">{checkoutResponse?.total_price.toLocaleString('id-ID')}</span>
                    </div>
                  </div>
                </div>
                <div className="flex flex-row gap-2 items-center mt-3">
                  <Checkbox id={'agre-term'} className='' checked={agreTerm} disabled={false} onClick={() => { setAgreTerm(!agreTerm) }} />
                  <label htmlFor="agre-term2" className="text-sm cursor-pointer hover:underline duration-200 hover:text-blue-500" title="Klik untuk membaca Syarat & Ketentuan" onClick={() => { setModalTermOpen(true) }}>Saya telah membaca Syarat & Ketentuan</label>
                </div>
                {
                  isLoading ?
                    <LoadingButton className="w-full mt-3" />
                    :
                    <Button type="button" variant="primary" className="w-full mt-3" onClick={book} disabled={!agreTerm || !checkoutResponse?.is_booking_continued}>Lanjutkan Pembayaran</Button>
                }
              </div>
            </>
          ) :
            (
              <>
                <div className="bg-gray-50 animate-pulse shadow-lg mt-5 p-5 w-full max-w-lg mx-auto min-h-[500px]">
                </div>
              </>
            )
        }
      </div>
    </>
  )
}