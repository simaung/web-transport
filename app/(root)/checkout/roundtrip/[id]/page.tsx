"use client"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { getItem, store, storeArray } from "@/lib/localStorage"
import config from "@/project.config"
import moment, { localeData } from "moment"
import { useEffect, useState } from "react"
import { FaEnvelope, FaPhoneAlt, FaUser } from "react-icons/fa"
import useSWR from "swr"
import { toast } from "@/components/ui/use-toast"
import SkeletonForm from "../../../components/skeleton/skeletonForm"
import { groupPayment } from "@/lib/grouping"
import { useRouter } from "next/navigation"
import { POST } from "@/lib/http"
import LoadingButton from "@/components/LoadingButton"
import { ZodNumberDef } from "zod"

interface bookingFormShcema {
  "transaction_code": string,
  "total_pax": number,
  "total_sub_price": number,
  "total_voucher_price": number,
  "total_point_used": number,
  "grand_total_price": number,
  "payment_status": string,
  "expired_at": string,
  "depart": roundtripFormSchema,
  "return": roundtripFormSchema
}

interface roundtripFormSchema {
  "passenger": passengersSchema[],
  "booking_code": string,
  "depart": string,
  "arrival": string,
  "depart_at": string,
  "etd": string,
  "transaction_status": string,
  "payment_status": string,
  "original_price": string,
  "total_price": string,
  "price_data": priceDataShcema,
  "voucher_code": string,
  "orderer_name": string,
  "orderer_phone": string,
  "orderer_email": string,
  "pax": number,
  "order_from": string,
  "reschedule": string,
}

interface priceDataShcema {
  "unit_price": ZodNumberDef,
  "pax": ZodNumberDef,
  "price": ZodNumberDef,
  "charg_bagasi": ZodNumberDef,
  "discount": ZodNumberDef,
  "discount_promo": ZodNumberDef,
  "discount_voucher": ZodNumberDef,
  "point_used": ZodNumberDef,
  "total_deduction": ZodNumberDef,
  "total_price": ZodNumberDef
}

interface passengersSchema {
  name: string,
  phone: string,
  seat_number: string,
  selling_price: string,
  passenger_name: string,
  passenger_seat: string
}

interface selectedPaymentSchema {
  booking_code: string,
  payment_method: string,
  payment_code: string
}

/* eslint-disable @next/next/no-img-element */
export default function Page({ params }: any) {
  const method = JSON.stringify({
    VA: "Virtual Account",
    QR: "Kode Qr",
    EWALLET: 'Dompet Online'
  })
  const [isLoading, setIsLoading] = useState(false);

  const Router = useRouter();
  const { id } = params;
  const [selectedPayment, setSelectedPayment] = useState<selectedPaymentSchema>({
    booking_code: id,
    payment_method: '',
    payment_code: ''
  })
  const { data, error } = useSWR<{ result: bookingFormShcema, message: string, status: string, status_code: number }>(`${config.apiUrl}/v1/booking/${id}`)
  const payment = useSWR(`${config.apiUrl}/v1/payment/${id}/roundtrip`)
  const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)
  var getPoint = (id: string) => {
    return dataPoints?.points?.find((c: any) => c.id === id)?.name || 'Unknown';
  };

  const orderFrom = data?.result?.depart?.order_from && data?.result?.return?.order_from;
  const invalidOrderFrom = orderFrom !== "whatsapp" && orderFrom !== "web" && orderFrom !== "mobile";

  if (payment?.data?.status === 'FAILED' || invalidOrderFrom) {
    if (data?.result?.depart?.booking_code) {
      Router.push('/reservation?kode=' + data?.result?.depart?.booking_code);
    }
    return;
  }

  const bookingForm = data?.result && data.result;
  const paymentGroup = payment?.data?.payments && (groupPayment(payment?.data?.payments, "type"))

  async function CreatePayment() {
    setIsLoading(true);
    if (selectedPayment.payment_code === '') {
      toast({
        variant: "warning",
        title: "TIDAK LENGKAP",
        description: "mohon pilih terlebih dahulu metode pembayaran",
      });
      setIsLoading(false);
      return
    }
    const { data: res } = await POST('/v1/payment/create/roundtrip', selectedPayment);
    if (res.status !== 'SUCCESS') {
      toast({
        variant: "destructive",
        title: res?.status,
        description: res?.message,
      });
      // Router.push('/reservation?kode=' + id)
      Router.push('/reservation?kode=' + data?.result?.depart?.booking_code);
      return
    }
    toast({
      variant: "success",
      title: "Berhasil membuat pembayaran",
      description: "Silahkan ikuti langkah pembayaran berikut."
    });

    const { paymentUrl, qrString, vaNumber } = res?.payments;
    const { booking_code, payment_code } = selectedPayment;

    const localData = getItem('paymentRoundtripHistory')
    if (!localData || localData.booking_code !== id) {
      store('paymentRoundtripHistory', { booking_code: id, [selectedPayment.payment_code]: vaNumber, qrString: qrString, expiration_time: bookingForm?.expired_at })
    } else {
      store('paymentRoundtripHistory', { ...localData, [selectedPayment.payment_code]: !localData[selectedPayment.payment_code] ? vaNumber : localData[selectedPayment.payment_code], qrString: !localData.qrString ? qrString : localData.qrString })
    }
    const latestLocalData = getItem('paymentRoundtripHistory');


    // if (paymentUrl !== '') {
    //   window.open(paymentUrl, '_blank');
    //   Router.push('/reservation?kode=' + id)
    //   return
    // }

    if (qrString !== '') {
      Router.push(`/checkout/bayar?payment_code=${payment_code}&&qrString=${qrString}&&booking_code=${bookingForm?.depart?.booking_code}&&type=trip`)
      return
    }

    if (vaNumber !== '') {
      // console.log(vaNumber, 'vaNumber');
      // Router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${vaNumber}&&booking_code=${booking_code}&&type=trip`)
      Router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${latestLocalData[selectedPayment.payment_code]}&&booking_code=${bookingForm?.depart?.booking_code}&&type=trip`)
      return
    }
  }
  const roundtripDetail = (data: roundtripFormSchema, label: string) => {
    return (
      <>
        {/* {JSON.stringify(data)} */}
        <div className="mt-3">
          <div className="flex flex-row justify-between items-center border-t-2 pt-2 border-b-[1px] border-gray-200 border-b-gray-50 border-dashed w-full mt-7 mb-3">
            <div className="uppercase font-semibold text-primary">{label}</div>
            <div className="font-semibold">{data.booking_code}</div>
          </div>
          <div className="flex flex-col sm:flex-row items-start justify-between gap-3">
            <span className="font-bold">{getPoint(data?.depart) || "-"}</span>
            {/* <span className="font-bold sm:block hidden">-</span> */}
            <span className="font-bold">{getPoint(data?.arrival) || "-"}</span>
          </div>
          <div className="flex flex-col md:flex-row items-start justify-between mt-1 text-sm">
            <span>{moment(data?.depart_at, 'YYYY-MM-DD').locale('id').format("dddd, DD MMMM YYYY") || "-"}</span>
            <span>{moment(data?.etd, 'HH:mm:ss').format("HH:mm") + " WIB" || "-"}</span>
          </div>
          <div className="overflow-x-auto">
            <table className="border-[1px] p-3 mt-3 w-full rounded-lg">
              <thead>
                <tr>
                  <th className="border-2 py-3 w-12">No.</th>
                  <th className="border-2 p-3">Nama</th>
                  {/* <th className="border-2 p-3">Telepon</th> */}
                  {/* <th className="border-2 py-3">Harga</th> */}
                  <th className="border-2 py-3 w-16">Kursi</th>
                </tr>
              </thead>
              <tbody>
                {
                  data?.passenger?.map((e: passengersSchema, i: number) => {
                    return (
                      <tr key={"paxcheckout" + i}>
                        <td className="border-2 p-3 w-fit text-center">{i + 1}</td>
                        <td className="border-2 p-3 text-nowrap">{e.passenger_name}</td>
                        {/* <td className="border-2 p-3">{e.phone}</td> */}
                        {/* <td className="border-2 p-3 text-center text-nowrap">Rp. {e.selling_price}</td> */}
                        <td className="border-2 p-3 text-center">{e.passenger_seat}</td>
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          </div>
        </div>
      </>
    )
  }
  return (
    <>
      <div className="container md:mt-7 mt-3">
        {bookingForm ?
          <>
            <div className="flex flex-col xl:flex-row gap-3">
              <div className="bg-white rounded-lg shadow-lg p-5 w-full xl:max-w-lg">
                <div className="flex flex-row justify-center sm:justify-between items-center border-b-2 border-dashed border-gray-200 uppercase pb-3 sm:pb-0">
                  <h4 className="font-bold py-3 sm:block hidden">Detail Pesanan</h4>
                  <img src="/images/logo/logo.png" alt="" className="w-36" />
                </div>
                <div className="flex flex-row justify-start sm:justify-between mt-3 items-center">
                  <span className="sm:block hidden">
                    Kode Transaksi
                  </span>
                  <span className="font-bold">
                    {bookingForm?.transaction_code}
                  </span>
                </div>
                <div className="uppercase font-bold mt-3">
                  Data Pemesan
                </div>
                <div className="flex flex-col md:flex-row gap-1 md:mt-1 justify-between md:items-center">
                  <div className="">
                    <span className="text-xs md:block hidden">
                      Nama Pemesan
                    </span>
                    {/* <br /> */}
                    <span className="text-base font-bold flex flex-row items-center gap-3">
                      <FaUser />
                      <span>
                        {bookingForm?.depart?.orderer_name}
                      </span>
                    </span>
                  </div>
                  <div className="flex flex-col gap-1">
                    <span className="flex items-center gap-3"><FaPhoneAlt />{bookingForm?.depart?.orderer_phone}</span>
                    <span className="flex items-center gap-3"><FaEnvelope />{bookingForm?.depart?.orderer_email}</span>
                  </div>
                </div>
                {roundtripDetail(bookingForm?.depart, 'Pergi')}
                {roundtripDetail(bookingForm?.return, 'Pulang')}
                <div className="mt-3 border-y-2 border-dashed border-gray-300 py-3">
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between text-sm mb-2">
                    <span>&nbsp;</span>
                    <div className="w-56 text-right font-bold border-b-2 max-w-32">
                      {(bookingForm?.depart?.voucher_code || bookingForm?.return?.voucher_code) || <><span className="italic font-thin text-gray-300">Tanpa Voucher</span></>}
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Sub Total</span>
                    <div className="w-24 flex flex-row justify-between">
                      <span>Rp. </span>
                      <span>{bookingForm?.total_sub_price?.toLocaleString('id-ID') || "api"}</span>
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Total Potongan</span>
                    <div className="flex">
                      <span>-</span>
                      <div className="w-24 flex flex-row justify-between">
                        <span>Rp. </span>
                        <span>{(bookingForm?.total_point_used !== 0 ? bookingForm?.total_point_used : bookingForm?.total_point_used)?.toLocaleString('id-ID') || "api"}</span>
                      </div>
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Total Harga</span>
                    <div className="w-24 flex flex-row justify-between font-bold text-red-500">
                      <span>Rp. </span>
                      <span className="">{bookingForm?.grand_total_price?.toLocaleString('id-ID')}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-white rounded-lg shadow-lg p-5 w-full flex flex-col justify-start gap-3 h-fit">
                <div className="flex flex-row justify-between items-center border-b-2 border-dashed border-gray-200 uppercase">
                  <h4 className="font-bold py-3">PEMBAYARAN</h4>
                </div>
                <div className="h-96 py-3 overflow-y-auto">
                  {payment.isLoading ?
                    (
                      <>
                        Loading
                      </>
                    )
                    :
                    <>
                      {payment?.data?.payments?.length === 0 ?
                        <>
                          Payment tidak di temukan
                        </>
                        :
                        <>
                          <div className="flex flex-col gap-7">
                            {Object?.keys(paymentGroup)?.map(u => (
                              <>
                                <div className="flex flex-col gap-3" key={u + "paymentgroup"}>
                                  <span className="font-bold w-full border-b-2 pb-2">{JSON.parse(method)[u]}</span>
                                  {paymentGroup[u].map((s: { name: string, code: string, image: string, type: string, status: string }) => (
                                    <>
                                      <div
                                        className={`w-full ${s.status === 'active' ? "bg-white hover:border-primary cursor-pointer" : "bg-gray-100 cursor-not-allowed text-gray-400"} p-3 rounded-lg border-2 duration-300 flex flex-row items-center justify-between ${s.code === selectedPayment.payment_code ? "border-primary" : ""}`}
                                        key={s.name + "paymentgroup2"}
                                        onClick={() => {
                                          if (s.status === 'active') {
                                            setSelectedPayment({ ...selectedPayment, payment_code: s.code, payment_method: s.type })
                                          }
                                        }}
                                      >
                                        <div className="flex flex-col">
                                          <span>
                                            {s.name}
                                          </span>
                                          <span className="text-[9px] italic text-red-300">
                                            {s.status !== 'active' && ('Minimal transaksi Rp. 50.000')}
                                          </span>
                                        </div>
                                        <img src={s.image} alt="..." className={`h-8 ${s.status === 'active' ? "opacity-100" : "opacity-45"}`} />
                                      </div>
                                    </>
                                  ))}
                                </div>
                              </>
                            ))}
                          </div>
                        </>
                      }
                    </>
                  }
                </div>
                {
                  isLoading ?
                    <LoadingButton className="w-full" />
                    :
                    <Button type="button" variant="primary" className="w-full" onClick={CreatePayment}>Lanjutkan</Button>
                }
              </div>
            </div>
          </>
          :
          <div className="h-96 mt-[3%] bg-gray-100 w-full rounded-lg animate-pulse"></div>
        }
      </div >
    </>
  )
}