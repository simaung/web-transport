/* eslint-disable @next/next/no-img-element */
"use client"

import LoadingButton from "@/components/LoadingButton";
import { Button } from "@/components/ui/button";
import { Checkbox } from "@/components/ui/checkbox";
import { toast } from "@/components/ui/use-toast";
import cookie from "@/lib/cookies";
import { GET, POST } from "@/lib/http";
import { getItem, storeArray } from "@/lib/localStorage";
import config from "@/project.config";
import moment from "moment";
import { useCallback, useEffect, useState } from "react"
import { FaEnvelope, FaPhoneAlt, FaTimes, FaUser } from "react-icons/fa";
import useSWR from "swr";
import TermUseModal from "../../components/feature/checkout/termUseModal";
import CheckoutRoundtripModal from "../../components/feature/checkout/checkoutRoundtripModal";
import 'moment/locale/id'
import { useRouter } from "next/navigation";

interface roundtripSchema {
  "is_point": boolean,
  "voucher_code": string,
  "depart": formSchema,
  "return": formSchema
}
interface formSchema {
  "is_point": boolean,
  "voucher_code": string,
  "pickup_point": string,
  "pickup_name": string,
  "dropoff_point": string,
  "dropoff_name": string,
  "pax": number,
  "tripcode": string,
  "departure_date": string,
  "departure_time": string,
  "passengers": passengersSchema[],
  "booking_contact": bookingContact
}
interface bookingContact {
  "name": string,
  "phone": string,
  "email": string,
}
interface passengersSchema {
  "name": string,
  "phone": string,
  "seat_number": string
}

interface profileShcema {
  "name": string,
  "email": string,
  "phone": string,
  "point": number,
  "type": string,
  "avatar": string,
  "province_id": number,
  "province": string,
  "regencies_id": number,
  "regencies": string,
  "district_id": number,
  "district": string,
  "villages_id": number,
  "villages": string,
}

interface responseCheckoutSchema {
  "is_booking_continued": boolean,
  "total_pax": number,
  "total_voucher_price": number,
  "total_sub_price": number,
  "total_point_used": number,
  "grand_total_price": number,
  "depart": checkoutSchema,
  "return": checkoutSchema
}

interface checkoutSchema {
  "pax": number,
  "depart_date": string,
  "price": number,
  "sub_price": number,
  "voucher_price": number,
  "point_used": number,
  "total_price": number,
  "route": routeSchema,
  "payment_data": null,
  "passengers": passengerCheckoutSchema[],
  "etd": string,
  "eta": string,
  "voucher_message": null,
  "voucher_status": false,
  "is_booking_continued": boolean,
}
interface routeSchema {
  "depart_code": string,
  "depart_name": string,
  "depart_address": string,
  "arrival_code": string,
  "arrival_name": string,
  "arrival_address": string,
}

interface passengerCheckoutSchema {
  "seat_number": string,
  "selling_price": number
}


export default function RoundTrip() {
  const Router = useRouter();

  const [bookingForm, setRoundTripFormData] = useState<roundtripSchema | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(false);
  const [profile, setProfile] = useState<profileShcema>()
  const [checkoutResponse, setCheckoutResponse] = useState<responseCheckoutSchema>()
  const [modalTermOpen, setModalTermOpen] = useState(false)
  const [agreTerm, setAgreTerm] = useState(false)
  const [modalOpen, setModalOpen] = useState(false)


  const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)
  var getPoint = (id: string) => {
    return dataPoints?.points?.find((c: any) => c.id === id)?.name || 'Unknown';
  };

  async function getProfile() {
    const { data } = await GET("/v1/account/profile", {})
    if (data.status === "SUCCESS") {
      setProfile(data.data)
    }
  }

  const checkout = useCallback(async () => {
    const { data } = await POST('/v1/checkout/roundtrip', {
      ...bookingForm,
    });
    if (data?.checkout_roundtrip) {
      if (data?.checkout_roundtrip?.voucher_message !== null || data?.checkout_roundtrip?.depart?.voucher_message !== null || data?.checkout_roundtrip?.return?.voucher_message !== null) {
        toast({
          variant: data?.checkout_roundtrip?.voucher_status ? "success" : "warning",
          title: data?.checkout_roundtrip?.voucher_status ? "Berhasil" : "Perhatian",
          description: data?.checkout_roundtrip?.voucher_message || data?.checkout_roundtrip?.depart?.voucher_message || data?.checkout_roundtrip?.return?.voucher_message || 'Kesalahan Server Checkout',
        })
      }
      setCheckoutResponse(data.checkout_roundtrip);
    } else {
      toast({
        variant: "warning",
        title: "Kesalahan",
        description: "Checkout data is invalid.",
      });
    }
    // setCheckoutResponse(data.checkout_roundtrip)
  }, [bookingForm]);

  async function book() {
    setIsLoading(true);
    const { data } = await POST('/v1/booking/roundtrip', bookingForm || {})
    const { booking_roundtrip, status_code, status, message } = data
    if (status === "SUCCESS") {
      console.log(booking_roundtrip);
      storeArray('bookHistory', { ...booking_roundtrip?.depart, ...bookingForm });
      storeArray('bookHistory', { ...booking_roundtrip?.return, ...bookingForm });

      // storeArray('bookHistory', { ...data?.booking_roundtrip, ...bookingForm });
      if (booking_roundtrip.payment_status === 'pending') {
        Router.push(`/checkout/roundtrip/${booking_roundtrip.transaction_code}`)
        toast({
          variant: "success",
          title: "Berhasil melakukan booking",
          description: "Silahkan lanjutkan pembayaran",
        })
      } else {
        Router.push(`/reservation?kode=${booking_roundtrip.depart.booking_id}`)
        toast({
          variant: "success",
          title: "Berhasil melakukan booking",
          description: "Transaksi ini sudah lunas menggunakan point, silahkan lanjut cetak tiket.",
        })
      }
    } else {
      toast({
        variant: "warning",
        title: "Perhatian",
        description: message,
      })
      setIsLoading(false);
    }
  }

  useEffect(() => {
    if (!profile && cookie.getToken()) {
      getProfile();
    }
  }, [profile]);

  useEffect(() => {
    if (cookie.getToken()) {
      getProfile()
    }
    const data = getItem('bookingForm');
    if (!data) return;
    setRoundTripFormData({
      is_point: false,
      voucher_code: '',
      depart: {
        is_point: false,
        voucher_code: "",
        pickup_point: data.pickup_point,
        pickup_name: data.pickup_name,
        dropoff_point: data.dropoff_point,
        dropoff_name: data.dropoff_name,
        pax: data.passengers.length,
        tripcode: data.tripcode,
        departure_date: data.departure_date,
        departure_time: data.departure_time,
        passengers: data.passengers,
        booking_contact: data.booking_contact
      },
      return: {
        is_point: false,
        voucher_code: "",
        pickup_point: data.dropoff_point,
        pickup_name: data.dropoff_name,
        dropoff_point: data.pickup_point,
        dropoff_name: data.pickup_name,
        pax: data.return_passengers.length,
        tripcode: data.return_tripcode,
        departure_date: data.return_date,
        departure_time: data.return_time,
        passengers: data.return_passengers,
        booking_contact: data.booking_contact
      }
    })
  }, []);

  useEffect(() => {
    if (bookingForm) {
      checkout();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    checkout,
    bookingForm?.depart?.voucher_code,
    bookingForm?.depart?.is_point,
    bookingForm?.return?.voucher_code,
    bookingForm?.return?.is_point,
  ]);
  return (
    <>
      <TermUseModal isOpen={modalTermOpen} setIsOpen={setModalTermOpen} onSubmit={setAgreTerm} />
      <CheckoutRoundtripModal isOpen={modalOpen} setIsOpen={setModalOpen} onChange={setRoundTripFormData} data={bookingForm} onSubmit={checkout} />
      <div className="container">
        <div className="flex flex-col gap-3 w-full">
          <div className="bg-white border-2 p-5 w-full mx-auto">
            <div className="flex flex-row md:justify-between justify-center items-center border-b-2 border-dashed border-gray-200 uppercase">
              <h4 className="font-bold py-3 hidden md:block">Data Pemesan</h4>
              <img src="/images/logo/logo.png" alt="" className="w-36 pb-5 md:pb-0" />
            </div>
            <div className="mt-3">
              <div className="flex flex-col md:flex-row gap-1 md:mt-1 justify-between md:items-center">
                <div>
                  <span className="text-xs md:block hidden">
                    Nama Pemesan
                  </span>
                  {/* <br /> */}
                  <span className="text-base font-bold flex flex-row items-center gap-3">
                    <FaUser />
                    <span>
                      {bookingForm?.depart?.booking_contact?.name}
                    </span>
                  </span>
                </div>
                <div className="flex flex-col gap-1">
                  <span className="flex items-center gap-3"><FaPhoneAlt />{bookingForm?.depart?.booking_contact?.phone}</span>
                  <span className="flex items-center gap-3"><FaEnvelope />{bookingForm?.depart?.booking_contact?.email}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col md:flex-row md:gap-3 gap-1 w-full">
            {
              checkoutResponse ? (
                <>
                  <div className="bg-white border-2 p-5 w-full mx-auto">
                    <div className="flex flex-row md:justify-between justify-center items-center border-b-2 border-dashed border-gray-200 uppercase">
                      <h4 className="font-bold pb-3">Pergi</h4>
                    </div>
                    <div className="flex flex-col sm:flex-row gap-1 md:gap-0 items-start justify-between mt-5">
                      <span className="font-bold">{getPoint(checkoutResponse?.depart?.route?.depart_code!) || "-"}</span>
                      <span className="font-bold">{getPoint(checkoutResponse?.depart?.route?.arrival_code!) || "-"}</span>
                    </div>
                    <div className="flex flex-row items-start justify-between mt-1 text-sm">
                      <span>{moment(checkoutResponse?.depart?.depart_date, 'YYYY-MM-DD').locale('id').format("dddd, DD MMMM YYYY") || "-"}</span>
                      <span>{checkoutResponse?.depart?.etd + " WIB" || "-"}</span>
                    </div>
                    <div className="text-sm mt-7 pt-3 border-t-2 border-dashed">
                      <div className="overflow-x-auto">
                        <table className="border-[1px] p-3 mt-3 w-full rounded-lg">
                          <thead>
                            <tr>
                              <th className="border-2 py-3 w-12">No.</th>
                              <th className="border-2 p-3">Nama</th>
                              <th className="border-2 py-3 w-16">Kursi</th>
                            </tr>
                          </thead>
                          <tbody>
                            {
                              bookingForm?.depart?.passengers.map((e: passengersSchema, i: number) => {
                                return (
                                  <>
                                    <tr>
                                      <td className="border-2 py-3 w-fit text-center">{i + 1}</td>
                                      <td className="border-2 p-3">{e.name}</td>
                                      <td className="border-2 py-3 text-center">{e.seat_number}</td>
                                    </tr>
                                  </>
                                )
                              })
                            }
                          </tbody>
                        </table>
                      </div>
                    </div>
                    {/* <div className="mt-3 border-t-2 border-dashed border-gray-300 pt-3">
                      <div className="border-b-[1px] border-gray-50 pb-2 flex lg:flex-row flex-col gap-1 lg:items-center justify-between text-sm">
                        {cookie.getToken() ?
                          <>
                            <div className="flex justify-start items-center gap-2 lg:self-center">
                              <Checkbox
                                id={'is-point-depart'}
                                className=''
                                checked={bookingForm?.depart?.is_point}
                                disabled={profile?.point! < checkoutResponse?.depart?.price! || !checkoutResponse?.depart?.price}
                                onCheckedChange={(e: boolean) => {
                                  if (bookingForm?.depart?.voucher_code) {
                                    toast({
                                      variant: "warning",
                                      title: "Peringatan!",
                                      description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                                    })
                                  } else {
                                    // logic point
                                    if (e) {
                                      setProfile((prev: any) => ({
                                        ...prev,
                                        point: profile?.point! - checkoutResponse?.depart?.price
                                      }));
                                    } else {
                                      setProfile((prev: any) => ({
                                        ...prev,
                                        point: profile?.point! + checkoutResponse?.depart?.price
                                      }));
                                    }

                                    setRoundTripFormData((prev: any) => ({
                                      ...prev,
                                      depart: {
                                        ...prev?.depart,
                                        is_point: e,
                                      },
                                    }));
                                    setCheckoutResponse((prev: any) => ({
                                      ...prev,
                                      depart: {
                                        ...prev?.depart,
                                        price: null,
                                        sub_price: null,
                                        point_used: null,
                                        voucher_price: null,
                                        total_price: null,
                                      },
                                      grand_total_price: null
                                    }));
                                  }
                                }}
                              />
                              <label htmlFor="is-point-depart" className="text-xs text-gray-600">
                                Gunakan (
                                <span className="text-primary font-bold italic">
                                  {profile?.point.toLocaleString('id-ID')}
                                </span>
                                ) Point
                              </label>
                            </div>
                          </>
                          :
                          <>
                            &nbsp;
                          </>
                        }
                        <div className="flex flex-row justify-between">
                          <div className="relative">
                            <input
                              type="text"
                              placeholder="Tambahkan Voucher"
                              className={`bg-gray-50 cursor-pointer text-right p-3 py-2 text-xs border-b-2 rounded-lg ${bookingForm?.depart?.voucher_code ? "pr-9" : ""}`}
                              value={bookingForm?.depart?.voucher_code}
                              onClick={() => {
                                if (bookingForm?.depart?.is_point === true) {
                                  toast({
                                    variant: "warning",
                                    title: "Peringatan!",
                                    description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                                  })
                                } else {
                                  setModalOpen(true);
                                  setModalUntuk('depart');
                                }
                              }}
                              readOnly
                            />
                            {bookingForm?.depart?.voucher_code && (
                              <FaTimes
                                className="absolute right-3 top-[9px] text-red-500 hover:font-bold duration-300 hover:bg-gray-300 cursor-pointer"
                                onClick={() => {
                                  setRoundTripFormData((prev: any) => ({
                                    ...prev,
                                    depart: {
                                      ...prev?.depart,
                                      voucher_code: '',
                                    },
                                  }));
                                }}
                              />
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between text-sm">
                        <span>Harga Tiket</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.depart?.price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Jumlah Penumpang</span>
                        <span>{"X " + checkoutResponse?.depart?.passengers?.length || "..."}</span>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Sub Total</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.depart?.sub_price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Diskon Voucher</span>
                        <div className="flex">
                          <span>-</span>
                          <div className="w-24 flex flex-row justify-between">
                            <span>Rp. </span>
                            <span>{checkoutResponse?.depart?.voucher_price?.toLocaleString('id-ID') || '...'}</span>
                          </div>
                        </div>
                      </div>
                      {bookingForm?.depart?.is_point && (
                        <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                          <span>Diskon Point</span>
                          <div className="flex">
                            <span>-</span>
                            <div className="w-24 flex flex-row justify-between">
                              <span>Rp. </span>
                              <span>{checkoutResponse?.depart?.point_used?.toLocaleString('id-ID') || '...'}</span>
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Harga Pergi</span>
                        <div className="w-24 flex flex-row justify-between font-bold text-red-500">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.depart?.total_price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                    </div> */}
                  </div>
                  <div className="bg-white border-2 p-5 w-full mx-auto">
                    <div className="flex flex-row md:justify-between justify-center items-center border-b-2 border-dashed border-gray-200 uppercase">
                      <h4 className="font-bold pb-3">Pulang</h4>
                    </div>
                    <div className="flex flex-col sm:flex-row gap-1 md:gap-0 items-start justify-between mt-5">
                      <span className="font-bold">{getPoint(checkoutResponse?.return?.route?.depart_code!) || "-"}</span>
                      <span className="font-bold">{getPoint(checkoutResponse?.return?.route?.arrival_code!) || "-"}</span>
                    </div>
                    <div className="flex flex-row items-start justify-between mt-1 text-sm">
                      <span>{moment(checkoutResponse?.return?.depart_date, 'YYYY-MM-DD').locale('id').format("dddd, DD MMMM YYYY") || "-"}</span>
                      <span>{checkoutResponse?.return?.etd + " WIB" || "-"}</span>
                    </div>
                    <div className="text-sm mt-7 pt-3 border-t-2 border-dashed">
                      <div className="overflow-x-auto">
                        <table className="border-[1px] p-3 mt-3 w-full rounded-lg">
                          <thead>
                            <tr>
                              <th className="border-2 py-3 w-12">No.</th>
                              <th className="border-2 p-3">Nama</th>
                              <th className="border-2 py-3 w-16">Kursi</th>
                            </tr>
                          </thead>
                          <tbody>
                            {
                              bookingForm?.return?.passengers.map((e: passengersSchema, i: number) => {
                                return (
                                  <>
                                    <tr>
                                      <td className="border-2 py-3 w-fit text-center">{i + 1}</td>
                                      <td className="border-2 p-3">{e.name}</td>
                                      <td className="border-2 py-3 text-center">{e.seat_number}</td>
                                    </tr>
                                  </>
                                )
                              })
                            }
                          </tbody>
                        </table>
                      </div>
                    </div>
                    {/* <div className="mt-3 border-t-2 border-dashed border-gray-300 pt-3">
                      <div className="border-b-[1px] border-gray-50 pb-2 flex lg:flex-row flex-col gap-1 lg:items-center justify-between text-sm">
                        {cookie.getToken() ?
                          <>
                            <div className="flex justify-start items-center gap-2 lg:self-center">
                              <Checkbox
                                id={'is-point-return'}
                                className=''
                                checked={bookingForm?.return?.is_point}
                                disabled={profile?.point! < checkoutResponse?.return?.price! || !checkoutResponse?.return?.price}
                                onCheckedChange={(e: boolean) => {
                                  if (bookingForm?.return?.voucher_code) {
                                    toast({
                                      variant: "warning",
                                      title: "Peringatan!",
                                      description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                                    })
                                  } else {
                                    // logic point
                                    if (e) {
                                      setProfile((prev: any) => ({
                                        ...prev,
                                        point: profile?.point! - checkoutResponse?.return?.price
                                      }));
                                    } else {
                                      setProfile((prev: any) => ({
                                        ...prev,
                                        point: profile?.point! + checkoutResponse?.return?.price
                                      }));
                                    }

                                    setRoundTripFormData((prev: any) => ({
                                      ...prev,
                                      return: {
                                        ...prev?.return,
                                        is_point: e,
                                      },
                                    }));
                                    setCheckoutResponse((prev: any) => ({
                                      ...prev,
                                      return: {
                                        ...prev?.return,
                                        price: null,
                                        sub_price: null,
                                        point_used: null,
                                        voucher_price: null,
                                        total_price: null,
                                      },
                                      grand_total_price: null
                                    }));
                                  }
                                }}
                              />
                              <label htmlFor="is-point-return" className="text-xs text-gray-600">
                                Gunakan (
                                <span className="text-primary font-bold italic">
                                  {profile?.point.toLocaleString('id-ID')}
                                </span>
                                ) Point
                              </label>
                            </div>
                          </>
                          :
                          <>
                            &nbsp;
                          </>
                        }
                        <div className="flex flex-row justify-between">
                          <div className="relative">
                            <input
                              type="text"
                              placeholder="Tambahkan Voucher"
                              className={`bg-gray-50 cursor-pointer text-right p-3 py-2 text-xs border-b-2 rounded-lg ${bookingForm?.return?.voucher_code ? "pr-9" : ""}`}
                              value={bookingForm?.return?.voucher_code}
                              onClick={() => {
                                if (bookingForm?.return?.is_point === true) {
                                  toast({
                                    variant: "warning",
                                    title: "Peringatan!",
                                    description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                                  })
                                } else {
                                  setModalOpen(true);
                                  setModalUntuk('return');
                                }
                              }}
                              readOnly
                            />
                            {bookingForm?.return?.voucher_code && (
                              <FaTimes
                                className="absolute right-3 top-[9px] text-red-500 hover:font-bold duration-300 hover:bg-gray-300 cursor-pointer"
                                onClick={() => {
                                  setRoundTripFormData((prev: any) => ({
                                    ...prev,
                                    return: {
                                      ...prev?.return,
                                      voucher_code: '',
                                    },
                                  }));
                                }}
                              />
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between text-sm">
                        <span>Harga Tiket</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.return?.price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Jumlah Penumpang</span>
                        <span>{"X " + checkoutResponse?.return?.passengers?.length || "..."}</span>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Sub Total</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.return?.sub_price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Diskon Voucher</span>
                        <div className="flex">
                          <span>-</span>
                          <div className="w-24 flex flex-row justify-between">
                            <span>Rp. </span>
                            <span>{checkoutResponse?.return?.voucher_price?.toLocaleString('id-ID') || '...'}</span>
                          </div>
                        </div>
                      </div>
                      {bookingForm?.return?.is_point && (
                        <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                          <span>Diskon Point</span>
                          <div className="flex">
                            <span>-</span>
                            <div className="w-24 flex flex-row justify-between">
                              <span>Rp. </span>
                              <span>{checkoutResponse?.return?.point_used?.toLocaleString('id-ID') || '...'}</span>
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                        <span>Harga Pergi</span>
                        <div className="w-24 flex flex-row justify-between font-bold text-red-500">
                          <span>Rp. </span>
                          <span>{checkoutResponse?.return?.total_price?.toLocaleString('id-ID') || '...'}</span>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </>
              ) :
                (
                  <>
                    <div className="bg-gray-50 animate-pulse shadow-lg p-5 w-full mx-auto min-h-[500px]"></div>
                    <div className="bg-gray-50 animate-pulse shadow-lg p-5 w-full mx-auto min-h-[500px]"></div>
                  </>
                )
            }
          </div>
          <div className="bg-white border-2 p-5 w-full mx-auto">
            <div className="border-b-[1px] border-gray-50 pb-2 flex lg:flex-row flex-col gap-1 lg:items-center justify-between text-sm">
              {cookie.getToken() ?
                <>
                  <div className="flex justify-start items-center gap-2 lg:self-center">
                    <Checkbox
                      id="is-point-depart"
                      className=""
                      checked={bookingForm?.is_point}
                      disabled={
                        profile?.point! < checkoutResponse?.depart?.sub_price! &&
                        profile?.point! < checkoutResponse?.return?.sub_price!
                      }
                      onCheckedChange={(e: boolean) => {
                        if (bookingForm?.voucher_code) {
                          toast({
                            variant: "warning",
                            title: "Peringatan!",
                            description: "Anda tidak bisa menggunakan point dan voucher secara bersamaan",
                          });
                          return;
                        }

                        // const departPrice = checkoutResponse?.depart?.price || 0;
                        // const returnPrice = checkoutResponse?.return?.price || 0;
                        // const totalPrice = departPrice + returnPrice;
                        // const availablePoints = profile?.point || 0;

                        // // Handle insufficient points case
                        // if (e && availablePoints < totalPrice) {
                        //   toast({
                        //     variant: "warning",
                        //     title: "Peringatan!",
                        //     description: "Poin Anda tidak mencukupi untuk membayar seluruh perjalanan",
                        //   });
                        //   return;
                        // }

                        // // Calculate points used or refunded
                        // const pointUsed = e
                        //   ? Math.min(totalPrice, availablePoints) // Use points if checked
                        //   : checkoutResponse?.depart?.point_used + checkoutResponse?.return?.point_used || 0; // Refund previously used points

                        // // Update the profile state
                        // setProfile((prev: any) => ({
                        //   ...prev,
                        //   point: e
                        //     ? availablePoints - pointUsed // Deduct points if checked
                        //     : availablePoints + pointUsed, // Refund points if unchecked
                        // }));


                        setRoundTripFormData((prev: any) => ({
                          ...prev,
                          depart: {
                            ...prev?.depart,
                            is_point: e,
                          },
                          return: {
                            ...prev?.return,
                            is_point: e,
                          },
                          is_point: e,
                        }));

                        setCheckoutResponse((prev: any) => ({
                          ...prev,
                          depart: {
                            ...prev?.depart,
                            price: null,
                            sub_price: null,
                            point_used: null,
                            voucher_price: null,
                            total_price: null,
                          },
                          return: {
                            ...prev?.return,
                            price: null,
                            sub_price: null,
                            point_used: null,
                            voucher_price: null,
                            total_price: null,
                          },
                          grand_total_price: null,
                          total_voucher_price: null,
                          total_sub_price: null,
                        }));
                      }}
                    />

                    <label htmlFor="is-point-depart" className="text-xs text-gray-600">
                      Gunakan (
                      <span className="text-primary font-bold italic">
                        {profile?.point.toLocaleString('id-ID')}
                      </span>
                      ) Point
                      {/* {
                        bookingForm?.is_point && (
                          <>
                            <span className="ml-2 font-bold text-primary">(- {checkoutResponse?.total_point_used?.toLocaleString('id-ID')})</span>
                          </>
                        )
                      } */}
                    </label>
                  </div>
                </>
                :
                <>
                  &nbsp;
                </>
              }
              <div className="flex flex-row justify-between">
                <div className="relative">
                  <input
                    type="text"
                    placeholder="Tambahkan Voucher"
                    className={`bg-gray-50 cursor-pointer text-right p-3 py-2 text-xs border-b-2 rounded-lg ${bookingForm?.voucher_code ? "pr-9" : ""}`}
                    value={bookingForm?.voucher_code}
                    onClick={() => {
                      if (bookingForm?.is_point === true) {
                        toast({
                          variant: "warning",
                          title: "Peringatan!",
                          description: 'Anda tidak bisa menggunakan point dan voucher secara bersamaan'
                        })
                      } else {
                        setModalOpen(true);
                        // setModalUntuk('depart');
                      }
                    }}
                    readOnly
                  />
                  {bookingForm?.voucher_code && (
                    <FaTimes
                      className="absolute right-3 top-[9px] text-red-500 hover:font-bold duration-300 hover:bg-gray-300 cursor-pointer"
                      onClick={() => {
                        setRoundTripFormData((prev: any) => ({
                          ...prev,
                          depart: {
                            ...prev?.depart,
                            voucher_code: '',
                          },
                          return: {
                            ...prev?.depart,
                            voucher_code: '',
                          },
                          voucher_code: '',
                        }));
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
              <span>Harga Pergi</span>
              <div className="w-24 flex flex-row justify-between">
                <span>Rp. </span>
                <span className="">{checkoutResponse?.depart?.sub_price?.toLocaleString('id-ID') || '...'}</span>
              </div>
            </div>
            <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
              <span>Harga Pulang</span>
              <div className="w-24 flex flex-row justify-between">
                <span>Rp. </span>
                <span className="">{checkoutResponse?.return?.sub_price?.toLocaleString('id-ID') || '...'}</span>
              </div>
            </div>
            <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
              <span>Sub Total</span>
              <div className="w-24 flex flex-row justify-between font-bold text-blue-500">
                <span>Rp. </span>
                <span className="">{checkoutResponse?.total_sub_price?.toLocaleString('id-ID') || '...'}</span>
              </div>
            </div>
            <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
              <span>Total Potongan</span>
              <div className="flex font-bold text-red-500">
                <span>-</span>
                <div className="w-24 flex flex-row justify-between">
                  <span>Rp. </span>
                  <span>{(checkoutResponse?.total_voucher_price !== 0 ? checkoutResponse?.total_voucher_price : checkoutResponse?.total_point_used)?.toLocaleString('id-ID') || '...'}</span>
                </div>
              </div>
            </div>
            <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
              <span>Total Harga</span>
              <div className="w-24 flex flex-row justify-between font-bold text-green-500">
                <span>Rp. </span>
                <span className="">{checkoutResponse?.grand_total_price?.toLocaleString('id-ID') || '...'}</span>
              </div>
            </div>
            <div className="flex flex-row gap-2 items-center mt-3">
              <Checkbox id={'agre-term'} className='' checked={agreTerm} disabled={false} onClick={() => { setAgreTerm(!agreTerm) }} />
              <label htmlFor="agre-term2" className="text-sm cursor-pointer hover:underline duration-200 hover:text-blue-500" title="Klik untuk membaca Syarat & Ketentuan" onClick={() => { setModalTermOpen(true) }}>Saya telah membaca Syarat & Ketentuan</label>
            </div>
            {
              isLoading ?
                <LoadingButton className="w-full mt-3" />
                :
                <Button
                  type="button"
                  variant="primary"
                  className="w-full mt-3"
                  onClick={book}
                  disabled={!agreTerm ||!checkoutResponse?.depart?.is_booking_continued||!checkoutResponse?.return?.is_booking_continued}
                >
                  Lanjutkan Pembayaran
                </Button>
            }
          </div>
        </div>
      </div>
    </>
  )
}