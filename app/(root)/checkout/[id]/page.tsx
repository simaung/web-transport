"use client"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { getItem, store, storeArray } from "@/lib/localStorage"
import config from "@/project.config"
import moment, { localeData } from "moment"
import { useEffect, useState } from "react"
import { FaEnvelope, FaPhoneAlt, FaUser } from "react-icons/fa"
import useSWR from "swr"
import { toast } from "@/components/ui/use-toast"
import SkeletonForm from "../../components/skeleton/skeletonForm"
import { groupPayment } from "@/lib/grouping"
import { useRouter } from "next/navigation"
import { POST } from "@/lib/http"
import LoadingButton from "@/components/LoadingButton"

interface bookingFormShcema {
  booking_id: string,
  transaction_status: string,
  payment_status: string,
  orderer_name: string,
  orderer_phone: string,
  orderer_email: string,
  pax: number,
  expiration_time: string,
  order_from: string,
  expiration_time_zone: string,
  depart_date: string,
  sub_price: number,
  point_price: number,
  voucher_price: number,
  total_price: number,
  route: {
    depart_code: string,
    depart_name: string,
    depart_address: string,
    arrival_code: string,
    arrival_name: string,
    arrival_address: string
  },
  payment_data: string,
  voucher_code: string,
  passengers: passengersSchema[],
  etd: string,
  eta: string
}

interface passengersSchema {
  name: string,
  phone: string,
  seat_number: string,
  selling_price: string
}

interface selectedPaymentSchema {
  booking_code: string,
  payment_method: string,
  payment_code: string
}

/* eslint-disable @next/next/no-img-element */
export default function Page({ params }: any) {
  const method = JSON.stringify({
    VA: "Virtual Account",
    QR: "Kode Qr",
    EWALLET: 'Dompet Online'
  })
  const [isLoading, setIsLoading] = useState(false);

  const router = useRouter();
  const { id } = params;
  const [selectedPayment, setSelectedPayment] = useState<selectedPaymentSchema>({
    booking_code: id,
    payment_method: '',
    payment_code: ''
  })
  const payment = useSWR(`${config.apiUrl}/v1/payment/${id}`)
  const { data, error } = useSWR<{ result: bookingFormShcema, message: string, status: string, status_code: number }>(`${config.apiUrl}/v1/booking/${id}`)

  // if (!data?.result) {
  //   toast({
  //     variant: "destructive",
  //     title: data?.status,
  //     description: data?.message,
  //   });
  // }

  if (payment?.data?.status === 'FAILED' && (data?.result?.order_from !== "whatsapp" && data?.result?.order_from !== "web" && data?.result?.order_from !== "mobile")) {
    // router.push("/")
    router.push('/reservation?kode=' + id)
    return
  }

  const bookingForm = data?.result && data.result;
  const paymentGroup = payment?.data?.payments && (groupPayment(payment?.data?.payments, "type"))

  async function CreatePayment() {
    setIsLoading(true);
    if (selectedPayment.payment_code === '') {
      toast({
        variant: "warning",
        title: "TIDAK LENGKAP",
        description: "mohon pilih terlebih dahulu metode pembayaran",
      });
      setIsLoading(false);
      return
    }
    const { data: res } = await POST('/v1/payment/create', selectedPayment);
    if (res.status !== 'SUCCESS') {
      toast({
        variant: "destructive",
        title: res?.status,
        description: res?.message,
      });
      router.push('/reservation?kode=' + id)
      return
    }
    toast({
      variant: "success",
      title: "Berhasil membuat pembayaran",
      description: "Silahkan ikuti langkah pembayaran berikut."
    });

    const { paymentUrl, qrString, vaNumber } = res?.payments;
    const { booking_code, payment_code } = selectedPayment;

    const localData = getItem('paymentHistory')
    if (!localData || localData.booking_code !== id) {
      store('paymentHistory', { booking_code: id, [selectedPayment.payment_code]: vaNumber, qrString: qrString, expiration_time: bookingForm?.expiration_time })
    } else {
      store('paymentHistory', { ...localData, [selectedPayment.payment_code]: !localData[selectedPayment.payment_code] ? vaNumber : localData[selectedPayment.payment_code], qrString: !localData.qrString ? qrString : localData.qrString })
    }
    const latestLocalData = getItem('paymentHistory');


    if (paymentUrl !== '') {
      window.open(paymentUrl, '_blank');
      router.push('/reservation?kode=' + id)
      return
    }

    if (qrString !== '') {
      // console.log(qrString, 'qrString');
      router.push(`/checkout/bayar?payment_code=${payment_code}&&qrString=${qrString}&&booking_code=${booking_code}&&type=trip`)
      return
    }

    if (vaNumber !== '') {
      // console.log(vaNumber, 'vaNumber');
      // router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${vaNumber}&&booking_code=${booking_code}&&type=trip`)
      router.push(`/checkout/bayar?payment_code=${payment_code}&&vaNumber=${latestLocalData[selectedPayment.payment_code]}&&booking_code=${booking_code}&&type=trip`)
      return
    }
  }
  return (
    <>
      <div className="container md:mt-7 mt-3">
        {bookingForm ?
          <>
            <div className="flex flex-col xl:flex-row gap-3">
              <div className="bg-white rounded-lg shadow-lg p-5 w-full xl:max-w-lg">
                <div className="flex flex-row justify-center sm:justify-between items-center border-b-2 border-dashed border-gray-200 uppercase pb-3 sm:pb-0">
                  <h4 className="font-bold py-3 sm:block hidden">Detail Pesanan</h4>
                  <img src="/images/logo/logo.png" alt="" className="w-36" />
                </div>
                <div className="flex flex-row justify-start sm:justify-between mt-3 items-center">
                  <span className="sm:block hidden">
                    Kode Booking
                  </span>
                  <span className="font-bold">
                    {bookingForm?.booking_id}
                  </span>
                </div>
                <div className="flex flex-col sm:flex-row items-start justify-between mt-3 gap-3">
                  <span className="font-bold">{bookingForm?.route?.depart_name || "-"}</span>
                  {/* <span className="font-bold sm:block hidden">-</span> */}
                  <span className="font-bold">{bookingForm?.route?.arrival_name || "-"}</span>
                </div>
                <div className="flex flex-col md:flex-row items-start justify-between mt-1 text-sm">
                  <span>{moment(bookingForm?.depart_date, 'YYYY-MM-DD').locale('id').format("dddd, DD MMMM YYYY") || "-"}</span>
                  <span>{moment(bookingForm?.etd, 'HH:mm:ss').format("HH:mm") + " WIB" || "-"}</span>
                </div>
                <div className="text-sm mt-3 pt-3 border-t-2 border-dashed">
                  <span className="uppercase font-bold md:block hidden">
                    Data Pemesan
                  </span>
                  <div className="flex flex-col md:flex-row gap-1 md:mt-1 justify-between md:items-center">
                    <div>
                      <span className="text-xs md:block hidden">
                        Nama Pemesan
                      </span>
                      {/* <br /> */}
                      <span className="text-base font-bold flex flex-row items-center gap-3">
                        <FaUser />
                        <span>
                          {bookingForm?.orderer_name}
                        </span>
                      </span>
                    </div>
                    <div className="flex flex-col gap-1">
                      <span className="flex items-center gap-3"><FaPhoneAlt />{bookingForm?.orderer_phone}</span>
                      <span className="flex items-center gap-3"><FaEnvelope />{bookingForm?.orderer_email}</span>
                    </div>
                  </div>
                  <div className="overflow-x-auto">
                    <table className="border-[1px] p-3 mt-3 w-full rounded-lg">
                      <thead>
                        <tr>
                          <th className="border-2 py-3 w-12">No.</th>
                          <th className="border-2 p-3">Nama</th>
                          {/* <th className="border-2 p-3">Telepon</th> */}
                          {/* <th className="border-2 py-3">Harga</th> */}
                          <th className="border-2 py-3 w-16">Kursi</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          bookingForm?.passengers?.map((e: passengersSchema, i: number) => {
                            return (
                              <tr key={"paxcheckout" + i}>
                                <td className="border-2 p-3 w-fit text-center">{i + 1}</td>
                                <td className="border-2 p-3 text-nowrap">{e.name}</td>
                                {/* <td className="border-2 p-3">{e.phone}</td> */}
                                {/* <td className="border-2 p-3 text-center text-nowrap">Rp. {e.selling_price}</td> */}
                                <td className="border-2 p-3 text-center">{e.seat_number}</td>
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="mt-3 border-y-2 border-dashed border-gray-300 py-3">
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between text-sm mb-2">
                    <span>&nbsp;</span>
                    <div className="w-56 text-right font-bold border-b-2 max-w-32">
                      {bookingForm?.voucher_code || <><span className="italic font-thin text-gray-300">Tanpa Voucher</span></>}
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Sub Total</span>
                    <div className="w-24 flex flex-row justify-between">
                      <span>Rp. </span>
                      <span>{bookingForm?.sub_price?.toLocaleString('id-ID') || "api"}</span>
                    </div>
                  </div>
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Diskon Voucher</span>
                    <div className="flex">
                      <span>-</span>
                      <div className="w-24 flex flex-row justify-between">
                        <span>Rp. </span>
                        <span>{bookingForm?.voucher_price?.toLocaleString('id-ID') || "api"}</span>
                      </div>
                    </div>
                  </div>
                  {bookingForm?.point_price !== 0 && (
                    <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                      <span>Diskon Point</span>
                      <div className="flex">
                        <span>-</span>
                        <div className="w-24 flex flex-row justify-between">
                          <span>Rp. </span>
                          <span>{bookingForm?.point_price?.toLocaleString('id-ID') || "api"}</span>
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="border-b-[1px] border-gray-50 pb-1 flex flex-row items-start justify-between mt-1 text-sm">
                    <span>Total Harga</span>
                    <div className="w-24 flex flex-row justify-between font-bold text-red-500">
                      <span>Rp. </span>
                      <span className="">{bookingForm?.total_price?.toLocaleString('id-ID')}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-white rounded-lg shadow-lg p-5 w-full flex flex-col justify-between gap-3">
                <div className="flex flex-row justify-between items-center border-b-2 border-dashed border-gray-200 uppercase">
                  <h4 className="font-bold py-3">PEMBAYARAN</h4>
                </div>
                <div className="h-96 py-3 overflow-y-auto">
                  {payment.isLoading ?
                    (
                      <>
                        Loading
                      </>
                    )
                    :
                    <>
                      {payment?.data?.payments?.length === 0 ?
                        <>
                          Payment tidak di temukan
                        </>
                        :
                        <>
                          <div className="flex flex-col gap-7 ">
                            {Object?.keys(paymentGroup)?.map(u => (
                              <>
                                <div className="flex flex-col gap-3" key={u + "paymentgroup"}>
                                  <span className="font-bold w-full border-b-2 pb-2">{JSON.parse(method)[u]}</span>
                                  {paymentGroup[u].map((s: { name: string, code: string, image: string, type: string, status: string }) => (
                                    <>
                                      <div
                                        className={`w-full ${s.status === 'active' ? "bg-white hover:border-primary cursor-pointer" : "bg-gray-100 cursor-not-allowed text-gray-400"} p-3 rounded-lg border-2 duration-300 flex flex-row items-center justify-between ${s.code === selectedPayment.payment_code ? "border-primary" : ""}`}
                                        key={s.name + "paymentgroup2"}
                                        onClick={() => {
                                          if (s.status === 'active') {
                                            setSelectedPayment({ ...selectedPayment, payment_code: s.code, payment_method: s.type })
                                          }
                                        }}
                                      >
                                        <div className="flex flex-col">
                                          <span>
                                            {s.name}
                                          </span>
                                          <span className="text-[9px] italic text-red-300">
                                            {s.status !== 'active' && ('Minimal transaksi Rp. 50.000')}
                                          </span>
                                        </div>
                                        <img src={s.image} alt="..." className={`h-8 ${s.status === 'active' ? "opacity-100" : "opacity-45"}`} />
                                      </div>
                                    </>
                                  ))}
                                </div>
                              </>
                            ))}
                          </div>
                        </>
                      }
                    </>
                  }
                </div>
                {
                  isLoading ?
                    <LoadingButton className="w-full" />
                    :
                    <Button type="button" variant="primary" className="w-full" onClick={CreatePayment}>Lanjutkan</Button>
                }
              </div>
            </div>
          </>
          :
          <div className="h-96 mt-[3%] bg-gray-100 w-full rounded-lg animate-pulse"></div>
        }
      </div >
    </>
  )
}