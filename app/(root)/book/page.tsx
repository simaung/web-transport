/* eslint-disable react-hooks/exhaustive-deps */
"use client"
import config from '@/project.config';
import { useSearchParams, useRouter } from 'next/navigation';
import useSWR from 'swr';
import SeatMap from '../components/feature/seatMap';
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@/components/ui/accordion';
import { FaArrowRight, FaCarSide, FaSearch, FaUser } from 'react-icons/fa';
import { MessageConstant as m } from '@/constants';
import moment from 'moment';
import 'moment/locale/id';
import { z } from "zod"
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { Input } from '@/components/ui/input';
import { zodResolver } from "@hookform/resolvers/zod"
import { Button } from '@/components/ui/button';
import { Checkbox } from '@/components/ui/checkbox';
import { useState, useEffect } from 'react';
import { useForm, UseFormProps, useFieldArray } from "react-hook-form";
import { store, storeArray, getItem, removeItem } from '@/lib/localStorage';
import * as EmailValidator from 'email-validator';
import Seat from '@/components/ui/seat';
import { toast } from "@/components/ui/use-toast"
import { POST, GET } from '@/lib/http';
import Link from 'next/link';
import cookie from '@/lib/cookies';
import LoadingButton from '@/components/LoadingButton';
import GoogleEmail from '../components/googleEmail';

const bookingFormSchema = z.object({
	tripcode: z.string().min(1, { message: 'Trip code tidak sesuai' }),
	departure_date: z.string().min(1, { message: 'Departure date tidak sesuai' }),
	departure_time: z.string().min(1, { message: 'Departure time tidak sesuai' }),
	pickup_point: z.string().min(1, { message: 'Pickup point tidak sesuai' }),
	dropoff_point: z.string().min(1, { message: 'Dropoff tidak sesuai' }),
	booking_code: z.string().optional(),
	booking_contact: z.object({
		name: z.string().min(1, { message: "Mohon masukan nama pemesan" }),
		phone: z.string().min(1, { message: "Nomor Telepon harus lebih dari 9 angka" }),
		// email: z.string().min(1, { message: "Mohon isi email yang masih aktif" }),
		email: z.string().min(1, { message: "Email harus di isi" }).email("Email tidak benar"),
	}),
	passengers: z.array(
		z.object(
			{
				name: z.string().min(1, { message: "Mohon masukan nama penumpang" }),
				phone: z.string(),
				seat_number: z.string()
			}
		)
	),
	return_passengers: z.array(
		z.object(
			{
				name: z.string(),
				phone: z.string(),
				seat_number: z.string()
			}
		)
	).optional(),
	is_point: z.boolean(),
	return_tripcode: z.string().optional(),
	return_date: z.string().optional(),
	return_time: z.string().optional()
});

interface responseProfileSchema {
	"name": string,
	"email": string,
	"phone": string,
	"point": number,
	"type": string,
	"avatar": string
}

type Passengers = z.infer<typeof bookingFormSchema>["passengers"][number];
const passengersInitial: Passengers[] = [
	// { name: "", phone: "" }
];
interface selectSeatSchema {
	id_seat: string;
	seat_number: string;
}
export default function Page() {
	const [isEmailOpen, setisEmailOpen] = useState(false);

	const bookingForm = getItem('bookingForm')
	const [isLoadingButton, setIsLoadingButton] = useState(false);
	const [profile, setProfile] = useState<responseProfileSchema>();
	const [stepSelectSeat, setStepSelectSeat] = useState(false);
	const searchParams = useSearchParams();
	const Router = useRouter();
	const dataQuery = {
		tripcode: searchParams.get("tripcode") || '',
		depart: searchParams.get("depart") || '',
		arrival: searchParams.get("arrival") || '',
		date: searchParams.get("date") || '',
		time: searchParams.get("time") || '',
		return_date: searchParams.get("return-date") || '',
		return_time: searchParams.get("return-time") || '',
		return_tripcode: searchParams.get("return-tripcode") || '',
		pax: Number(searchParams.get("pax")),
		booking_code: searchParams.get("booking_code"),
	}
	// Validation checks
	const validation = []
	if (!dataQuery.tripcode) {
		validation.push("Tripcode is required");
	}
	if (!dataQuery.depart) {
		validation.push("Departure location is required");
	}
	if (!dataQuery.arrival) {
		validation.push("Arrival location is required");
	}
	if (!dataQuery.date) {
		validation.push("Date is required");
	}
	if (!dataQuery.time) {
		validation.push("Time is required");
	}
	if (dataQuery?.return_date) {
		if (!dataQuery?.return_time) {
			validation.push("Return time is required");
		}
		if (!dataQuery?.return_tripcode) {
			validation.push("Return tripcode is required");
		}
	}
	if (isNaN(dataQuery.pax) || dataQuery.pax <= 0) {
		validation.push("Passenger count must be a valid number greater than 0");
	}

	const [selectSeat, setSelectSeat] = useState<selectSeatSchema[]>([])
	const [selectSeatReturn, setSelectSeatReturn] = useState<selectSeatSchema[]>([])
	const [withoutLogin, setWithoutLogin] = useState(false)
	const [sameAllData, setSameAllData] = useState(false)
	const form = useForm<z.infer<typeof bookingFormSchema>>({
		resolver: zodResolver(bookingFormSchema),
		defaultValues: {
			"tripcode": dataQuery.tripcode,
			"departure_date": dataQuery.date,
			"departure_time": dataQuery.time,
			"pickup_point": dataQuery.depart,
			"dropoff_point": dataQuery.arrival,
			"booking_contact": {
				"name": bookingForm?.booking_contact?.name || "",
				"phone": bookingForm?.booking_contact?.phone || "",
				"email": bookingForm?.booking_contact?.email || ""
			},
			"passengers": passengersInitial,
			"return_passengers": passengersInitial,
			"is_point": false
		},
	})
	const { register, handleSubmit, setValue, watch, getValues } = form;
	const { fields, append, remove } = useFieldArray({
		name: "passengers",
		control: form.control
	});
	const { fields: returnFields, append: returnAppend, remove: returnRemove } = useFieldArray({
		name: "return_passengers",
		control: form.control
	});
	function onSubmit(values: z.infer<typeof bookingFormSchema>) {
		// tambah validasi untuk kursi pulang jika di perlukan
		// console.log(values);
		// return

		const { booking_contact, passengers, return_passengers } = values;
		const isEmailValid = EmailValidator.validate(booking_contact.email);
		if (!isEmailValid) {
			toast({
				variant: "warning",
				title: "Email tidak benar",
				description: "mohon masukan email seperti contohemail@gmail.com"
			});
			// alert('Email tidak benar, mohon masukan email seperti "contohemail@gmail.com"')
			return
		}

		const validateKursi = [];
		passengers.forEach(e => {
			if (e.name === "") {
				// alert('Mohon semua nama penumpang')
				toast({
					variant: "warning",
					title: "Nama Penumpang Kosong",
					description: "Mohon masukan nama penumpang"
				});
				return
			}
			if (e.seat_number === "") {
				validateKursi.push(1)
			}
		});
		if (dataQuery?.return_date) {
			return_passengers?.forEach(e => {
				if (e.name === "") {
					// alert('Mohon semua nama penumpang')
					toast({
						variant: "warning",
						title: "Nama Penumpang Kosong",
						description: "Mohon masukan nama penumpang"
					});
					return
				}
				if (e.seat_number === "") {
					validateKursi.push(1)
				}
			});
		}
		if (validateKursi.length !== 0 && stepSelectSeat) {
			// alert("anda perlu memilih " + validateKursi.length + " Kursi")
			toast({
				variant: "warning",
				title: "Kursi tidak sesuai",
				description: "Silahkan pilih " + validateKursi.length + " kursi lagi"
			});
			return
		}
		// tambahkan validation untuk nama dan kursi yang di pilih sesuai dengan dataQuery.pax

		const dataToStore = {
			...values,
			pickup_name: getPoint(dataQuery?.depart),
			dropoff_name: getPoint(dataQuery?.arrival),
		}
		if (!dataQuery?.return_date) {
			delete dataToStore.return_passengers;
		} else if (dataQuery?.return_date) {
			dataToStore.return_tripcode = dataQuery?.return_tripcode;
			dataToStore.return_date = dataQuery?.return_date;
			dataToStore.return_time = dataQuery?.return_time;
		}

		store('bookingForm', dataToStore);
		if (stepSelectSeat) {
			if (dataQuery?.return_tripcode) {
				setIsLoadingButton(true);
				Router.push('/checkout/roundtrip')
			} else {
				setIsLoadingButton(true);
				Router.push('/checkout')
			}
		}
		setStepSelectSeat(true)
	}
	const { data: dataPoints } = useSWR(`${config.apiUrl}/v1/points`)
	var getPoint = (id: string) => {
		return dataPoints.points.find((c: any) => c.id === id)?.name
	}
	const paramQuery = `pickup_point=${dataQuery.depart}&dropoff_point=${dataQuery.arrival}&departure_date=${dataQuery.date}&tripcode=${dataQuery.tripcode}`
	// const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/seat?${paramQuery}`)
	const [data, setData] = useState();
	const [returnData, setReturnData] = useState();
	const { data: dataDetail, error: errorDetail, isLoading: loadingDetail } = useSWR(`${config.apiUrl}/v1/course?tripcode=${dataQuery.tripcode}`)
	const { data: return_dataDetail, error: return_errorDetail, isLoading: return_loadingDetail } = useSWR(`${config.apiUrl}/v1/course?tripcode=${dataQuery?.return_tripcode || dataQuery.tripcode}`)
	async function getSeat() {
		const res = await POST('/v1/seatmap', {
			pickup_point: dataQuery.depart,
			dropoff_point: dataQuery.arrival,
			departure_date: dataQuery.date,
			tripcode: dataQuery.tripcode
		})
		if (res.data) {
			setData(res.data);
		}

	}
	async function getRetunSeat() {
		const res = await POST('/v1/seatmap', {
			pickup_point: dataQuery.arrival,
			dropoff_point: dataQuery.depart,
			departure_date: dataQuery.return_date,
			tripcode: dataQuery.return_tripcode
		})
		if (res.data) {
			setReturnData(res.data);
		}
	}
	const bookingContact = watch('booking_contact');
	useEffect(() => {
		if (bookingContact?.name !== '') {
			if (sameAllData) {
				fields.map((e, x) => {
					setValue(`passengers.${x}`, { name: bookingContact?.name, phone: bookingContact.phone, seat_number: '' });
				})
				returnFields.map((e, x) => {
					setValue(`return_passengers.${x}`, { name: bookingContact?.name, phone: bookingContact.phone, seat_number: '' });
				})
			}
			if (!sameAllData) {
				fields.map((e, x) => {
					setValue(`passengers.${x}`, { name: "", phone: "", seat_number: '' });
				})
				returnFields.map((e, x) => {
					setValue(`return_passengers.${x}`, { name: "", phone: "", seat_number: '' });
				})
			}
		} else {
			setSameAllData(false)
		}
	}, [sameAllData]);
	async function getProfile() {
		const { data } = await GET(config.apiUrl + "/v1/account/profile", {});
		// console.log(data);
		if (data.status === "SUCCESS") {
			setProfile(data.data)
			setValue('booking_contact.name', data.data.name)
			setValue('booking_contact.email', data.data.email)
			setValue('booking_contact.phone', data.data.phone.replace('+62', '0'))
		}
	}
	useEffect(() => {
		for (let i = 0; i < dataQuery.pax; i++) {
			append({ name: '', phone: '', seat_number: '' })
			returnAppend({ name: '', phone: '', seat_number: '' })
		}
		getSeat();
		if (dataQuery?.return_date) {
			getRetunSeat();
		}
		if (cookie?.getToken()) {
			getProfile();
		}
		if (dataQuery.booking_code) {
			setValue('booking_code', dataQuery.booking_code);
		}
		removeItem('bookingForm');
		return
	}, []);
	// useEffect(() => {
	//     console.log(form.watch('passengers'));
	// }, [sameAllData]);
	if (validation.length !== 0) {
		toast({
			variant: 'warning',
			title: "DATA TIDAK LENGKAP",
			description: validation.join(', ')
		})
		setTimeout(() => {
			Router.push('/')
		}, 1000);
		return
	}
	return (
		<div className='container mt-[3%]'>
			<div className="flex flex-col lg:flex-row gap-3 w-full">
				<div className='border-solid border-2 border-gray-300 p-4 lg:max-w-md w-full'>
					<div className='text-xl lg:text-2xl font-bold uppercase border-b-2 pb-2 mb-3'>Detail Perjalanan</div>
					<div>
						{(dataPoints && dataDetail && return_dataDetail) && (
							<>
								{dataQuery?.return_date && (
									<div className="text-lg font-semibold">Berangkat</div>
								)}
								<div className='font-bold text-primary'>
									<div>
										{getPoint(dataQuery.depart)}
										<span> → </span>
										{getPoint(dataQuery.arrival)}
									</div>
									<div className='text-gray-800'>
										{moment(dataQuery.date, 'YYYY-MM-DD').format('dddd, DD MMMM YYYY')}
										<br />
										{moment(dataQuery.time, 'HH:mm:ss').format('HH:mm')} WIB
										<br />
										{dataQuery.pax} Penumpang
									</div>
								</div>
								<Accordion
									type="single"
									collapsible className="w-full"
									defaultValue={window && (window?.screen?.width >= 768 ? "item-1" : "")}
								// defaultValue={"item-1"}
								>
									<AccordionItem value="item-1">
										<AccordionTrigger>Rute Perjalanan</AccordionTrigger>
										<AccordionContent>
											{dataDetail && (
												<>
													{dataDetail.course.map((trip: any, i: number) => (
														<div key={'book' + i}>
															<div className='flex flex-wrap my-1 justify-start'>
																<div className='bg-secondary p-3 rounded-full w-10 h-10 text-white'>
																	<FaCarSide size={15} />
																</div>
																<div className={`${trip.point_id == dataQuery.depart || trip.point_id == dataQuery.arrival ? 'font-bold text-primary' : ''} ml-2`}>
																	<p>{trip.point_name}</p>
																	<p>{trip.departure_hour} WIB</p>
																</div>
															</div>
															<div className={` ${dataDetail.course.length - i > 1 ? 'border-l-2 border-gray-300 min-h-6 ml-5' : ''}`}></div>
														</div>
													))}
												</>
											)}
										</AccordionContent>
									</AccordionItem>
								</Accordion>
								{dataQuery?.return_date && (
									<>
										<div className="text-lg font-semibold mt-2">Pulang</div>
										<div className='font-bold text-primary'>
											<div>
												{getPoint(dataQuery.arrival)}
												<span> → </span>
												{getPoint(dataQuery.depart)}
											</div>
											<div className='text-gray-800'>
												{moment(dataQuery.return_date, 'YYYY-MM-DD').format('dddd, DD MMMM YYYY')}
												<br />
												{moment(dataQuery.return_time, 'HH:mm:ss').format('HH:mm')} WIB
												<br />
												{dataQuery.pax} Penumpang
											</div>
										</div>
										<Accordion
											type="single"
											collapsible className="w-full"
											defaultValue={window && (window?.screen?.width >= 768 ? "item-2" : "")}
										// defaultValue={"item-2"}
										>
											<AccordionItem value="item-2">
												<AccordionTrigger>Rute Perjalanan</AccordionTrigger>
												<AccordionContent>
													{return_dataDetail && (
														<>
															{return_dataDetail.course.map((trip: any, i: number) => (
																<div key={'book' + i}>
																	<div className='flex flex-wrap my-1 justify-start'>
																		<div className='bg-secondary p-3 rounded-full w-10 h-10 text-white'>
																			<FaCarSide size={15} />
																		</div>
																		<div className={`${trip.point_id == dataQuery.depart || trip.point_id == dataQuery.arrival ? 'font-bold text-primary' : ''} ml-2`}>
																			<p>{trip.point_name}</p>
																			<p>{trip.departure_hour} WIB</p>
																		</div>
																	</div>
																	<div className={` ${dataDetail.course.length - i > 1 ? 'border-l-2 border-gray-300 min-h-6 ml-5' : ''}`}></div>
																</div>
															))}
														</>
													)}
												</AccordionContent>
											</AccordionItem>
										</Accordion>
									</>
								)}
							</>
						)}
					</div>
				</div>
				<div className='border-solid p-4 border-2 border-gray-300 w-full h-fit'>
					{data ?
						<>
							{dataQuery.pax < (Number(config.maxPax) + 1) ?
								(
									<>
										{dataQuery.pax < 1 ?
											(
												<div className='md:col-span-2 text-justify pt-4 text-red-500'>
													{m.MINPAX}
												</div>
											) :
											(
												<>
													<Form {...form}>
														<form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-2 justify-start w-full h-fit">
															{
																!stepSelectSeat ?
																	<div id='form-penumpang'>
																		<div className='text-xl lg:text-2xl font-bold border-b-2 mb-3 uppercase pb-2'>Data Pemesan</div>
																		<FormField
																			control={form.control}
																			name="booking_contact.name"
																			render={({ field }) => (
																				<FormItem>
																					<FormControl>
																						<Input placeholder={`Nama`} {...field} className="mb-3 w-full" disabled={profile?.name ? true : false} />
																					</FormControl>
																					<FormMessage className="text-xs" />
																				</FormItem>
																			)}
																		/>
																		<FormField
																			control={form.control}
																			name="booking_contact.phone"
																			render={({ field }) => (
																				<FormItem>
																					<FormControl>
																						<Input placeholder={`Nomor Telepon`} type='number' {...field} className="mb-3" disabled={profile?.phone ? true : false} />
																					</FormControl>
																					<FormMessage className="text-xs" />
																				</FormItem>
																			)}
																		/>
																		<FormField
																			control={form.control}
																			name="booking_contact.email"
																			render={({ field }) => (
																				<FormItem>
																					<FormControl>
																						<Input placeholder={`Email`} {...field} className="mb-3" disabled={profile?.email ? true : false} />
																						{/* <Input placeholder={`Email`} {...field} className="mb-3" disabled={true} readOnly /> */}
																						{/* <GoogleEmail setIsOpen={setisEmailOpen} setValue={setValue} value={watch('booking_contact.email')} /> */}
																					</FormControl>
																					<FormMessage className="text-xs" />
																				</FormItem>
																			)}
																		/>
																		<div className="flex flex-row gap-3 items-center">
																			<Checkbox id={'sameAllData'} className='' checked={sameAllData} disabled={false} onCheckedChange={(e: boolean) => {
																				setSameAllData(e)
																			}} />
																			<label htmlFor="sameAllData" className='text-nowrap'>Pemesan adalah Penumpang</label>
																		</div>
																		{fields.length > 0 ?
																			<>
																				<div className='text-xl lg:text-2xl font-bold border-b-2 mb-3 pb-2 mt-5 lg:mt-2 uppercase'>Data Penumpang</div>
																				<div className="grid grid-cols-1 md:grid-cols-2 gap-1 md:gap-3">
																					{fields.map((z, index) => {
																						return (
																							<>
																								<FormField
																									control={form.control}
																									name={`passengers.${index}.name`}
																									defaultValue={z.name}
																									render={({ field }) => (
																										<FormItem>
																											<FormControl>
																												<Input placeholder={`Nama Penumpang ${index + 1}`} {...field} className="mb-3" />
																											</FormControl>
																											<FormMessage className="text-xs" />
																										</FormItem>
																									)}
																								/>
																							</>
																						);
																					})}
																				</div>
																			</>
																			:
																			<>
																			</>
																		}
																		{!cookie.getToken() && (
																			<div className="flex flex-row gap-3 items-center justify-start">
																				<Checkbox id={'withLogin'} className='' checked={withoutLogin} disabled={false} onCheckedChange={(e: boolean) => {
																					setWithoutLogin(e)
																				}} />
																				<label htmlFor="withLogin">Tanpa Login</label>
																			</div>
																		)}
																	</div>
																	:
																	<div id='form-kursi'>
																		<div className='text-xl lg:text-2xl font-bold border-b-2 mb-1 uppercase pb-2'>Data Kursi</div>
																		<div className={`flex flex-col ${!dataQuery?.return_date ? "xl:flex-row" : ""} gap-3`}>
																			<div className={`w-full ${dataQuery?.return_date ? "xl:flex-row flex-col flex gap-5 w-full justify-center" : "xl:max-w-sm"}`}>
																				{/* jadikan returnData dan seatmap jadi ada lagi */}
																				{data && (
																					<>
																						<div>
																							{dataQuery?.return_date && (
																								<span className="font-semibold">Kursi Berangkat</span>
																							)}
																							<SeatMap
																								data={data}
																								selectSeat={selectSeat}
																								setSelectSeat={setSelectSeat}
																								// append={append}
																								// remove={remove}
																								setValue={setValue}
																								fields={watch}
																								untuk="passengers"
																							/>
																						</div>
																						{dataQuery?.return_date && (
																							<>
																								<div>
																									<span className="font-semibold">Kursi Pulang</span>
																									<SeatMap
																										data={returnData}
																										selectSeat={selectSeatReturn}
																										setSelectSeat={setSelectSeatReturn}
																										// append={append}
																										// remove={remove}
																										setValue={setValue}
																										fields={watch}
																										untuk="return_passengers"
																									/>
																								</div>
																							</>
																						)}
																					</>
																				)}
																			</div>
																			<div>
																				<h3 className="font-bold text-lg mb-3 w-full xl:text-center uppercase">informasi kursi</h3>
																				<div className="flex flex-row justify-center items-center gap-3 mb-5">
																					<div className="flex flex-col gap-1 items-center justify-center font-semibold w-24 p-3 border-2 rounded-lg text-sm">
																						<Seat state={'active'} label={""} />
																						<span>Dipilih</span>
																					</div>
																					<div className="flex flex-col gap-1 items-center justify-center font-semibold w-24 p-3 border-2 rounded-lg text-sm">
																						<Seat state={'available'} label={""} />
																						<span>Tersedia</span>
																					</div>
																					<div className="flex flex-col gap-1 items-center justify-center font-semibold w-24 p-3 border-2 rounded-lg text-sm">
																						<Seat state={'booked'} label={""} />
																						<span>Terjual</span>
																					</div>
																				</div>
																				<p className='text-sm'>
																					Catatan :
																					<ul className='space-y-3 pl-5'>
																						<li>
																							Kursi sewaktu-waktu dapat dipesan oleh pengguna lain yang terlebih dahulu menyelesaikan pembeliannya.
																						</li>
																						<li>
																							Penumpang harap datang di outlet 15 menit sebelum keberangkatan untuk mengkonfirmasi keberangkatan. <span className="text-red-500 italic">Jika anda terlambat maka tiket akan hangus dan tidak bisa di gunakan</span>
																						</li>
																					</ul>
																				</p>
																			</div>
																		</div>
																	</div>
															}
															{withoutLogin || cookie.getToken() ?
																<>
																	{isLoadingButton ?
																		<LoadingButton className='w-full' />
																		:
																		<Button
																			variant="primary"
																			type="submit"
																			className="gap-2"
																		>
																			<FaArrowRight />{stepSelectSeat ? "Pesan" : 'Lanjutkan'}
																		</Button>
																	}
																</>
																:
																<>
																	<Link href={'/auth/login?page=book'}>
																		<Button variant="primary" type="button" className="gap-2 w-full"><FaUser />Login</Button>
																	</Link>
																</>
															}
														</form>
													</Form>
												</>
											)}
									</>
								)
								:
								(
									<div className='md:col-span-2 text-justify pt-4 text-red-500'>
										{m.MAXPAX}
										{/* {m.MINPAX} */}
										&nbsp;
										<a href={`https://api.whatsapp.com/send/?phone=${config.contactService?.replaceAll(' ', "")?.replaceAll('-', "")?.replaceAll('+', '')}&text=Saya ingin pesan kursi lebih dari ${config.maxPax} &type=phone_number&app_absent=0`} aria-label="Our phone" title="Our phone" className="hover:font-bold duration-300">
											{config.contactService}
										</a>
									</div>
								)
							}
						</>
						:
						<>
							<div className="h-96 bg-gray-100 animate-pulse rounded-lg shadow-lg"></div>
						</>
					}
				</div>
			</div>
		</div >
	)
}
