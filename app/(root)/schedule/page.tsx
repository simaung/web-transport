'use client'

import config from '@/project.config';
import { ScheduleType } from '@/types/outlet';
import { useRouter, useSearchParams } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import useSWR from 'swr';
import SkeletonCard from '../components/skeleton/skeletonCard';
import ReservationFormSide from '../components/feature/reservationFormSide';
import DataNotFound from '../components/dataNotFound';
import { MessageConstant as m } from '@/constants';
import moment from 'moment';
import { timeDuration } from '@/lib/timeDuration';
import { POST } from '@/lib/http';
import ListSchedule from '../components/feature/shcedule/listSchedule';

interface scheduleSchema {
    "status": string,
    "status_code": number,
    "inventories": []
}

export default function Schedule() {
    const searchParams = useSearchParams();
    const router = useRouter()
    const [isLoadingButton, setisLoadingButton] = useState(false);
    const [dataQuery, setDataQuery] = useState({
        time: searchParams.get("time")!,
        tripcode: searchParams.get("tripcode")!,
        pickup_point: searchParams.get("depart")!,
        dropoff_point: searchParams.get("arrival")!,
        departure_date: searchParams.get("date")!,
        return_date: searchParams.get("return-date")!,
        return_time: searchParams.get("return-time")!,
        return_tripcode: searchParams.get("return-tripcode")!,
        pax: Number(searchParams.get("pax")!),
        booking_code:searchParams.get('booking_code')
    });

    useEffect(() => {
        const newQuery = {
            time: searchParams.get("time")!,
            tripcode: searchParams.get("tripcode")!,
            pickup_point: searchParams.get("depart")!,
            dropoff_point: searchParams.get("arrival")!,
            departure_date: searchParams.get("date")!,
            return_date: searchParams.get("return-date")!,
            return_time: searchParams.get("return-time")!,
            return_tripcode: searchParams.get("return-tripcode")!,
            pax: Number(searchParams.get("pax")!),
            booking_code:searchParams.get("booking_code")
        };
        setDataQuery(newQuery);
    }, [searchParams]);
    // const dataQuery = {
    //     pickup_point: searchParams.get("depart"),
    //     dropoff_point: searchParams.get("arrival"),
    //     departure_date: searchParams.get("date"),
    //     pax: Number(searchParams.get("pax")),
    // }

    // const paramQuery = `pickup_point=${dataQuery.depart}&dropoff_point=${dataQuery.arrival}&departure_date=${dataQuery.date}`

    // const { data, error, isLoading } = useSWR(`${config.apiUrl}/v1/schedule?${paramQuery}`)

    const [data, setData] = useState<scheduleSchema>();
    const [isLoading, setIsLoading] = useState(false);

    let rupiah = new Intl.NumberFormat('id-ID', {
        style: 'decimal',
        currency: 'IDR',
    });

    function diff_hours(etd: string, eta: string) {
        const dateTimePatern = 'YYYY-MM-DD HH:mm:ss'
        var d1 = moment(dataQuery?.departure_date, dateTimePatern).format('YYYY-MM-DD');
        var d2 = moment(dataQuery?.departure_date, dateTimePatern).format('YYYY-MM-DD');
        if (eta < etd) {
            d1 = moment(dataQuery?.departure_date, dateTimePatern).add(-1, 'day').format('YYYY-MM-DD')
        }
        const startDate = moment(d1 + ' ' + etd, dateTimePatern).format(dateTimePatern)
        const endDate = moment(d2 + ' ' + eta, dateTimePatern).format(dateTimePatern)
        return timeDuration(startDate, endDate);
    }

    async function getSchedule() {
        setIsLoading(true);
        setData(undefined);
        // DISINI JADWAL SEARCH JADWAL 2X DENGAN DI BALIK gunakan isSelectReturn
        if (!dataQuery?.return_date) {
            const { data } = await POST('/v1/search', dataQuery)
            setData(data);
        } else if (dataQuery?.return_date && !dataQuery?.tripcode) {
            const { data } = await POST('/v1/search', dataQuery)
            setData(data);
        } else if (dataQuery?.return_date && dataQuery?.tripcode && !dataQuery.return_tripcode) {
            const { data } = await POST('/v1/search', {
                "pickup_point": dataQuery.dropoff_point,
                "dropoff_point": dataQuery.pickup_point,
                "departure_date": dataQuery.return_date,
                "pax": dataQuery.pax
            })
            setData(data);
        }
        setIsLoading(false);
        return
    }

    useEffect(() => {
        getSchedule()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataQuery]);

    return (
        <div className='container'>
            {/* {JSON.stringify(data)}
            <br />
            {JSON.stringify(dataQuery)} */}
            <br />
            <div className="flex flex-col lg:flex-row gap-11 lg:gap-3 mt-11">
                <div className='w-full lg:max-w-md'>
                    <div className='text-xl text-center font-semibold mb-4'>Pencarian Jadwal</div>
                    {
                        dataQuery.departure_date && (
                            <ReservationFormSide dataQuery={dataQuery} getSchedule={getSchedule} setJadwalLoading={setIsLoading} setDataJadwal={setData} />
                        )
                    }
                </div>
                <div className='w-full'>
                    <div className='text-xl text-center font-semibold'>
                        {!dataQuery?.tripcode ?
                            <>
                                Jadwal Keberangkatan
                            </>
                            :
                            <>
                                Jadwal Pulang
                            </>
                        }
                    </div>
                    {dataQuery.pax > Number(config.maxPax) ? (
                        <div className='lg:col-span-2 text-center pt-4 text-red-500'>
                            {m.MAXPAX} {config.contactService}
                        </div>
                    ) : (
                        <div>
                            {isLoading && (
                                <SkeletonCard times={3} type='row' width='md:w-[850px] w-[320px]' height='h-[150px]' />
                            )}
                            {data && (
                                <>
                                    {
                                        (data.status_code == 0) ? (
                                            <div className='pt-4'>
                                                {/* {JSON.stringify(data.inventories)} */}
                                                {
                                                    data.inventories.length == 0 && (
                                                        <DataNotFound title={"Aduuh maaf, jadwal yang kamu cari tidak tersedia."} desc={"Coba ubah pencarian, jika kamu mau cari jadwal untuk keberangkatan ditanggal yang lain."} />
                                                    )
                                                }
                                                {
                                                    data.inventories.map((schedule: ScheduleType, i: number) => (
                                                        <ListSchedule schedule={schedule} dataQuery={dataQuery} isLoadingButton={isLoadingButton} setisLoadingButton={setisLoadingButton} key={i} />
                                                    ))
                                                }
                                            </div>
                                        ) : (
                                            <DataNotFound title={"Aduuh maaf, jadwal yang kamu cari tidak tersedia."} desc={"Coba ubah pencarian, jika kamu mau cari jadwal untuk keberangkatan ditanggal yang lain."} />
                                        )
                                    }
                                </>
                            )
                            }
                        </div>
                    )}
                </div>
            </div>
        </div >
    )
}
