"use client"

import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { FaCheck, FaWhatsapp } from "react-icons/fa";
import { useState } from "react";
import { POST } from "@/lib/http";
import OtpInput from 'react-otp-input';
import { toast } from "@/components/ui/use-toast";
import cookie from "@/lib/cookies";
import { useRouter } from "next/navigation";
import { getItem } from "@/lib/localStorage";
import Link from "next/link";
import axios from "axios";
// import useSWR from "swr";

const WhatsappModal = ({ isOpen, setIsOpen, email, text }: any) => {
  const whatsappOfficial = '6281333505008'
  const router = useRouter()
  // const { data: waData } = useSWR('https://baileys-bot.arnes.co.id/admin/getWaData');
  function generateRandomString() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';

    for (let i = 0; i < 16; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
      if ((i + 1) % 4 === 0 && i !== 15) {
        result += '-';
      }
    }

    // length = 19
    // getItem('waData') && (
    //   axios.post('https://baileys-bot.arnes.co.id/admin/setTokenWa', { token: result })
    // )
    return result;
  }
  const whatsappText = `*PERMINTAAN LOGIN*\n\nTerimakasih telah menggunakan arnes shuttle. untuk mendapatkan OTP silahkan *kirim pesan ini tanpa mengubah apapun*\n\n${email && (email + "\n\n")}${generateRandomString()}\n\nTerima Kasih\nwww.arnes.co.id`
  const encodedWhatsAppText = encodeURIComponent(whatsappText);

  const [otp, setOtp] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isClicked, setIsClicked] = useState(false);
  async function verifyOtp() {
    setIsLoading(true);
    const { data } = await POST('/v1/auth/otp/verified/whatsapp', { otp: otp })
    if (data.status === 'SUCCESS') {
      // kalo sukses set cookie
      cookie.storeData(data.token);
      toast({
        variant: 'success',
        title: 'LOGIN BERHASIL',
        description: 'Selamat datang di arnes shuttle'
      })
      setTimeout(() => {
        router.push('/')
      }, 2000);
    } else {
      toast({
        variant: 'destructive',
        title: 'GAGAL LOGIN',
        description: data.message
      })
      setIsLoading(false);
    }
  }

  return (
    <Dialog open={isOpen} onOpenChange={() => {
      setIsOpen(false);
      setIsClicked(false);
      setIsLoading(false);
      setOtp('');
    }}>
      <DialogContent className="max-w-[380px] md:min-w-[500px]">
        <DialogHeader>
          <DialogTitle className="pb-2 border-b-2">Kirim Otp</DialogTitle>
        </DialogHeader>
        <DialogDescription>
          {
            getItem('waData') || getItem('waData2') || whatsappOfficial ?
              <>
                {text && (
                  <p className="text-center mb-3 text-black" dangerouslySetInnerHTML={{ __html: text }}></p>
                )}
                <small>Silahkan klik <span className="font-bold text-red-500 italic">`Request OTP`</span> untuk meminta Kode OTP</small>
                <a href={`https://wa.me/${(getItem('waData') || getItem('waData2'))?.id?.split(':')?.[0] || whatsappOfficial}?text=${encodedWhatsAppText}`} target="_blank" rel="noopener noreferrer" className="w-full">
                  <Button variant={"primary"} className="w-full bg-green-500 border-green-700 hover:bg-green-600 mb-3" onClick={() => setIsClicked(true)} >
                    <FaWhatsapp className="mr-2" /> Request OTP
                  </Button>
                </a>
              </>
              :
              <>
                <div className="flex flex-col w-full gap-3">
                  <Button variant={'primary'} className="w-full" disabled>Mohon maaf, fitur WhatsApp sedang Offline</Button>
                  <Link href={'/'}>
                    <center>
                      <span className="text-xs text-blue-500 underline italic w-full text-center">Kembali ke beranda</span>
                    </center>
                  </Link>
                </div>
              </>
          }
          {
            isClicked && (
              <>
                <label htmlFor="kode-otp">Kode OTP</label>
                {/* <Input type="text" className="w-full my-3" id="kode-otp" onChange={handleInput} /> */}
                <OtpInput
                  value={otp}
                  onChange={(e) => setOtp(e)}
                  numInputs={6}
                  renderSeparator={<span>-</span>}
                  renderInput={(props) =>
                    <input {...props} type="number" />
                  }
                  containerStyle={'flex w-full justify-between my-3'}
                  inputStyle={'w-11 h-11 border-primary bg-gray-100 text-xl font-bold outline-none border-2 rounded-lg border-opacity-50 text-center'}
                  skipDefaultStyles={true}
                />
                <small className="text-gray-800">Setelah menerima kode OTP silahkan kembali ke halaman ini dan masukan kode OTP yang anda terima dari WhatsApp</small>
              </>
            )
          }
          {
            isClicked && (
              <>
                <Button variant={"primary"} className="w-full" id="verifikasi-otp" disabled={otp.length === 6 ? isLoading ? true : false : true} onClick={verifyOtp}>
                  <FaCheck className="mr-2" />  Verifikasi OTP
                </Button>
              </>
            )
          }
        </DialogDescription>
        {/* <DialogFooter> */}
        {/* </DialogFooter> */}
      </DialogContent>
    </Dialog >
  )
}

export default WhatsappModal