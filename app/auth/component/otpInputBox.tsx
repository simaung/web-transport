import { useState, useRef } from 'react';

function OtpInputBox() {
  const [otp, setOtp] = useState(new Array(6).fill(""));
  const inputRefs = useRef<(HTMLInputElement | null)[]>([]);

  const handleInputChange = (element: HTMLInputElement, index: number) => {
    if (isNaN(Number(element.value))) return;

    setOtp([...otp.map((d, idx) => (idx === index ? element.value : d))]);

    // Move focus to next input
    if (element.nextSibling) {
      (element.nextSibling as HTMLInputElement).focus();
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>, index: number) => {
    if (event.key === "Backspace" && otp[index] === "" && index !== 0) {
      inputRefs.current[index - 1]?.focus();
    }
  };

  return (
    <div className="flex justify-between space-x-2">
      {otp.map((data, index) => (
        <input
          key={index}
          type="text"
          name="otp"
          maxLength={1}
          className="w-12 h-12 border border-gray-300 text-center text-lg focus:outline-none focus:border-blue-500"
          value={data}
          onChange={e => handleInputChange(e.target as HTMLInputElement, index)}
          onKeyDown={e => handleKeyDown(e, index)}
          ref={(el: any) => (inputRefs.current[index] = el)}
        />
      ))}
    </div>
  );
}

export default OtpInputBox;
