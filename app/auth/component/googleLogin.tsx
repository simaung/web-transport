"use client"
import { signInWithPopup } from "firebase/auth";
import { auth, provider } from "../../../firebase.config";
import { useState } from "react";
import { GET, POST } from "@/lib/http";
import cookie from "@/lib/cookies";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FaTimes } from "react-icons/fa";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { toast } from "@/components/ui/use-toast";

interface profileShcema {
  "name": string,
  "email": string,
  "phone": string,
  "point": string,
  "type": string,
  "avatar": string
}

export default function GoogleLogin({ setIsOpen, setEmail, setText }: any) {
  const [userEmail, setUserEmail] = useState('');
  const [profile, setProfile] = useState<profileShcema>()
  const [modalKaitkanOpen, setModalKaitkanOpen] = useState(false);
  const searchParams = useSearchParams();
  const page = searchParams.get("page") || '/';
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const [nomorTelepon, setNomorTelepon] = useState('');
  const [nomorLoading, setNomorLoading] = useState(false);
  const handleGoogleLogin = async () => {
    setLoading(true);
    try {
      const result = await signInWithPopup(auth, provider);
      const user = result.user;
      setUserEmail(user?.email || '');
      const idToken = await user.getIdToken();
      const { data: userApi } = await POST('/v1/auth/firebase', { idToken });

      if (userApi.status === "SUCCESS") {
        cookie.storeData(userApi.success.token);
        checkPhoneNumber(userApi.success.token)
      } else {
        toast({
          variant: 'destructive',
          title: "Google Gagal",
          description: "sedang dalam perbaikan"
        })
        setLoading(false);
      }
      // Redirect to home page or dashboard
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };
  async function checkPhoneNumber(token: string) {
    const { data: dataProfile } = await GET("/v1/account/profile", {})
    setProfile(dataProfile.data);
    if (dataProfile.data.phone === null) {
      setModalOpen(true);
      return
    }
    toast({
      variant: 'success',
      title: "Berhasil Login",
      description: "Silahkan nikmati fitur point menarik dari Kami"
    })
    router.push('/')
  }

  function logout() {
    cookie.deleteCookie();
    toast({
      variant: 'default',
      title: "LOGIN DIBATALKAN",
      description: "Anda di kembalikan ke halaman utama"
    })
    setTimeout(() => {
      router.push('/')
    }, 1000);
  }

  const handleUpdatePhone = async () => {
    setNomorLoading(true);
    const { data: responseUpdate } = await POST('/v1/account/update', { phone: nomorTelepon })
    if (responseUpdate.status === "SUCCESS") {
      router.push('/')
    } else {
      // open modal kaitkan akun?
      // kirim gunakan modal yang ada di whatsapp
      // kirim pesan
      // cek phone number sudah memiliki email?langsung buka login wasap dengan keterangan lanjutkan login:kaitkan akun
      toast({
        variant: 'destructive',
        title: "login gagal",
        description: responseUpdate.message
      })
      const { data: responseCekEmail } = await GET('/v1/account/email/+' + nomorTelepon, {});
      if (responseCekEmail?.success?.email !== '+' + nomorTelepon + '@mail.com') {
        setIsOpen(true);
        setModalOpen(false);
        setText(`Nomor <span style="color:red;font-weight:bold;">${nomorTelepon}</span> sudah terdaftar, Silahkan lanjutkan login menggunakan WhatsApp`);
        return
      }
      setModalKaitkanOpen(true);
      setModalOpen(false);
      setNomorLoading(false);
    }
  }

  const handleInputChange = (e: any) => {
    let val = e.target.value;
    if (val.startsWith('0')) {
      val = '62' + val.slice(1);
    }
    if (val === '' || val === '6' || val < "62" || val.slice(0, 2) !== "62") {
      val = "62";
    }
    val = val.replace(/-/g, '');
    setNomorTelepon(val);
  };
  return (
    <>
      <div className={`${modalKaitkanOpen ? "fixed" : "hidden"} top-0 backdrop-blur min-h-screen w-full z-50 left-0`}>
        <div className="flex min-h-screen items-center justify-center">
          <div className="bg-white w-full h-full max-h-screen p-5 max-w-lg rounded-lg shadow-lg">
            <div className="flex flex-row gap-3 items-center justify-between border-b-2 pb-3">
              <h3 className="text-mg font-bold uppercase">Kaitkan Akun</h3>
              <FaTimes className="text-gray-400" onClick={logout} />
            </div>
            <div className="text-left">
              <p className="text-center">
                Nomor <span className="font-bold">{nomorTelepon}</span> telah terdaftar pada sistem kami.
              </p>
              <h3 className="text-center font-bold my-3 mt-5">INGIN MENGAITKAN AKUN?</h3>
              <small>Note :<br />-Anda akan di arahkan ke wasap untuk mengirim pesan tanpa mengedit <br />-Patikan   <span className="font-bold text-red-500">nomor WhatsApp yang mengirim pesan</span> adalah  <span className="font-bold text-red-500">{nomorTelepon}</span>  </small>
            </div>
            <div className="flex justify-between gap-3 mt-5">
              <Button variant={'primary'} className="w-full border-gray-400 hover:bg-gray-100 bg-gray-300 text-black" onClick={logout}>Tidak</Button>
              <Button variant={'primary'} className="w-full" onClick={() => {
                setIsOpen(true);
                setEmail(userEmail);
              }}>
                Ya
              </Button>
            </div>
          </div>
        </div>
      </div>
      <div className={`${modalOpen ? "fixed" : "hidden"} top-0 backdrop-blur min-h-screen w-full z-40 left-0`}>
        <div className="flex min-h-screen items-center justify-center">
          <div className="bg-white w-full h-full max-h-screen p-5 max-w-lg rounded-lg shadow-lg">
            <div className="flex flex-row gap-3 items-center justify-between border-b-2 pb-3">
              <h3 className="text-mg font-bold uppercase">Tambah Nomor Telepon</h3>
              <FaTimes className="text-gray-400" onClick={logout} />
            </div>
            {/* <small>Mengapa saya di haruskan memasukan nomor telepon? jawaban: karena email anda belum terdaftar secara sepenuhnya di dalam sistem kami. ketika sudah terdaftar, maka setelah memilih akun untuk login akan di arahkan langsung ke halaman beranda.</small> */}
            <Input
              type="number"
              placeholder="08xxxxxxxxx"
              className="text-center outline-none ring-0 mt-3"
              value={nomorTelepon}
              onChange={handleInputChange}
            />
            <small className="text-xs italic">Nomor telepon ini <span className="text-red-500">tidak bisa di ubah</span>, mohon masukan nomor telepon dengan benar</small>
            {nomorLoading ?
              <Button
                variant={"primary"}
                disabled={true}
                className="w-full mt-3 animate-pulse cursor-not-allowed"
                id="btn-checkout"
              >
                Loading...
              </Button>
              :
              <Button
                variant={"primary"}
                className="w-full mt-3"
                id="btn-checkout"
                onClick={handleUpdatePhone}
                disabled={nomorTelepon.length < 9 ? true : false}
              >
                Simpan Nomor
              </Button>
            }
          </div>
        </div>
      </div>
      {loading ?
        <button
          className="animate-pulse w-full rounded-lg flex flex-row items-center gap-3 justify-center shadow-md py-2 bg-gray-50 hover:-translate-y-1 duration-700" type="button"
          onClick={() => toast({
            variant: 'warning',
            title: "Perhatian",
            description: 'Selesaikain login sebelumnya'
          })}
        >
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="27" height="27" viewBox="0 0 48 48">
            <path fill="#FFC107" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#FF3D00" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4CAF50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1976D2" d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
          </svg>
          Loading...
        </button>
        :
        <button
          className="w-full rounded-lg flex flex-row items-center gap-3 justify-center shadow-md py-2 bg-gray-50 hover:-translate-y-1 duration-700" type="button"
          onClick={handleGoogleLogin}
        >
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="27" height="27" viewBox="0 0 48 48">
            <path fill="#FFC107" d="M43.611,20.083H42V20H24v8h11.303c-1.649,4.657-6.08,8-11.303,8c-6.627,0-12-5.373-12-12c0-6.627,5.373-12,12-12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C12.955,4,4,12.955,4,24c0,11.045,8.955,20,20,20c11.045,0,20-8.955,20-20C44,22.659,43.862,21.35,43.611,20.083z"></path><path fill="#FF3D00" d="M6.306,14.691l6.571,4.819C14.655,15.108,18.961,12,24,12c3.059,0,5.842,1.154,7.961,3.039l5.657-5.657C34.046,6.053,29.268,4,24,4C16.318,4,9.656,8.337,6.306,14.691z"></path><path fill="#4CAF50" d="M24,44c5.166,0,9.86-1.977,13.409-5.192l-6.19-5.238C29.211,35.091,26.715,36,24,36c-5.202,0-9.619-3.317-11.283-7.946l-6.522,5.025C9.505,39.556,16.227,44,24,44z"></path><path fill="#1976D2" d="M43.611,20.083H42V20H24v8h11.303c-0.792,2.237-2.231,4.166-4.087,5.571c0.001-0.001,0.002-0.001,0.003-0.002l6.19,5.238C36.971,39.205,44,34,44,24C44,22.659,43.862,21.35,43.611,20.083z"></path>
          </svg>
          Login dengan google
        </button>
      }
    </>
  )
}