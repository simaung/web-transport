"use client"
import { Suspense, useState } from "react";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Checkbox } from "@/components/ui/checkbox";
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form";
import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { toast } from "@/components/ui/use-toast";
import { Toaster } from "@/components/ui/toaster";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import Link from "next/link";
import config from "@/project.config";
import GoogleLogin from "../component/googleLogin";
import WhatsappLogin from "../component/whatsappLogin";
import { GET } from "@/lib/http";

const reservationFormSchema = z.object({
  email: z.string()
    .min(1, { message: "Mohon masukan email anda" })
    .email("Silahkan perbaiki Email anda"),
  password: z.string().min(1, 'Mohon masukan password'),
});

/* eslint-disable @next/next/no-img-element */
function LoginPage() {
  const [isOpen, setIsOpen] = useState(false);
  const [email, setEmail] = useState('');
  const [text, setText] = useState('');
  const router = useRouter();
  const searchParams = useSearchParams();
  const page = searchParams.get("page") || '/';

  const [passwordShow, setPasswordShow] = useState(false);
  const form = useForm<z.infer<typeof reservationFormSchema>>({
    resolver: zodResolver(reservationFormSchema),
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const { register, handleSubmit, setValue, watch, getValues } = form;

  function onSubmit(values: z.infer<typeof reservationFormSchema>) {
    fetch(config.apiUrl + '/v1/auth/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    })
      .then(response => response.json())
      .then(async (data) => {
        if (data?.status === "SUCCESS") {
          // const { data: dataProfile } = await GET(config.apiUrl + "/v1/account/profile", {})
          // console.log(dataProfile);

          toast({
            variant: "success",
            title: "Login Berhasil",
            description: "Silahkan nikmati fitur menarik dari kami"
          });
          // router.push(page);
          // return;
        } else {
          toast({
            variant: "destructive",
            title: "Login Gagal",
            description: data.message
          });
        }
      })
      .catch(error => console.error('Error:', error));
  }

  return (
    <>
      <Toaster />
      {/* <div className="flex justify-center items-center w-full h-screen bg-secondary">
        <div className="container">
          <div className="flex flex-row justify-between">
            <div className="max-w-2xl text-white self-center -mt-24">
              <h1 className="uppercase font-bold text-2xl mb-3">
                login arnes shuttle
              </h1>
              <p>
                Silahkan masuk ke akun arnes yang sudah anda daftarkan dan dapatkan promo menarik kami.
              </p>
              <Link href={'/'}>
                <Button variant={"outline-white"} className="mt-3">Ke Beranda</Button>
              </Link>
            </div>
            <div className="bg-white rounded-lg p-5 gap-5 flex flex-col w-96 shadow-md">
              <img src="/images/logo/logo.png" alt="" className="w-56 self-center" />
              <hr />
              <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-2 justify-between w-full">
                  <FormField
                    control={form.control}
                    name="email"
                    render={({ field }) => (
                      <FormItem className="w-full">
                        <FormControl>
                          <Input type="email" placeholder="Email" {...field} />
                        </FormControl>
                        <FormMessage className="text-xs" />
                      </FormItem>
                    )}
                  />
                  <div className="relative">
                    <FormField
                      control={form.control}
                      name="password"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormControl>
                            <Input type={passwordShow ? "text" : "password"} placeholder="Password" {...field} >
                            </Input>
                          </FormControl>
                          <FormMessage className="text-xs" />
                        </FormItem>
                      )}
                    />
                    {
                      passwordShow ?
                        <FaEyeSlash className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setPasswordShow(!passwordShow) }} />
                        :
                        <FaEye className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setPasswordShow(!passwordShow) }} />
                    }
                  </div>
                  <div className="group flex flex-row gap-3 items-center">
                    <Checkbox
                      id={'persetujuan'}
                      className=''
                      // checked={setuju}
                      disabled={false}
                    // onCheckedChange={(e: boolean) => {
                    //   setSetuju(e)
                    // }}
                    />
                    <label htmlFor="persetujuan" className="text-xs">Tetap Login</label>
                  </div>
                  <Button variant={'primary'} type="submit" >Masuk</Button>
                  <GoogleLogin />
                  <WhatsappLogin />
                  <small className="text-xs text-center">
                    Belum memiliki akun? silahkan <Link href={'/auth/register'} className="text-blue-500 hover:text-blue-700 duration-300 cursor-pointer">Daftar</Link> terlebih dahulu!
                  </small>
                </form>
              </Form>
            </div>
          </div>
        </div>
      </div> */}

      <div className="relative">
        <img
          alt=""
          src="/images/background/bglogin.png"
          className="inset-0 h-screen w-full object-cover object-right fixed top-0 z-0"
        />
        <div className="absolute top-0 flex items-center justify-start h-screen w-full">
          <div className="sm:max-w-sm w-full text-center flex flex-col gap-3 p-7 bg-gray-900 bg-opacity-85 shadow-lg h-screen items-center justify-center">
            <h1 className="text-2xl font-bold sm:text-3xl text-white">Masuk Arnes Shuttle!</h1>
            <p className="text-white">
              Selamat datang di Arnes Shuttle! Mulai perjalanan Anda dengan kenyamanan dan kemudahan.
            </p>
            <hr />
            <GoogleLogin setIsOpen={setIsOpen} setEmail={setEmail} setText={setText} />
            <WhatsappLogin isOpen={isOpen} setIsOpen={setIsOpen} email={email} text={text} />
            <hr />
          </div>
        </div>
      </div>
    </>
  );
}

export default function Page() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <LoginPage />
    </Suspense>
  );
}
