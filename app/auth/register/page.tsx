"use client"
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Checkbox } from '@/components/ui/checkbox';
import { useState } from "react";
import { Form, FormControl, FormField, FormItem, FormMessage } from "@/components/ui/form"
import { z } from "zod"
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { FaEye, FaEyeSlash } from "react-icons/fa";
import { toast } from "@/components/ui/use-toast";
import { Toaster } from "@/components/ui/toaster";
import { useRouter } from "next/navigation";
import config from "@/project.config";
import Link from "next/link";

const reservationFormSchema = z.object({
  username: z.string().min(1, 'Masukan nama anda'),
  phone: z.string().min(9, 'Mohon masukan nomor telepon yang masih aktif').max(13, 'Maksimal nomor telepon adalah 13'),
  email: z.string()
    .min(1, { message: "Mohon masukan email anda" })
    .email("Silahkan perbaiki Email anda"),
  // .refine((e) => e === "abcd@fg.com", "This email is not in our database"),
  password: z.string().min(1, 'Mohon masukan password'),
  c_password: z.string().min(1, 'Mohon sesuaikan dengan password pertama'),
  type: z.string().min(1, 'type masih kosong'),
}).refine((data) => data.password === data.c_password, {
  message: "Password tidak sama",
  path: ["c_password"],
});

/* eslint-disable @next/next/no-img-element */
export default function Page() {
  const router = useRouter();
  const [setuju, setSetuju] = useState(false)
  const [passwordShow, setPasswordShow] = useState(false)
  const [cPasswordShow, setCPasswordShow] = useState(false)
  const form = useForm<z.infer<typeof reservationFormSchema>>({
    resolver: zodResolver(reservationFormSchema),
    defaultValues: {
      username: "",
      phone: "",
      email: "",
      password: "",
      c_password: "",
      type: "user"
    },
  })
  const { register, handleSubmit, setValue, watch, getValues } = form;

  function onSubmit(values: z.infer<typeof reservationFormSchema>) {
    fetch(config.apiUrl + '/v1/auth/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values),
    })
      .then(response => response.json())
      .then(data => {
        if (data?.status === "SUCCESS") {
          toast({
            variant: "success",
            title: "Registrasi Berhasil",
            description: "Silahkan lanjutkan login dengan akun yang telah anda daftarkan"
          })
          router.push('/auth/login')
          return
        } else {
          toast({
            variant: "destructive",
            title: "Registrasi Gagal",
            description: data.message
          })
        }
      })
      .catch(error => console.error('Error:', error));

    // const paramQuery = `depart=${values.depart}&arrival=${values.arrival}&date=${format(values.dateDepart, "y-MM-dd")}&pax=${values.pax}`
    // router.push("/schedule?" + paramQuery)
  }
  return (
    <>
      <Toaster />
      <div className="flex justify-center items-center w-full h-screen bg-secondary">
        {/* <img src="/images/logo/logo-white.png" alt="" className="w-screen self-center absolute top- left-0 opacity-30 blur-sm" /> */}
        <div className="container">
          <div className="flex flex-row justify-between">
            <div className="max-w-2xl text-white self-center -mt-24">
              <h1 className="uppercase font-bold text-2xl mb-3">
                Buat akun Arnes Shuttle
              </h1>
              <p>
                Bergabunglah dengan Arnes Shuttle untuk mendapatkan akses ke layanan transportasi yang cepat, nyaman, dan terpercaya. Daftar sekarang dan rasakan perbedaannya!
              </p>
            </div>
            <div className="bg-white rounded-lg p-5 gap-5 flex flex-col w-96">
              <img src="/images/logo/logo.png" alt="" className="w-56 self-center" />
              <hr />
              <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col gap-2 justify-between w-full">
                  <FormField
                    control={form.control}
                    name="username"
                    render={({ field }) => (
                      <FormItem className="w-full">
                        <FormControl>
                          <Input type="text" placeholder="Nama" {...field} />
                        </FormControl>
                        <FormMessage className="text-xs" />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="phone"
                    render={({ field }) => (
                      <FormItem className="w-full">
                        <FormControl>
                          <Input type="number" placeholder="Telepon" {...field} />
                        </FormControl>
                        <FormMessage className="text-xs" />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={form.control}
                    name="email"
                    render={({ field }) => (
                      <FormItem className="w-full">
                        <FormControl>
                          <Input type="email" placeholder="Email" {...field} />
                        </FormControl>
                        <FormMessage className="text-xs" />
                      </FormItem>
                    )}
                  />
                  <div className="relative">
                    <FormField
                      control={form.control}
                      name="password"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormControl>
                            <Input type={passwordShow ? "text" : "password"} placeholder="Password" {...field} >
                            </Input>
                          </FormControl>
                          <FormMessage className="text-xs" />
                        </FormItem>
                      )}
                    />
                    {
                      passwordShow ?
                        <FaEyeSlash className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setPasswordShow(!passwordShow) }} />
                        :
                        <FaEye className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setPasswordShow(!passwordShow) }} />
                    }
                  </div>
                  <hr />
                  <div className="relative">
                    <FormField
                      control={form.control}
                      name="c_password"
                      render={({ field }) => (
                        <FormItem className="w-full">
                          <FormControl>
                            <Input type={cPasswordShow ? "text" : "password"} placeholder="Ulangi Password" {...field} >
                            </Input>
                          </FormControl>
                          <FormMessage className="text-xs" />
                        </FormItem>
                      )}
                    />
                    {
                      passwordShow ?
                        <FaEyeSlash className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setCPasswordShow(!cPasswordShow) }} />
                        :
                        <FaEye className="absolute top-3 right-2 text-gray-500 cursor-pointer" title="show password" onClick={() => { setCPasswordShow(!cPasswordShow) }} />
                    }
                  </div>
                  <div className="group flex flex-row gap-3 items-center">
                    <Checkbox
                      id={'persetujuan'}
                      className=''
                      checked={setuju}
                      disabled={false}
                      onCheckedChange={(e: boolean) => {
                        setSetuju(e)
                      }}
                    />
                    <label htmlFor="persetujuan" className="text-xs">Saya telah membaca dan menyetujui <a href="/policy" rel="noopener noreferrer" className="text-blue-500 cursor-pointer hover:text-blue-700 duration-300">Syarat & Ketentuan</a> dari arnes shuttle.</label>
                  </div>
                  <Button variant={'primary'} type="submit" disabled={!setuju} >Registrasi</Button>
                  <small className="text-xs text-center">
                    Sudah memiliki akun? silahkan <Link href={'/auth/login'} className="text-blue-500 hover:text-blue-700 duration-300 cursor-pointer">Login</Link>
                  </small>
                </form>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}