export function groupingOutlet(data: any) {
    const sortData = sortFunction(data, "name");

    const result = Object.values(sortData.reduce((newData: any, currentItem: any) => {
        if (newData[currentItem.city]) {
            newData[currentItem.city].sortData.push(currentItem)
        } else {
            newData[currentItem.city] = { city: currentItem.city, sortData: [currentItem] }
        }
        return newData;
    }, {}));

    return sortFunction(result, "city");
}

function sortFunction(result: any, key: string) {

    result?.sort(function (a: any, b: any) {
        if (a[key] < b[key]) {
            return -1;
        }
        if (a[key] > b[key]) {
            return 1;
        }
        return 0;
    })

    return result
}

export function groupingDropPoint(data: any) {
    var i;
    for (i = 0; i < data?.length; i++) {
        data[i].id = data[i]['dropoff_point_id'];
        data[i].name = data[i]['dropoff_point_name'];
    }

    const sortData = sortFunction(data, "dropoff_point_name");

    const result = Object?.values(sortData?.reduce((newData: any, currentItem: any) => {
        if (newData[currentItem.dropoff_point_city]) {
            newData[currentItem.dropoff_point_city].sortData.push(currentItem)
        } else {
            newData[currentItem.dropoff_point_city] = { city: currentItem.dropoff_point_city, sortData: [currentItem] }
        }
        return newData;
    }, {}));

    return sortFunction(result, "city");
}

export function groupPayment(data: any, key: string) {
    // const filter: any[] = []
    // data.map((e: any) => {
    //     if (e.status === 'active') {
    //         filter.push(e)
    //     }
    // })
    return data.reduce((acc: any, obj: any) => {
        const groupKey = obj[key];
        if (!acc[groupKey]) {
            acc[groupKey] = [];
        }
        acc[groupKey].push(obj);
        return acc;
    }, {});
}