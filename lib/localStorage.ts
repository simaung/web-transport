export function store(key: string, data: object) {
  try {
    if (typeof window !== 'undefined') {
      localStorage.setItem(key, JSON.stringify(data));
    }
    return true
  } catch (error) {
    // console.log(error);
    return false
  }
}

// untuk history booking belum jadi ini 
export function storeArray(key: string, data: object) {
  try {
    if (typeof window !== 'undefined') {
      var lastUpdate = getItem(key) || []
      lastUpdate.push(data);
      localStorage.setItem(key, JSON.stringify(lastUpdate));
    }
    return true
  } catch (error) {
    // console.log(error);
    return false
  }
}

export function getItem(key: string) {
  try {
    if (typeof window !== 'undefined') {
      return JSON.parse(localStorage.getItem(key)!);
    }
  } catch (error) {
    // console.log(error);
    return false
  }
}

export function removeItem(key: string) {
  try {
    if (typeof window !== 'undefined') {
      localStorage.removeItem(key);
      return true
    }
  } catch (error) {
    // console.log(error);
    return false
  }
}