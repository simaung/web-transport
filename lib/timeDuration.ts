import moment from "moment";
export function timeDuration(startDate: string, endDate: string) {
  const format = "YYYY-MM-DD HH:mm:ss";
  const start = moment(startDate, format);
  const end = moment(endDate, format);

  const duration = moment.duration(end.diff(start));

  const hours = Math.floor(duration.asHours());
  const minutes = duration.minutes();

  var output = ''
  if (hours !== 0) {
    output += hours + ' Jam '
  }
  if (minutes !== 0) {
    output += minutes + ' Menit'
  }
  return output;
}