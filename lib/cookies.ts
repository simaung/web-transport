"use client"
import Cookies from "js-cookie"

function storeData(token: string) {
  Cookies.set('token', token, { expires: 365, path: '/' })
}

function getToken() {
  return Cookies.get('token')
}

function deleteCookie() {
  return Cookies.remove('token')
}

const cookie = {
  storeData,
  getToken,
  deleteCookie
}
export default cookie
