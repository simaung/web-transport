"use client"
import axios from 'axios';
import cookie from '../lib/cookies';

const instance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
  headers: {
    "Content-Type": "application/json",
    'order-from': 'web'
  },
});

instance.interceptors.request.use(
  (config) => {
    const authCookie = cookie.getToken();
    // console.log("Retrieved authCookie:", authCookie); // Debugging line

    if (authCookie) {
      config.headers['Authorization'] = `Bearer ${authCookie}`;
    } else {
      // console.log("No authCookie found");
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// axios get
export function GET(url: string, params: object) {
  return instance.get(url, { params: params });
}

// axios post
export async function POST(url: string, params: object) {
  return instance({
    method: 'post',
    url: url,
    data: params,
  });
}
