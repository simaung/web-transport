import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { useRouter } from "next/navigation"
import { FC, useState } from "react"

interface SelectInputProps {
    title: string,
    data?: []
}

const SelectInput: FC<SelectInputProps> = ({
    title,
    data
}) => {
    const router = useRouter()
    const [value, setValue] = useState('')

    function goTo(section: string) {
        setValue(section)
        router.push('outlet#' + section)
    }

    return (
        <Select onValueChange={(value) => { goTo(value) }}>
            < SelectTrigger className="w-[180px] md:w-[500px] border-2 focus:border-primary focus:ring-0 hover:border-primary" >
                <SelectValue placeholder={title} />
            </SelectTrigger >
            <SelectContent className="w-[180px] md:w-[500px]">
                {data && (
                    data.map((i: any) =>
                    (
                        <SelectItem
                            key={i.city}
                            value={`${i.city}`}
                            className={`
                                focus:bg-primary focus:text-primary-foreground
                                ${i.city == value ? 'bg-primary text-primary-foreground' : ''}
                            `}
                        >
                            {i.city}
                        </SelectItem>
                    ))
                )}
            </SelectContent>
        </Select >
    )
}

export default SelectInput