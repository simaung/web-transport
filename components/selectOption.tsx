// import useArrivalSelect from '@/hooks/useArrivalSelect';
import { FC, useId } from 'react';
import Select from 'react-select';

interface SelectOptionProps {
    value: any,
    onValueChange: any
    data: any
    title: string
    disabled?: boolean
    className?: string
}

const customStyles = {
    control: (baseStyles: any, state: any) => ({
        ...baseStyles,
        borderRadius: "0.5rem",
        boxShadow: "none",
        "&:hover": {
            border: "1px solid hsl(var(--secondary))",
        },
        "&:focus-within": {
            border: "1px solid hsl(var(--secondary))",
        },
    }),

    option: (defaultStyles: any, state: any) => ({
        ...defaultStyles,
        backgroundColor: state.isSelected ? "hsl(var(--primary))" : "",
        paddingLeft: "30px",
        fontSize: "0.875rem",
        lineHeight: "1.25rem",
        "&:hover": {
            backgroundColor: "hsl(var(--secondary))"
        },
    }),
    valueContainer: (provided: any, state: any) => ({
        ...provided,
        height: '30px',
        padding: '0 6px'
    }),
}

const SelectOption: FC<SelectOptionProps> = ({
    value,
    onValueChange,
    data,
    title,
    className,
}) => {
    function setData(val: any, onChange: any) {
        onChange(val.value)
    }

    return (
        <Select
            instanceId={useId()}
            placeholder={title}
            value={data.find((c: any) => c.value === value)}
            onChange={(val) => setData(val, onValueChange)}
            options={data}
            className={className}
            styles={customStyles}
        />
    )
}

export default SelectOption