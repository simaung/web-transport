// components/Barcode.js
import { useEffect, useRef } from 'react';
import JsBarcode from 'jsbarcode';

const Barcode = ({ value }: any) => {
  const canvasRef = useRef(null);

  useEffect(() => {
    if (canvasRef.current) {
      JsBarcode(canvasRef.current, value, {
        format: 'CODE128', // You can change the format here
        displayValue: false,
        width: 2,
        height: 71,
      });
    }
  }, [value]);

  return <canvas ref={canvasRef}></canvas>;
};

export default Barcode;
