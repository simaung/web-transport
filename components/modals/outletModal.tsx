"use client"

import useDetailOutletModal from "@/hooks/useDetailOutletModal";
import { Dialog, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "../ui/dialog"
import { Button } from "../ui/button";
import Link from "next/link";

const OutletModal = () => {
    const detailOutletModal = useDetailOutletModal();

    return (
        <Dialog open={detailOutletModal.isOpen} onOpenChange={detailOutletModal.onClose}>
            <DialogContent className="max-w-[380px] md:min-w-[500px]">
                <DialogHeader>
                    <DialogTitle className="pb-2 border-b-2">Detail Outlet</DialogTitle>
                    <DialogDescription>
                    </DialogDescription>
                    <div className="font-bold text-xl text-primary">{detailOutletModal.data?.name}</div>
                    <div className="mt-2 text-justify text-sm">
                        <p className="font-bold">Alamat :</p>
                        <p className="capitalize mb-4">
                            {detailOutletModal.data?.address.toLowerCase()}
                        </p>
                        <p className="font-bold">Nomor Kontak :</p>
                        <p className="mb-4">
                            {(detailOutletModal.data?.contact != '') ? detailOutletModal.data?.contact : '-'}
                        </p>
                        <p className="font-bold">Jam Operasional :</p>
                        <p>
                            {(detailOutletModal.data?.open_time != null) ? detailOutletModal.data?.open_time.substring(0, 5) + ' - ' : '-'}
                            {detailOutletModal.data?.close_time?.substring(0, 5)}
                        </p>
                    </div>
                </DialogHeader>
                <DialogFooter>
                    {detailOutletModal.data?.latitude === null ? (
                        <Button variant={"primary"} className="w-full" disabled>
                            Map belum tersedia
                        </Button>
                    ) : (
                        <Link
                            href={`https://www.google.com/maps/search/?api=1&query=${detailOutletModal.data?.latitude},${detailOutletModal.data?.longitude}`}
                            target="_blank"
                            className="w-full">
                            <Button variant={"primary"} className="w-full">
                                Lihat Map
                            </Button>
                        </Link>
                    )}
                </DialogFooter>
            </DialogContent>
        </Dialog>
    )
}

export default OutletModal