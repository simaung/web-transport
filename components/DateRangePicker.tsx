import { useState } from "react";
import DatePicker, { Calendar } from "react-multi-date-picker";
import { Button } from "./ui/button";
import moment from "moment";
import "moment/locale/id";

const DateRangePicker = () => {
  // Explicitly define the type as string[]
  const [dates, setDates] = useState<string[]>([]);

  return (
    <div className="h-fit bg-white border-[1px] border-gray-300 p-3 absolute top-11 w-fit left-0 rounded-lg">
      {/* Date Display */}
      <div className="flex flex-row gap-3">
        <div className="p-2 text-xs rounded-lg border-2 w-full text-center">
          {dates[0] ? moment(dates[0]).locale("id").format("LL") : "Tgl. Berangkat"}
        </div>
        <div className="p-2 text-xs rounded-lg border-2 w-full text-center">
          {dates[1] ? moment(dates[1]).locale("id").format("LL") : "Tgl. Pulang"}
        </div>
      </div>

      {/* Calendar */}
      <Calendar
        className="w-full mt-3"
        shadow={false}
        showOtherDays={true}
        highlightToday={false}
        multiple={true}
        value={dates}
        minDate={dates?.[0] || new Date()} // Min date set to today
        maxDate={''}
        format="YYYY-MM-DD"
        onChange={(selectedDates) => {
          const dateSplit = selectedDates.toLocaleString().split(',');
          const tglBerangkat = dateSplit?.[0];
          const tglPulang = dateSplit?.[1];
          if (selectedDates.length <= 2) {

            setDates(
              [tglBerangkat, tglPulang]
            )
          }
        }}
      />

      {/* Button */}
      <Button
        variant="primary"
        className="mt-3 w-full"
        onClick={() => {
          console.log("Selected Dates:", dates);
        }}
      >
        Selesai
      </Button>
    </div>
  );
};

export default DateRangePicker;
