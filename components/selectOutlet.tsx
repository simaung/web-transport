"use client";

import useArrivalSelect from '@/hooks/useArrivalSelect';
import { FC, useId } from 'react';
import Select from 'react-select';

interface SelectOutletProps {
    value: any,
    onValueChange: any
    data: any
    title: string
    disabled?: boolean
    className?: string
}

const getOutletData = (outlet: any) => {
    var outletData = new Array();

    outlet.map((outlet: any) => {
        const optionOutlet = outlet.sortData.map((point: any) => {
            return {
                'value': point.id,
                'label': point.name
            }
        })

        outletData.push({
            'label': outlet.city,
            'options': optionOutlet
        });
    })
    return outletData
}

const customStyles = {
    control: (baseStyles: any, state: any) => ({
        ...baseStyles,
        borderRadius: "0.5rem",
        boxShadow: "none",
        "&:hover": {
            border: "1px solid hsl(var(--secondary))",
        },
        "&:focus-within": {
            border: "1px solid hsl(var(--secondary))",
        },
    }),

    option: (defaultStyles: any, state: any) => ({
        ...defaultStyles,
        backgroundColor: state.isSelected ? "hsl(var(--primary))" : "",
        paddingLeft: "30px",
        fontSize: "0.875rem",
        lineHeight: "1.25rem",
        "&:hover": {
            backgroundColor: "hsl(var(--secondary))"
        },
    }),
    valueContainer: (provided: any, state: any) => ({
        ...provided,
        height: '30px',
        padding: '0 6px'
    }),
}

const SelectOutlet: FC<SelectOutletProps> = ({
    value,
    onValueChange,
    data,
    title,
    disabled,
    className
}) => {
    const arrivalSelect = useArrivalSelect()

    function setDepart(val: any, onChange: any) {

        onChange(val.value)
        if (title == 'Outlet Keberangkatan') {
            arrivalSelect.onDepartSelect(val.value)
        } else {
            arrivalSelect.onArrivalSelect(val.value)
        }
    }

    let selectedValue = "";
    getOutletData(data).map((outletList) => {
        outletList.options.map((ot: any) => {
            if (ot.value === value) {
                selectedValue = ot;
                return;
            }
        })
    });

    return (
        <Select
            instanceId={useId()}
            placeholder={title}
            value={selectedValue}
            onChange={(val) => setDepart(val, onValueChange)}
            options={getOutletData(data)}
            styles={customStyles}
            isDisabled={disabled}
            className={className}
            noOptionsMessage={() => 'Outlet Tujuan Tidak Tersedia'}
        />
    );
}

export default SelectOutlet