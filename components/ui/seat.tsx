"use client"
import { useState, useEffect } from "react"

type SeatState = "active" | "booked" | "available";

interface SeatProps {
  state: SeatState;
  label: string
}

export default function Seat({ state, label }: SeatProps) {
  const stateClasses: { [key in SeatState]: string } = {
    active: 'fill-secondary stroke-primary stroke-[25] text-white cursor-pointer',
    booked: 'fill-gray-200 stroke-gray-300 stroke-[25]',
    available: 'fill-white stroke-gray-400 stroke-[25] group-hover:stroke-primary duration-200 cursor-pointer'
  };

  const [classes, setClasses] = useState(stateClasses[state]);

  useEffect(() => {
    setClasses(stateClasses[state]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  return (
    <>
      <div className="relative w-full h-full max-w-16 flex items-center justify-center cursor-pointer">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 553.41 584.87" className="group w-full h-auto">
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <ellipse cx="276.7" cy="510.72" rx="264.2" ry="61.66" className={classes} />
              <rect x="60.88" y="12.5" width="432" height="492.48" rx="34.56" className={classes} />
              <rect x="449.32" y="126.55" width="86.4" height="264.38" rx="34.56" className={classes} />
              <rect x="17.68" y="126.55" width="86.4" height="264.38" rx="34.56" className={classes} />
              <text x="50%" y="50%" textAnchor="middle" dominantBaseline="middle" fontSize={151} className={`font-bold left-7 ${state === 'active' ? "text-white" : "text-black"}`}>
                {label}
              </text>
            </g>
          </g>
        </svg>
      </div>
    </>
  )
}