import { FaCircleNotch } from "react-icons/fa";
import { Button } from "./ui/button";

interface LoadingButtonProps {
  className?: string;
}

export default function LoadingButton({ className = "" }: LoadingButtonProps) {
  return (
    <Button
      variant="primary"
      disabled={true}
      type="button"
      className={`cursor-not-allowed ${className}`}
      title="Memproses data"
    >
      <FaCircleNotch className="animate-spin mr-3" /> Loading...
    </Button>
  );
}
