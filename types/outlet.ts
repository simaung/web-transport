export type Outlet = {
    "id": string,
    "name": string,
    "city": string,
    "address": string,
    "contact": string,
    "open_time": string,
    "close_time": string,
    "latitude": string,
    "longitude": string,
    "link_maps": string
}

export type ScheduleType = {
    "tripcode": string,
    "departure_time": string,
    "arrival_time": string,
    "remaining_seat": number,
    "price": number,
    "price_discount": number,
    "vessel_name": string,
    "course": CourseType[]
}

export type SeatLayout = {
    "id": string,
    "layout_id": number,
    "name": string,
    "type": string,
    "col": string,
    "row": string,
    "status": string,
    "label": string
}

export type CourseType = {
    "sequence": number,
    "point_id": string,
    "point_name": string,
    "departure_hour": string,
    "departure_time_zone": string,
    "days_offest": number
}