const config = {
    appName: process.env.NEXT_PUBLIC_APP_NAME,
    apiUrl: process.env.NEXT_PUBLIC_API_URL,
    appCode: process.env.NEXT_PUBLIC_APP_CODE,
    contactService: process.env.NEXT_PUBLIC_CONTACT_SERVICE,
    emailService: process.env.NEXT_PUBLIC_EMAIL_SERVICE,
    addressService: process.env.NEXT_PUBLIC_ADDRESS_SERVICE,
    mapService: process.env.NEXT_PUBLIC_MAP_SERVICE,
    twitterService: process.env.NEXT_PUBLIC_TWITTER_SERVICE,
    instagramService: process.env.NEXT_PUBLIC_INSTAGRAM_SERVICE,
    facebookService: process.env.NEXT_PUBLIC_FACEBOOK_SERVICE,
    maxPax: process.env.NEXT_PUBLIC_MAX_PAX,
    footDesc1: process.env.NEXT_PUBLIC_MAIN_DESC_1,
    footDesc2: process.env.NEXT_PUBLIC_MAIN_DESC_2,
    footDesc3: process.env.NEXT_PUBLIC_MAIN_DESC_3
}

export default config